/* Simplified version of WebKit-style NaN-boxing, as defined in
 * https://github.com/WebKit/WebKit/blob/main/Source/JavaScriptCore/runtime/JSCJSValue.h#L402 */

type Obj = i256;
represented as (_ as w256)

enum Num { Int(i32), Double(i64) }
enum T {
  Undef,
  Null,
  Bool(Bool),
  Num(Num),
  ObjRef(Obj),
}
represented as
split .[48, 16] {
  | 0 from (ObjRef | Undef | Null | Bool) =>
    split .[0, 48] {
      | 6 from Bool(False) => w64 with .[0, 48] : (= 6)
      | 7 from Bool(True) => w64 with .[0, 48] : (= 7)
      | 10 from Undef => w64 with .[0, 48] : (= 10)
      | 2 from Null => w64 with .[0, 48] : (= 2)
      | _ from ObjRef => &<48, 0>(_.ObjRef as Obj)
    } with .[48, 16] : (= 0)
  | 65534 from Num(Int) => // 65534 = 0xfffe
    w64 with .[0, 32] : _.Num.Int as w32 with .[32, 16] : (= 0) with .[48, 16] : (= 65534)
  | _ from Num(Double) =>
     _.Num.Double as w64 // ribbit does not support arithmetic operations on atoms yet
}

let undef : T = Undef;
let num : T = Num(Int(42));
// Trying to build an ObjRef value breaks ribbit; should be fixed with modern mem tys/ptrs
