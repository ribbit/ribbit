enum Reg32 {
  X0, X1, X2, X3, X4, X5, X6, X7,
  X8, X9, X10, X11, X12, X13,
  X14, X15, X16, X17, X18, X19,
  X20, X21, X22, X23, X24, X25,
  X26, X27, X28, X29, X30, X31
}
represented by c
enum Op32 {
  Add(Reg32, Reg32, Reg32),
  Addi(Reg32, Reg32, i12),
  Jal(Reg32, i20),
  Sw(Reg32, Reg32, i12),
}
represented as
split .[0, 7] {
    | 19 from Addi =>
      w32
      with .[7, 5] : (_.Addi.0 as Reg32)
      with .[12, 3] : (= 0)
      with .[15, 5] : (_.Addi.1 as Reg32)
      with .[20, 12] : (_.Addi.2 as w12)
    | 51 from Add =>
      w32
      with .[7, 5] : (_.Add.0 as Reg32)
      with .[12, 3] : (= 0)
      with .[15, 5] : (_.Add.1 as Reg32)
      with .[20, 5] : (_.Add.2 as Reg32)
      with .[25, 7] : (= 0)
    | 111 from Jal =>
      w32
      with .[7, 5] : (_.Jal.0 as Reg32)
      with .[12, 7]  : (_.Jal.1.[11,7] as w7)
      with .[20, 1]  : (_.Jal.1.[10,1] as w1)
      with .[21, 10] : (_.Jal.1.[0,10] as w10)
      with .[31, 1]  : (_.Jal.1.[19,1] as w1)
    | 35 from Sw =>
      w32
      with .[7, 5] : (_.Sw.2.[0,5] as w5)
      with .[12, 3] : (= 2)
      with .[15, 5] : (_.Sw.0 as Reg32)
      with .[20, 5] : (_.Sw.1 as Reg32)
      with .[25, 7] : (_.Sw.2.[5,7] as w7)
  }

// enum Reg16 { X8, X9, X10, X11, X12, X13, X14, X15 }
// represented by c

// struct Addi16Imm(i5, i1);
// represented by c
// struct Jal16Off(i3, i1, i1, i1, i1, i2, i1, i1);
// represented by c
// struct Sw16Imm(i1, i2, i1, i1);
// represented by c

// enum Op16 {
//   Add(Reg32, Reg32),
//   Addi(Reg32, Addi16Imm),
//   Jal(Jal16Off),
//   Sw(Reg16, Reg16, Sw16Imm),
// }
// represented as split .[0, 2] {
//   | 0 from Sw =>
//     w16 with .[0, 2] : (= 0)
//     with .[2, 3] : (_.Sw.1 as Reg16)
//     with .[5, 1] : (_.Sw.2.3 as w1)
//     with .[6, 1] : (_.Sw.2.0 as w1)
//     with .[7, 3] : (_.Sw.0 as Reg16)
//     with .[10, 2] : (_.Sw.2.1 as w2)
//     with .[12, 1] : (_.Sw.2.2 as w1)
//     with .[13, 3] : (= 6)
//   | 1 from (Addi | Jal) => split .[13, 3] {
//       | 0 from Addi =>
//         w16 with .[0, 2] : (= 1)
//         with .[2, 5] : (_.Addi.1.0 as w5)
//         with .[7, 5] : (_.Addi.0 as Reg32)
//         with .[12, 1] : (_.Addi.1.1 as w1)
//         with .[13, 3] : (= 0)
//       | 1 from Jal =>
//         w16 with .[0, 2] : (= 1)
//         with .[2, 1] : (_.Jal.2 as w1)
//         with .[3, 3] : (_.Jal.0 as w3)
//         with .[6, 1] : (_.Jal.4 as w1)
//         with .[7, 1] : (_.Jal.3 as w1)
//         with .[8, 1] : (_.Jal.6 as w1)
//         with .[9, 2] : (_.Jal.5 as w2)
//         with .[11, 1] : (_.Jal.1 as w1)
//         with .[12, 1] : (_.Jal.7 as w1)
//         with .[13, 3] : (= 1)
//     }
//   | 2 from Add =>
//     w16 with .[0, 2] : (= 2)
//     with .[2, 5] : (_.Add.1 as Reg32)
//     with .[7, 5] : (_.Add.0 as Reg32)
//     with .[12, 4] : (= 9)
// }

// enum Op {
//   Op32(Op32),
//   Op16(Op16)
// }
// represented as split .[0, 2] {
//   | 0 from Op16(Sw(_)) => _.Op16 as Op16
//   | 1 from Op16(Addi(_)), Op16(Jal(_)) => _.Op16 as Op16
//   | 2 from Op16(Add(_)) => _.Op16 as Op16
//   | 3 from Op32(_) => _.Op32 as Op32
//   //| _ from Op16(_) => _.Op16 as Op16
// }

fn is_compressible(o : Op32) -> Bool {
  match o {
    Addi(rd, rs, imm) =>
      let x0 : Reg32 = X0;
      let bound : i12 = 64;
      rd == rs && !(rs == x0) && imm < bound,
    Add(rd, rs1, rs2) =>
      let x0 : Reg32 = X0;
      rd == rs1 && !(rs1 == x0) && !(rs2 == x0),
    Jal(X1, off) =>
      let bound : i20 = 4096;
      off < bound,
    Sw(rbase, roff, imm) =>
      let x8 : Reg32 = X8;
      let x15 : Reg32 = X15;
      let mask : i12 = 3103; // 0b110000011111
      let zero : i12 = 0;
      x8 <= rbase && rbase <= x15 && x8 <= roff && roff <= x15 && ((imm & mask) == zero),
    _ => False,
  }
}

let imm : i20 = 12;
let value_builder : Op32 = Sw(X1, X2, imm);
