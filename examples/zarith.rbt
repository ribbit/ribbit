/* Zarith in Ribbit

  Zarith is an OCaml library for arbitrary precision numbers.
  Zarith's numbers are either small integers, implemented by
  naked, 63-bits OCaml integers, or big integers using GMPs.

  The original Zarith library, implemented by Xavier Leroy,
  leverages various tricks using the FFI C interface to implement
  its memory layout, which is not expressible in OCaml.

  We demonstrate how Ribbit can readily implement this layout.
  See the article for details.
*/

type BigInt = i128;
represented as (_ as w128)
/* BigInt here are wide integer encoded "as itself" on 128 bits.
 * _ is the root/empty source path, denoting the whole integer value. */

enum Zarith { Small(i63), Big(BigInt) }
represented as split .[0,1] {
  | 1 from Small =>
    w64 with .[1,63] : (_.Small as w63) with .[0,1] : (= 1)
  | 0 from Big =>
    &<64, 8>(_.Big as BigInt) with .[0,1] : (= 0)
}

let small : Zarith = Small(42);

struct ZarithPair(Zarith, Zarith);
represented as {{ _.0 as Zarith, _.1 as Zarith }}

fn leq(p : ZarithPair) -> Bool {
  match p {
    (Small(x), Small(y)) => x <= y,
    (Big(x), Big(y)) => x <= y,
    (Small(_), _) => True,
    (_, Small(_)) => False,
  }
}

let small_pair : ZarithPair = (Small(8), Small(0));
let res1 : Bool = leq(p = small_pair);

let other_pair : ZarithPair = (Small(42), Big(1234567890));
let res2 : Bool = leq(p = other_pair);

fn incr(z: Zarith) -> Zarith {
  match z {
    Small(x) => Small(x+1),
    Big(x) => Big(x+1),
  }
}
let one : Zarith = incr(z=Small(0));

/* If present, the bench statement specifies
 * 1) a matching function whose execution should be timed (here, leq)
 * 2) between one and nine value identifiers on which the function should be applied.
 * The produced executable (compiled from LLVM IR)
 * will use the (i+1)-th value if i is its first argument,
 * where i is a **single digit**, or the first value if it is called with no arguments.
 */
bench leq {zero_pair; other_pair}
