(** Global settings *)

val heuristics : bool ref
val dot_output : bool ref
val show_output : bool ref
val ir_output : bool ref
val metrics : bool ref
