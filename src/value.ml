open Syntax

type t = Syntax.value =
  | VInt of Z.t
  | VRecord of (Name.t * t) list
  | VTuple of t list
  | VConstr of Name.t * t

let rec pp fmt = function
  | VInt z -> Fmt.pf fmt "%a" Z.pp_print z
  | VRecord l ->
    Fmt.pf fmt "{%a}" Fmt.(list ~sep:comma @@ pair ~sep:(any ": ") Name.pp pp) l
  | VTuple l ->
    Fmt.pf fmt "(%a)" Fmt.(list ~sep:comma pp) l
  | VConstr (constr, VTuple []) ->
    Fmt.pf fmt "%s" constr
  | VConstr (constr, VTuple args) ->
    Fmt.pf fmt "%s(%a)" constr Fmt.(list ~sep:comma pp) args
  | VConstr (constr, arg) ->
    Fmt.pf fmt "%s(%a)" constr pp arg

let rec focus (h0:Hole.t) v = match Hole.pop h0, v with
  | None, _ -> v
  | Some(HField n, hole), VRecord l ->
    focus hole (List.assoc n l)
  | Some(HAccess pos, hole), VTuple l ->
    focus hole (List.nth l pos)
  | Some(HConstr constr, hole), VConstr (constructor, argument)
    when constr = constructor ->
    focus hole argument
  | _ ->
    Report.fail "Mismatched holexpr types: %a disagrees with %a.@." Hole.pp h0 pp v
