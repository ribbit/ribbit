type in_args = {id: Target.in_id; ty: Types.ty; mty: Mem.ty; prov: Prov.t}

type out_args = {id: Target.out_id; ty: Types.ty; mty: Mem.ty}

val expr_to_mem : 
  MemoryTree.t File.logger -> Env.t -> in_args Name.Map.t ->
  Target.out_id -> 
  TypedExpr.t ->
  Target.program

val match_to_mem :
  MemoryTree.t File.logger -> Env.t -> in_args Name.Map.t ->
  Target.out_id ->
  TypedExpr.matching ->
  Target.program

(* TODO why do we need this function exposed? *)
val refine : Target.out_id -> Mem.Shape.t -> Mem.Shape.t -> Target.t
