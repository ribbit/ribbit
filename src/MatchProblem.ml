module ResSet = CCSet.Make(Int)
module ResMap = Map.Make(Int)

type 'a t = (Prov.t Name.Map.t * 'a) CCImmutArray.t

let view = CCImmutArray.to_list

let empty = CCImmutArray.empty
let fresh ~(p: Prov.t Name.Map.t) v =
  CCImmutArray.singleton (p, v)

(** [get_res res mp] returns the outputs in [mp] that appear in [res]
    in ascending order of id in [res]. *)
let get_res res (mp : _ t) =
  List.rev @@
  ResSet.fold
    (fun i outs -> CCImmutArray.get mp i :: outs)
    res []

let (++) = CCImmutArray.append
let concat_map f l =
  List.fold_left
    (fun s x -> s ++ f x)
    empty
    l

let map f a = CCImmutArray.map (fun (pat, v) -> pat, f pat v) a

let fold_ascending f (a : _ t) z =
  CCImmutArray.foldi (fun st b (p, v) -> f b p v st) z a

(** Import from branches indexed by provenances *)

let from_prov_branches (br: (Prov.t Name.Map.t * 'a) list) : 'a t =
  CCImmutArray.of_list br

(** Printers *)

let pp_with ?pp_sep pp_clause fmt x =
  CCImmutArray.pp ?pp_sep pp_clause fmt x

let pp pp_target fmt (x : _ t) =
  (* let pp_bind fmt bind = *)
  (*   if Name.Map.is_empty bind then () else *)
  (*     Fmt.pf fmt "@ binding @[<hov2>{@ %a@ }@]" *)
  (*       (Name.Map.pp ~sep:Fmt.semi ~bind:(Fmt.any "=") Hole.pp) bind *)
  (* in *)
  let pp_clause fmt (pats, t) =
    Fmt.pf fmt "| @[@[<hv>@[%a@]@ @]-> %a@]"
      (Name.Map.pp Prov.pp) pats
      pp_target t
  in 
  Fmt.pf fmt "@[<v2>  %a@]"
    (pp_with ~pp_sep:Fmt.cut pp_clause) x
