module type S = sig
  type t
  val name : string
  val constrs : Value.t Name.Map.t
  val ty : Types.ty
  val tydecl : Syntax.typedecl
  val is_value : Value.t -> bool
  val to_ : t -> Value.t
  val of_ : Value.t -> t
  val wrap : (t -> t) -> Value.t -> Value.t
  val wrap2 : (t -> t -> t) -> Value.t -> Value.t -> Value.t
end

let mk_enum (type ty) name (l : (string * ty) list) =
  let mk_constr tag = Syntax.VConstr (tag, VTuple []) in
  let module M = struct
    type t = ty
    let name = name
    let constrs =
      List.fold_left (fun m (tag, _) -> Name.Map.add tag (mk_constr tag) m)
        Name.Map.empty
        l
    let ty = Types.Sum (List.map (fun (tag, _) -> (tag, Types.Product [])) l)
    let is_value v =
      match v with
      | Syntax.VConstr (tag, VTuple []) -> Name.Map.mem tag constrs
      | _ -> false
    let to_ v =
      let tag, _ = List.find (fun (_, v') -> v = v') l in
      Name.Map.find tag constrs
    let of_ c = match c with
      | Syntax.VConstr (tag, VTuple []) -> List.assoc tag l
      | _ -> invalid_arg "not a constructor"
    let wrap f v = to_ @@ f @@ of_ v
    let wrap2 f v1 v2 = to_ @@ f (of_ v1) (of_ v2)
    let tydecl =
      {Syntax.
        name ;
        definition =
          Enum (List.map (fun (tag, _) -> (tag, (Tuple [] : Syntax.type_expr))) l);
        repr = Default ;
      }      
  end
  in
  (module M : S with type t = ty)

module Bool = struct
  let true_name = "True"
  let false_name = "False"
  include (val (mk_enum "Bool" [ false_name, false; true_name, true ]))
  let true_constr = Name.Map.find true_name constrs
  let false_constr = Name.Map.find false_name constrs
end

module Int = struct
  type t = Z.t
  let to_ i = Syntax.VInt i
  let of_ = function Syntax.VInt i -> i | _ -> invalid_arg "Not an integer"
  let wrap f v = to_ @@ f @@ of_ v
  let wrap2 f v1 v2 = to_ @@ f (of_ v1) (of_ v2)
end
