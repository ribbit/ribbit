(** Compilation of pattern matching via memory trees *)

type t
val pp_dot : (string * t) Fmt.t

val assemble :
  ?optimize:bool ->
  t File.logger -> Env.t ->
  (Target.in_id * Prov.t * Mem.ty) Name.Map.t ->
  Target.t MatchProblem.t -> Target.t

val from_provs : (* a.k.a. frog *)
  ?optimize:bool ->
  t File.logger -> Env.t ->
  (Target.in_id * Prov.t * Mem.ty) Name.Map.t ->
  (Prov.t Name.Map.t * Target.t) list -> Target.t
