type t =
  | EPos of Name.t * Hole.t
  | EConstr of Name.t * t
  | ETuple of t list
  | ERecord of (Name.t * t) list
  | EInt of Z.t

let rec subst s = function
  | EPos (n, h) ->
    begin match Name.Map.get n s with
      | None -> EPos (n, h)
      | Some (n', h') -> EPos (n', Hole.(h' ++ h))
    end
  | EConstr (n, pe) -> EConstr (n, subst s pe)
  | ETuple tes ->
    ETuple (List.map (subst s) tes)
  | ERecord tes ->
    ERecord (CCList.Assoc.map_values (subst s) tes)
  | EInt z -> EInt z

let rec erase_vars = function
  | EPos _ -> EPos ("dummy", Hole.Empty)
  | EConstr (k, e) -> EConstr (k, erase_vars e)
  | ETuple l -> ETuple (List.map erase_vars l)
  | ERecord l -> ERecord (List.map (fun (n, e) -> n, erase_vars e) l)
  | EInt z -> EInt z

let rec pp fmt e = match e with
  | EPos (x, pos) ->
    Fmt.pf fmt "%s%a" x Hole.pp_suffix pos
  | EConstr (constr, ETuple []) ->
    Fmt.pf fmt "%s" constr
  | EConstr (constr, ETuple args) ->
    Fmt.pf fmt "@[<2>%s(@,%a)@]" constr (Fmt.list ~sep:Fmt.comma pp) args
  | EConstr (constr, arg) ->
    Fmt.pf fmt "@[<2>%s(@,%a)@]" constr pp arg
  | ETuple l ->
    Fmt.pf fmt "@[<1>(@,%a)@]" (Fmt.list ~sep:Fmt.comma pp) l
  | ERecord l ->
    Fmt.pf fmt "@[<1>{@,%a}@]" Fmt.(list ~sep:comma @@ box @@ pair ~sep:(any ": ") Name.pp pp) l
  | EInt z -> Fmt.pf fmt "%a" Z.pp_print z

let rec focus (h0 : Hole.t) e0 = match Hole.pop h0, e0 with
  | None, _ -> e0
  | Some(HField n, hole), ERecord l ->
    focus hole @@ List.assoc n l
  | Some(HAccess pos, hole), ETuple l ->
    focus hole @@ List.nth l pos
  | Some(HConstr constr, hole), EConstr (constructor, argument)
    when constr = constructor ->
    focus hole argument
  | Some (HBits {offset; width}, hole), EInt z ->
    focus hole @@ EInt (Z.extract z offset width)
  | _ ->
    Report.fail "Mismatched holexpr types: %a ≠ %a@." Hole.pp h0 pp e0

(* return the tail end of the path in case we're stuck on a variable *)
let rec focus_below_expr (h0: Hole.t) e0 : t * (Hole.t option) =
  match Hole.pop h0, e0 with
  | None, _ -> e0, None
  | _, EPos _ -> e0, Some h0
  | Some(HField n, hole), ERecord l ->
    focus_below_expr hole @@ List.assoc n l
  | Some(HAccess pos, hole), ETuple l ->
    focus_below_expr hole @@ List.nth l pos
  | Some(HConstr constr, hole), EConstr (constructor, argument)
    when constr = constructor ->
    focus_below_expr hole argument
  | Some (HBits {offset; width}, hole), EInt z ->
    focus_below_expr hole @@ EInt (Z.extract z offset width)
  | _ ->
    Report.fail "Mismatched holexpr types: %a ≠ %a@." Hole.pp h0 pp e0

let gather_pos =
  let rec aux acc p_out = function
    | EPos (x_in, p_in) -> Hole.Map.add p_out (x_in, p_in) acc
    | ETuple l ->
      CCList.foldi (fun acc i e -> aux acc Hole.(p_out $. HAccess i) e) acc l
    | ERecord l ->
      List.fold_left (fun acc (x, e) -> aux acc Hole.(p_out $. HField x) e) acc l
    | EConstr (k, e) -> aux acc Hole.(p_out $. HConstr k) e
    | _ -> acc
  in
  aux Hole.Map.empty Hole.Empty

let occurrences x =
  let rec aux acc cur_pos = function
    | EPos (x', pos) when x = x' ->
      Hole.Map.add cur_pos pos acc
    | ETuple l ->
      CCList.foldi (fun acc i e -> aux acc Hole.(cur_pos $. HAccess i) e) acc l
    | ERecord l ->
      List.fold_left (fun acc (n, e) -> aux acc Hole.(cur_pos $. HField n) e) acc l
    | EConstr (k, e) ->
      aux acc Hole.(cur_pos $. HConstr k) e
    | _ -> acc
  in
  aux Hole.Map.empty Hole.Empty

let rec prov_of = function
  | EPos _ | EInt _ -> Prov.Top
  | ETuple l -> Tup (List.map prov_of l)
  | ERecord l -> Rec (List.map (fun (n, e) -> n, prov_of e) l)
  | EConstr (k, e) -> Cons (k, prov_of e)

