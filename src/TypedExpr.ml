type t = {
  ty : Types.ty ;
  mty : Mem.Ty.t ;
  desc : desc ;
}

and desc =
  | PathExpr of PathExpr.t
  | Let of Name.t * t * t
  | Matching of matching
  | Call of Name.t * Name.t Name.Map.t
  | Prim of Target.prim * Name.t list

and matching = { matchee : Name.t list ; clauses : (Prov.t list * t) list }

type funct = {
  arg : Name.t ;
  argty : Types.ty ;
  body : t ;
}

let rec subst s e =
  let desc = match e.desc with
  | PathExpr pe ->
    PathExpr (PathExpr.subst s pe)
  | Let (n, e, e') ->
    let s' = Name.Map.remove n s in
    Let (n, subst s e, subst s' e')
  | Matching {matchee ; clauses} ->
    let clauses = List.map (fun (p, e) -> p, subst s e) clauses in
    Matching {matchee; clauses}
  | Call (f, args) ->
    assert (not @@ Name.Map.exists (fun n _ -> Name.Map.mem n s) args);
    Call (f, args)
  | Prim (prim, l) ->
    assert (not @@ List.exists (fun n -> Name.Map.mem n s) l);
    Prim (prim, l)
  in
  {e with desc}


let mk_let n e1 e2 =
  { ty = e2.ty ; mty = e2.mty ; desc = Let (n, e1, e2) }
let rec simplify t =
  match t.desc with
  | Let (n, {desc = Let (n2, e11, e12) }, e2) ->
    simplify @@
    mk_let n2 e11 @@
    mk_let n e12 @@
    e2
  | Let (n, e1, e2) ->
    mk_let n (simplify e1) (simplify e2)
  | Matching {matchee; clauses} -> 
    let clauses = List.map (fun (p, t) -> (p, simplify t)) clauses in
    {t with desc = Matching {matchee; clauses}}
  | _ -> t


let rec pp fmt e = match e.desc with
  | PathExpr c -> PathExpr.pp fmt c
  | Let (n, e1, e2) ->
    Fmt.pf fmt "@[<v>@[<hv 2>let %a : %a =@ @[%a@]@];@ %a@]"
      Name.pp n Types.pp e1.ty pp e1 pp e2
  | Call (n, arg) ->
    Fmt.pf fmt "@[%s(%a)@]" n (Name.Map.pp Name.pp) arg
  | Prim (p, e) -> Fmt.pf fmt "@[%a(%a)@]" Target.pp_prim p (Fmt.list ~sep:Fmt.comma Name.pp) e
  | Matching { matchee; clauses } ->
    Fmt.pf fmt "@[<v 2>match @[%a@] {@ %a@;<0 -2>}@]"
      (Fmt.list ~sep:Fmt.comma Name.pp) matchee
      (Fmt.list pp_clause) clauses
and pp_clause fmt (pattern, expr) =
  Fmt.pf fmt "@[<2>%a =>@ %a,@]"
    (Fmt.list ~sep:Fmt.comma Prov.pp) pattern pp expr
