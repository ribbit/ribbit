let file _name lexbuf =
  Peahell.Input.wrap (Parser.program Lexer.token) lexbuf

let toplevel lexbuf =
  Peahell.Input.wrap (Parser.toplevel_phrase Lexer.token) lexbuf

let expect _name lexbuf =
  Peahell.Input.wrap (Parser.expect_file Lexer.token) lexbuf

let expect_delimiters = "(*EXPECT", "EXPECT*)"

let value s =
  Peahell.Input.wrap (Parser.value_top Lexer.token) @@ Lexing.from_string s
