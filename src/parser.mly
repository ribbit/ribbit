(* The parser *)

%{

open Input.Parse
open Syntax
   
let make_loc (startpos, endpos) = Location.Location (startpos, endpos)

type error +=
   | Unclosed of Location.loc * string * Location.loc * string
   | Doesnt_fit of Location.loc * Z.t
              
let unclosed opening_name opening_loc closing_name closing_loc =
  raise(Error(Unclosed(make_loc opening_loc, opening_name,
                       make_loc closing_loc, closing_name)))

(* Error reporting *)
let prepare_error = function
  | Error (Unclosed (opening_loc, _opening, closing_loc, closing)) ->
     Some (Report.errorf
        ~loc:closing_loc
        ~sub:[opening_loc]
        "Syntax error: '%s' expected" closing)
  | Error (Doesnt_fit (loc, i)) ->
     Some (Report.errorf
        ~loc
        "Syntax error: '%a' doesn't fit in an integer" Z.pp_print i)
  | _ -> None
let () = Report.register_report_of_exn prepare_error

  
%}

/* Tokens */
%token EOF
%token COMMA
%token SEMI
%token DOT
%token COLON
%token EQ
%token <string> UIDENT
%token <string> LIDENT
%token LPAREN RPAREN
%token LANGLE RANGLE
%token LACCO RACCO
%token LACCOACCO RACCOACCO
%token LBRACK RBRACK
%token BAR AMPER CARRET
%token PLUS MINUS MULT SLASH
%token LANGLEEQ RANGLEEQ EQEQ
%token AMPERAMPER BARBAR
%token LANGLELANGLE RANGLERANGLE
%token BANG
%token TYPE ENUM STRUCT
%token FUN LET MATCH BENCH REPRESENTED AS BY SPLIT WITH FROM
%token ARROW DOUBLEARROW
%token <Z.t> ZINT
%token <int> INTTY
%token <int> UINTTY
%token <int> WORDTY
%token <string> EXPECT
%token UNDERSCORE

%nonassoc SEMI
%right    BAR BARBAR
%right    AMPER AMPERAMPER
%right    CARRET
%left     EQEQ LANGLE LANGLELANGLE LANGLEEQ RANGLE RANGLERANGLE RANGLEEQ
%left     PLUS MINUS
%left     MULT SLASH
%nonassoc prec_unary_minus /* unary - */

/* Entry points */

%type <Syntax.command> command
%start program
%type <Syntax.program> program
%start toplevel_phrase
%type <Syntax.program> toplevel_phrase
%start expect_file
%type <(Syntax.program * int * int) list> expect_file

%start value_top
%type <Value.t> value_top

%type <Syntax.provenance> provenance
%%

program: list(item) EOF {$1}
toplevel_phrase: list(item) SEMI {$1}
item: l=command EXPECT? { l };

expect_file: expect_item* EOF {$1}
%inline expect_item:
  | l = command+ EXPECT { l, $endofs(l), $endofs($2) }
;

command: value_declaration | type_declaration | function_declaration | bench { $1 };

%inline value_binding:
  LET name=name COLON ty=type_expr EQ rhs=expr SEMI
  { name, ty, rhs }
value_declaration:
  vb=value_binding { ValDecl vb }
;

function_declaration:
  FUN name=name LPAREN arg_tys=comma_sep_list(named_field(type_expr)) RPAREN
  ARROW res_ty=type_expr LACCO body=expr RACCO
  { FunDecl { name; arg_tys=Name.Map.of_list arg_tys; res_ty; body } }
;

type_declaration:
  | TYPE name=constr EQ ty=type_expr SEMI repr=repr
    { TypeDecl {name; definition=Type_expr ty; repr} }
  | STRUCT name=constr tys=tuple(type_expr) SEMI repr=repr
    { TypeDecl {name; definition=Type_expr (Tuple tys); repr} }
  | STRUCT name=constr fields=record(type_expr) repr=repr
    { TypeDecl {name; definition=Type_expr (Record fields); repr} }
  | ENUM name=constr definition=enum_definition repr=repr
    { TypeDecl {name; definition; repr}}
;

enum_definition:
  LACCO l=comma_sep_list(data_constructor) RACCO
  { Enum l }
;

data_constructor:
  | constructor=constr
    { (constructor, Tuple []) }
  | constructor=constr argument=simple_type_expr
    { (constructor, argument) }
;

(* Type expressions *)
simple_type_expr:
  | name=constr
    { Type_constructor name }
  | l=record(type_expr)
    { Record l }
  | l=tuple(type_expr)
    { match l with | [x] -> x | l -> Tuple l }
  | width=INTTY { Integer {signed = true; width} }
  | width=UINTTY { Integer {signed = false; width} }
;

type_expr:
  | ty=simple_type_expr {ty}
;

(* Memory types *)
repr:
  | REPRESENTED AS mty=memty { Memty mty }
  | REPRESENTED BY n=name { Named n }
  | { Default }
;

simple_memty:
  | n=constr { TVar n }
  | LPAREN ty=memty RPAREN { ty }
  | LPAREN EQ v=ZINT RPAREN { TConst v }
  | w=WORDTY { TWord w }
  | p=path AS mty=simple_memty { TFrag (mty, p) }
  | AMPER LANGLE len=int COMMA align=int RANGLE LPAREN pointee=memty RPAREN
    { TPtr { len ; align ; pointee } }
  | LACCOACCO l=separated_nonempty_list(COMMA,memty) RACCOACCO
    { TStruct l }
  | SPLIT discrs=separated_nonempty_list(COMMA,mempath)
    LACCO branches=membranches RACCO
    { TSplit { discrs; branches } }

memty:
  | ty=simple_memty {ty}
  | ty=memty WITH p=mempath COLON spec=simple_memty
    { TWith (ty, p, spec) }
  | ty=memty WITH p=mempath EQ v=ZINT
    { TWith (ty, p, TConst v) }
  | ty=memty WITH p=mempath COLON LACCO branches=membranches RACCO
    { TWith (ty, p, TSplit { discrs = [Root] ; branches }) }

membranches: BAR? l = separated_list(BAR,membranch) { l }
membranch:
  | lhs=separated_nonempty_list(COMMA, discr_val)
    FROM provenance=provenance DOUBLEARROW ty=memty
    { {lhs; provenance; ty} }
discr_val:
  | w=ZINT { Some w }
  | UNDERSCORE { None }

provenance:
  | UNDERSCORE
    { Any }
  | l=tuple(provenance)
    { Tuple l }
  | l=record(provenance)
    { Record l }
  | k=constr
    { Constr (k, Any) }
  | k=constr arguments=tuple(provenance)
    { Constr (k, match arguments with [p] -> p | l -> Tuple l) }
  | p1=provenance BAR p2=provenance
    { Or (p1, p2) }

mempath:
  | DOT { Root }
  | p=mempath_non_empty { p }
%inline mempath_suffix:
  | DOT i=int { fun p -> Access (p, i) }
  | DOT LBRACK i=int COMMA j=int RBRACK { fun p -> Extract (p, i, j) }
  | DOT MULT { fun p -> Deref p }
mempath_non_empty:
  | f=mempath_suffix { f Root }
  | p=mempath_non_empty f=mempath_suffix { f p }

path:
  | UNDERSCORE { Hole.Empty }
  | hole=path DOT pos=int { Hole.(hole $. HAccess pos) }
  | hole=path DOT field=name { Hole.(hole $. HField field) }
  | hole=path DOT constr=constr
    { Hole.(hole $. HConstr constr) }
  | hole=path DOT LBRACK offset=int COMMA width=int RBRACK
    { Hole.(hole $. HBits {offset; width}) }

(* Expressions *)
clauses: l=comma_sep_list(clause) { l }
clause:
  | pattern=patterns DOUBLEARROW expr=expr { {pattern; expr} }

patterns:
  | l=separated_nonempty_list(COMMA,pattern)
    { match l with [p] -> p | l -> PTuple l }
simple_pattern:
  | i=ZINT { PInt i }
  | UNDERSCORE { PAny }
  | constructor=constr { PConstr (constructor, PTuple []) }
  | LPAREN p=patterns RPAREN { p }
  | l=record(pattern) { PRecord l }
  | var=name { PVar var }
pattern:
  | p=simple_pattern { p }
  | p1=pattern BAR p2=pattern { POr (p1, p2) }
  | constructor=constr p=simple_pattern
    { PConstr (constructor, p) }

simple_expr:
  | var=name { EVar var }
  | i=ZINT { EInt i }
  | l=tuple(expr)
    { match l with [e] -> e | l -> ETuple l }
  | l=record(expr) { ERecord l }
  | BANG e=simple_expr { EPrim1 (Not, e) }
  | constructor=constr { EConstr (constructor, ETuple []) }
  | LPAREN e=expr COLON ty=type_expr RPAREN { ECstr(e, ty) }
expr:
  | e=simple_expr { e }
  | MINUS e=expr %prec prec_unary_minus
    { EPrim1 (Minus, e) }
  | e1=expr op=binop e2=expr
    { EPrim2 (op, e1, e2) }
  | f=name LPAREN args=comma_sep_list(bound_field(expr)) RPAREN
    { EApp (f, Name.Map.of_list args) }
  | LET name=name EQ e1=expr SEMI e2=expr { ELet (name, e1, e2) }
  | vb=value_binding e2=expr { let n, ty, e = vb in ELet (n, ECstr (e, ty), e2) }
  | MATCH e=expr LACCO cls=clauses RACCO { EMatch (e, cls) }
  | constructor=constr LPAREN l=separated_list(COMMA,expr) RPAREN
    { let arguments = match l with [e] -> e | l -> ETuple l in
      EConstr (constructor, arguments) }

%inline binop:
  | PLUS {Add} | MINUS {Sub} | MULT {Mul} | SLASH {Div}
  | LANGLEEQ {Leq} | LANGLE {Le} | RANGLEEQ {Geq} | RANGLE {Ge} | EQEQ {Equal}
  | AMPERAMPER {And} | BARBAR {Or}
  | LANGLELANGLE {ShiftLeft} | RANGLERANGLE {ShiftRight}
  | AMPER {BitAnd} | BAR {BitOr} | CARRET {BitXor}

bench:
  BENCH matcher=name LACCO matchees=separated_nonempty_list(SEMI, name) RACCO
  { Bench (matcher, matchees) }

value_top: v=value EOF { v }
value:
  | constructor=constr arguments=value_args
    { Value.VConstr (constructor, arguments) }
  | l=tuple(value) { Value.VTuple l }
  | l=record(value) { Value.VRecord l }
  | i=ZINT { Value.VInt i }
value_args:
  | { VTuple [] }
  | LPAREN l=separated_list(COMMA,value) RPAREN
    { match l with [v] -> v | l -> VTuple l }

record(r): LACCO l=comma_sep_list(named_field(r)) RACCO { l };

tuple(r): LPAREN l=comma_sep_list(r) RPAREN { l };

named_field(r): name=name COLON l=r { name, l };

bound_field(r): name=name EQ l=r { name, l };

comma_sep_list(r):
  | /*empty*/ {[]}
  | COMMA {[]}
  | x=r {[x]}
  | x=r COMMA l=comma_sep_list(r) {x::l};

name: LIDENT {$1};
constr: UIDENT {$1};
int:
  | i=ZINT
    { try Z.to_int i
      with Z.Overflow -> raise (Error(Doesnt_fit(make_loc $loc(i),i)))
    }
