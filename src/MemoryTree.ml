open Mem

module ResSet = CCSet.Make(Int)
module ResMap = Map.Make(Int)

type res = ResSet.t
type t =
  | Leaf of res
  | Bud of Mem.ty * res
  | Par of t list
  | Switch of { discr: Name.t; path: Mem.path; cases: (Z.t * t) list; default: t option }
type tree = t

(* TODO: Use a real Hashcons library someday *)
module MkHashCons(M : Hashtbl.HashedType) = struct
  module Tbl = Hashtbl.Make(M)
  type 'a t = 'a Tbl.t
  let make () = Tbl.create 17
  let get = Tbl.find_opt
  let add tbl k v = Tbl.replace tbl k v
  let hashcons tbl k v =
    add tbl k v; v

  let memo f =
    let tbl = make () in
    let f_memo k = match get tbl k with
      | Some res -> res
      | None ->
        let res = f k in
        Tbl.add tbl k res ;
        res
    in
    `Memo f_memo
        
  let memo_rec f =
    let tbl = make () in
    let rec f_rec (t : M.t) =
      match get tbl t with
      | Some res -> res
      | None ->
        let res = f f_rec t in
        add tbl t res;
        res
    in
    `Memo f_rec
end
    
module HC = MkHashCons(struct
    type t = tree
    let equal = (=) (* TODO real = *)
    let hash = Hashtbl.hash
  end)
module HC2 = MkHashCons(struct
    type t = tree * tree
    let equal = (=) (* TODO real = *)
    let hash = Hashtbl.hash
  end)
module HCSet = MkHashCons(struct
    type t = ResSet.t * tree
    let equal = (=)
    let hash = Hashtbl.hash
  end)

let fold ~f_bud ~f_leaf ~f_par ~f_switch =
  let f f_rec t =
    let acc = match t with
      | Leaf l -> f_leaf l
      | Bud (ty, l) -> f_bud ty l
      | Par trs -> f_par @@ List.map (fun tr -> f_rec tr) trs
      | Switch { discr; path; cases; default } ->
        f_switch discr path
          (List.map (fun (z, tr) -> z, f_rec tr) cases)
          (Option.map f_rec default)
    in
    acc
  in
  let `Memo f = HC.memo_rec f in
  f


let map_leaves_no_memo
    ?(bud=fun t r -> Bud(t, r))
    ?(leaf=fun r -> Leaf r) =
  let rec f = function
    | Leaf res -> leaf res
    | Bud (t, res) -> bud t res
    | Par l -> Par (CCList.map f l)
    | Switch { discr; path; cases; default } ->
      Switch { discr; path;
               cases = CCList.map (fun (i, l) -> i, f l) cases ;
               default = Option.map f default }
  in
  f

let map_leaves
    ?(bud=fun t r -> Bud(t, r))
    ?(leaf=fun r -> Leaf r) () =
  let f f_rec = function
    | Leaf res -> leaf res
    | Bud (t, res) -> bud t res
    | Par l -> Par (CCList.map f_rec l)
    | Switch { discr; path; cases; default } ->
      Switch { discr; path;
               cases = CCList.map (fun (i, l) -> i, f_rec l) cases ;
               default = Option.map f_rec default }
  in
  HC.memo_rec f

(* let map_leaves
 *     ?(nop=Nop)
 *     ?(bud=fun t r -> Bud(t, r))
 *     ?(leaf=fun r -> Leaf r) () =
 *   let tbl = HC.make () in
 *   let rec aux (t : t) =
 *     match HC.get tbl t with
 *     | Some x -> x
 *     | None ->
 *       let res = match t with
 *         | Leaf res -> leaf res
 *         | Bud (t, res) -> bud t res
 *         | Nop -> nop
 *         | Par l -> Par (CCList.map aux l)
 *         | Switch { path; cases; default } ->
 *           Switch { path;
 *                    cases = CCList.map (fun (i, l) -> i, aux l) cases ;
 *                    default = Option.map aux default }
 *       in
 *       HC.hashcons tbl t res
 *   in
 *   `Memo aux *)

let share = 
  let tbl = HC.make () in
  let rec aux dt =
    match HC.get tbl dt with
    | Some dt -> dt
    | None ->
      let dt = match dt with
        | Leaf _ | Bud _ -> dt
        | Par l ->
          Par (List.map (fun t -> aux t) l)
        | Switch {discr; path; cases; default} ->
          let cases = List.map (fun (w, dt) -> (w, aux dt)) cases in
          let default = Option.map aux default in
          Switch {discr; path; cases; default}
      in
      HC.hashcons tbl dt dt
  in
  aux

let pp fmt =
  let rec aux fmt =
    let pp_res fmt r =
      ResSet.pp
        ~pp_start:(Fmt.any "@[{") ~pp_stop:(Fmt.any "}@]")
        ~pp_sep:(Fmt.any ",@ ")
        Fmt.int fmt r
    in
    (* let pp_res fmt {MatchProblem. pat; expr; bind } =
     *   Fmt.pf fmt "%a (%a) {%a}"
     *     Syntax.pp_expr expr
     *     Fmt.(list int) or_brs
     *     (Name.Map.pp Fmt.string Mem.Path.pp) bind
     * in *)
    function
    | Bud (ty, l) ->
      Fmt.pf fmt "@[Bud(%a : %a)@]" Mem.Ty.pp ty pp_res l
    | Leaf res ->
      Fmt.pf fmt "@[Leaf(%a)@]"
        pp_res res
    | Par brs ->
      Fmt.pf fmt "@[<v>Par%a@]"
        Fmt.(list ~sep:nop @@ (any "@,- " ++ aux)) brs
    | Switch { discr; path; cases; default } ->
      Fmt.pf fmt "@[<v>@[Switch(%s%a)@]%a%a@]" discr Mem.Path.pp_suffix path
        Fmt.(list ~sep:nop @@
             (fun fmt (w, tr) ->
                pf fmt "@,| %a → %a"
                  Z.pp_print w
                  aux tr))
             cases
        Fmt.(option (fun fmt arg -> pf fmt "@,| _ → %a" aux arg)) default
  in aux fmt

module Dot = struct
  type node =
    | DBud of Mem.ty * res
    | DLeaf of res
    | DPar
    | DSwitch of Name.t * Mem.path * (int * Z.t option) list

  let as_dot_graph tr =
    let r = ref 0 in
    let new_id () = incr r; !r in
    let vertices : (int * node) list ref = ref [] in
    let edges : (int * int option * int) list ref = ref [] in
    let add_vertex id x = vertices := (id, x) :: !vertices in
    let add_edge src sub tgt = edges := (src, sub, tgt) :: !edges in
    let f_leaf l =
      let id = new_id () in
      add_vertex id (DLeaf l);
      id
    and f_bud ty l =
      let id = new_id () in
      add_vertex id (DBud (ty, l));
      id
    and f_par children =
      let id = new_id () in
      let children = List.mapi (fun i id' -> i, id') children in
      add_vertex id DPar;
      List.iter (fun (_i, child) -> add_edge id None child) children;
      id
    and f_switch discr pa cases default =
      let id = new_id () in
      let branches =
        (match default with None -> [] | Some id' -> [0, None, id'])
        @ List.mapi (fun i (z, id') -> i, Some z, id') cases
      in
      add_vertex id (DSwitch (discr, pa, List.map (fun (i, lhs, _) -> i, lhs) branches));
      List.iter (fun (i, _, id') -> add_edge id (Some i) id') branches;
      id
    in
    let _ = fold ~f_leaf ~f_bud ~f_par ~f_switch tr in
    List.rev !vertices, !edges

  let pp_bindings fmt bindings = 
    let f fmt (n,e) =
      Fmt.pf fmt {|%a = %a\l|} Name.pp n Mem.Path.pp e
    in
    Fmt.pf fmt "%a"
      (Fmt.iter_bindings ~sep:Fmt.nop Name.Map.iter f)
      bindings

  let pp_vertex ppf (id, node) =
    let pp_res fmt r =
      ResSet.pp
        ~pp_start:(Fmt.any "@[{") ~pp_stop:(Fmt.any "}@]")
        ~pp_sep:(Fmt.any ",@ ")
        Fmt.int fmt r
    in      
    let attrs =
      match node with
      | DLeaf res ->
        let label = Fmt.str "{%a}" pp_res res in
        [ ("label", label);
          ("style","filled");
          ("fillcolor","olivedrab2");
          ("shape", "Mrecord") ]
      | DBud (ty, res) ->
        let label = Fmt.str "{%a|%a}" Ty.pp ty pp_res res in
        [ ("label", label);
          ("style","filled");
          ("fillcolor","yellow");
          ("shape", "Mrecord") ]
      | DPar ->
        let label = Fmt.str "Par" in
        [ ("label", label); ("shape", "record") ]
      | DSwitch (discr, pa, branches) ->
        let pp_branch fmt (i, lhs) = (match lhs with
            | Some z -> Fmt.pf fmt "<%i> %a" i Z.pp_print z
            | None -> Fmt.pf fmt "<%i> _" i) in
        let label = Fmt.str "{ Switch %s%a | { %a } }"
            discr
            Mem.Path.pp pa
            Fmt.(list ~sep:(any " | ") pp_branch) branches in
        [ ("label", label); ("shape", "record") ]
    in
    let pp_attr ppf (field, v) = Fmt.pf ppf {|%s="%s"|} field v in
    Fmt.pf ppf "%d [%a];" id (Fmt.list ~sep:(Fmt.any ", ") pp_attr) attrs

  let pp_edge ppf (src, sub, tgt) =
    match sub with
    | Some sub -> Fmt.pf ppf {|%d:%i:s -> %d:n;|} src sub tgt
    | None ->  Fmt.pf ppf {|%d:s -> %d:n;|} src tgt

  let pp ppf (name, tr) =
    let vertices, edges = as_dot_graph tr in
    Fmt.pf ppf {|@[<v2>digraph G {@,concentrate=true;rankdir="TD";@,%a@,@,%a@,labelloc="t";@,label="%s"@,}@]|}
      (Fmt.list ~sep:Fmt.cut pp_vertex) vertices
      (Fmt.list ~sep:Fmt.cut pp_edge) edges
      name
end
let pp_dot = Dot.pp


(** {2 Scaffolding}

    Turn a type into a template tree
*)

let rec scaffold env discr path : Mem.ty -> t = function
  | TVar (n, specs) ->
    let mty = Ty.expand_var n env ~specs in
    scaffold env discr path mty
  | TFrag frag ->
    Bud (Ty.expand_frag frag, ResSet.empty)
  | TConst z ->
    Leaf ResSet.empty
  | TWord {width; specs} ->
    let val_switch =
      Switch { discr; path; cases = []; default = Some (Leaf ResSet.empty) }
    in
    let branches = scaffold_specs env discr path specs in
    Par (val_switch :: branches)
  | TPtr { width; align; pointee; specs } ->
    (* INVARIANT deref before spec *)
    let branches = scaffold_specs env discr path specs in
    let deref_branch = scaffold env discr Path.(path $. Deref) pointee in
    Par (deref_branch :: branches)
  | TStruct fields ->
    let trs =
      List.mapi (fun i ty -> scaffold env discr Path.(path $. Access i) ty) fields
    in
    Par trs
  | TSplit { discrs; branches } ->
    let brs = List.map (fun br -> scaffold env discr path br.Ty.ty) branches in
    let trs = List.mapi (fun i rpath ->
        let path = Path.(path ++ rpath) in
        let cases = List.mapi
          (fun j br -> List.nth br.Ty.lhs i, List.nth brs j)
          branches
        in
        let default = List.find_map
          (function | (None, tr) -> Some tr | _ -> None)
          cases
        in
        let cases = List.filter_map
          (function | (Some z, tr) -> Some (z, tr) | _ -> None)
          cases
        in
        Switch {discr; path; cases; default})
      discrs in
    Par trs
and scaffold_specs 
  : type a . _ -> Name.t -> path -> Ty.specs -> _
  = fun env discr path specs ->
    (* INVARIANT: by ascending orders of path *)
    PathMap.to_seq specs
    |> OSeq.map (fun (rpath, spec) -> scaffold env discr Mem.Path.(path ++ rpath) spec)
    |> OSeq.to_list


let populate r0 =
  map_leaves_no_memo
    ~leaf:(fun r -> assert (ResSet.is_empty r); Leaf r0)
    ~bud:(fun n r -> assert (ResSet.is_empty r); Bud (n, r0))
    

(** {2 Weaving}

    Weave a pattern (and its type) onto a base tree
*)

let weave_pattern res_id discr_id
    (prov0 : Mem.prov) mem_env ty0 (tr0 : t) : t =
  let rec aux : _ -> Mem.prov -> Ty.t -> t -> t =
    fun path0 prov0 ty0 tr0 ->
      let error () =  
        Report.fail "@[<v 2>Incompatible weave:@ @[%a@]@;<1 -2>with type@ @[%a@]@;<1 -2>on@ %a@]@."
          MProv.pp prov0
          Mem.Ty.pp ty0
          pp tr0
      in 
      match prov0, ty0, tr0 with
      | pat, TVar (n, specs), tr ->
        aux path0 pat (Ty.expand_var ~specs n mem_env) tr
      | POr (p, p'), _, _ ->
        aux path0 p' ty0 (aux path0 p ty0 tr0)
      | _, TConst w, Leaf res -> Leaf (ResSet.add res_id res)
      | PAny, TFrag _, Bud (mty, res) ->
        Bud (mty, ResSet.add res_id res)
      | p, TFrag _, Bud (mty, res) ->
        let tr = scaffold mem_env discr_id path0 mty in
        let tr = populate res tr in
        aux path0 p mty tr
      | p, TFrag frag, tr ->
        aux path0 p (Ty.expand_frag frag) tr
      | PAny, TStruct l, Par trs ->
        let l = OSeq.to_list @@ OSeq.zip_index @@ OSeq.of_list l in
        Par (List.map2 (fun (i, ty) t ->
            aux Path.(path0 $. Access i) PAny ty t
          ) l trs) 
      | PStruct pats, TStruct tys, Par trs ->
        let pats = OSeq.of_list pats in
        let tys = OSeq.of_list tys in
        let pattys = OSeq.to_list @@ OSeq.zip_index @@ OSeq.zip pats tys in
        Par (List.map2 (fun (i, (pat, ty)) t ->
            aux Path.(path0 $. Access i) pat ty t
          ) pattys trs) 
      | PAny,
        TWord {width; specs},
        Par (Switch { discr; path ; cases ; default = Some (Leaf default_res) } :: ts) ->
        (* INVARIANT: first par is a switch on the constant value *)
        (* INVARIANT: par by ascending orders of path *)
        let switch =
          let default = Some (Leaf (ResSet.add res_id default_res)) in
          let cases =
            List.map (function
                | w, Leaf res -> w, Leaf (ResSet.add res_id res)
                | _ -> error()
              ) cases
          in
          Switch { discr; path; cases; default }
        in 
        let specs =
          let l = OSeq.to_list @@ PathMap.to_seq specs in (* Ascending order! *)
          List.map2 (fun (rpath, ty) t ->
              aux Path.(path0 ++ rpath) PAny ty t
            ) l ts
        in 
        Par (switch :: specs) 
      | PWord {width; specs},
        TWord {width = width'; specs = specs'},
        Par (Switch { discr; path ; cases ; default = Some (Leaf default_res) }  :: ts) ->
        (* INVARIANT: first par is a switch on the constant value *)
        (* INVARIANT: par by ascending orders of path *) 
        assert (width = width');
        let pat_and_tys = PathMap.merge_safe ~f:(fun k -> function
            | `Both (p, ty) -> Some (p, ty)
            | `Left _ | `Right _ -> error())
            specs specs'
        in 
        let switch =
          let default = Some (Leaf (ResSet.add res_id default_res)) in
          let cases =
            List.map (function
                | w, Leaf res -> w, Leaf (ResSet.add res_id res)
                | _ -> error()
              ) cases
          in
          Switch { discr; path; cases; default }
        in 
        let specs =
          let l = OSeq.to_list @@ PathMap.to_seq pat_and_tys in (* Ascending order! *)
          List.map2 (fun (rpath, (p, ty)) t ->
              aux Path.(path0 ++ rpath) p ty t
            ) l ts
        in
        Par (switch :: specs) 
      | PValue w,
        TWord {width; specs},
        Par (Switch { discr; path; cases; default = Some (Leaf default_res) } :: ts) ->
        (* INVARIANT: first par is a switch on the constant value *)
        (* INVARIANT: par by ascending orders of path *)
        let switch =
          let cases = CCList.Assoc.update ~eq:Z.equal
              ~f:(function None -> Some (Leaf (ResSet.add res_id default_res))
                         | Some (Leaf res) -> Some (Leaf (ResSet.add res_id res))
                         | Some _ -> error())
              w cases
          in
          Switch { discr; path; cases; default = Some (Leaf default_res) }
        in 
        let specs =
          let l = OSeq.to_list @@ PathMap.to_seq specs in (* Ascending order! *)
          List.map2 (fun (rpath, ty) t ->
              aux Path.(path0 ++ rpath) PAny ty t
            ) l ts
        in 
        Par (switch :: specs) 
      | PAny, TPtr { width; align; pointee; specs }, Par (pointee_tr :: trs) ->
        (* INVARIANT deref before spec *)
        (* INVARIANT: par by ascending orders of path *)
        let specs = OSeq.to_list @@ PathMap.to_seq specs in
        let trs' = List.map2 (fun (rpath, ty) t ->
            aux Path.(path0 ++ rpath) PAny ty t
          ) specs trs
        in
        Par (aux Path.(path0 $. Deref) PAny pointee pointee_tr :: trs') 
      | PPtr pat, TPtr ty, Par (pointee_tr :: trs) ->
        assert (pat.width = ty.width && pat.align = ty.align);
        let specmap = PathMap.merge_safe ~f:(fun k -> function
            | `Both (p, ty) -> Some (p, ty)
            | `Left _ | `Right _ -> assert false) pat.specs ty.specs
        in 
        (* INVARIANT deref before spec *)
        (* INVARIANT: par by ascending orders of path *) 
        let specs = OSeq.to_list @@ PathMap.to_seq specmap in
        let trs' = List.map2 (fun (rpath, (pat, ty)) t ->
            aux Path.(path0 ++ rpath) pat ty t
          ) specs trs
        in
        Par (aux Path.(path0 $. Deref) pat.pointee ty.pointee pointee_tr :: trs')
      | pat,
        TSplit { discrs; branches },
        Par pars ->
        (* only consider branches whose type agrees with the pattern
         * on ALL discriminants *)
        let discrs = List.mapi (fun i dpath ->
            let domain = List.fold_left
              (fun acc Ty.{lhs; _} -> match List.nth lhs i with
                | Some z -> ZSet.add z acc
                | None -> acc)
              ZSet.empty branches
            in
            dpath, domain)
          discrs
        in
        let branch_matches_pat = fun Ty.{lhs; provs; ty} ->
          List.for_all2 (fun (dpath, domain) dval ->
              match dval, MProv.(possible_ints @@ focus dpath pat) with
              | Some word, `Set set -> ZSet.mem word set
              | Some word, `NonZero -> word <> Z.zero
              | None, `Set set -> (* set must NOT be fully within other dvals *)
                ZSet.exists (fun z -> not (ZSet.mem z domain)) set
              | _ -> true)
            discrs lhs
        in
        (* in each switch tree, explore subtrees that match
         * at least one branch that matches our current pattern
         * (i.e., whose discr value is not completely foreign) *)
        let pars = List.mapi (fun i -> function
            | Switch {discr; path; cases; default} ->
              let cases = List.map (fun (w, tr) ->
                  let matching_branches = List.filter (fun br ->
                      List.nth br.Ty.lhs i = Some w &&
                      branch_matches_pat br)
                    branches
                  in
                  match matching_branches with
                  | [] -> w, tr
                  | [Ty.{lhs; provs; ty}] -> w, aux path0 prov0 ty tr
                  | _ -> failwith "should not happen")
                cases
              in
              let default = Option.map (fun tr ->
                  let matching_branches = List.filter (fun br ->
                      List.nth br.Ty.lhs i = None &&
                      branch_matches_pat br)
                    branches
                  in
                  match matching_branches with
                  | [] -> tr
                  | [Ty.{lhs; provs; ty}] -> aux path0 prov0 ty tr
                  | _ -> failwith "should not happen")
                default
              in
              Switch {discr; path; cases; default}
            | _ -> error ())
          pars
        in
        Par pars
      | _ -> error()
  in
  try 
    aux Path.Empty prov0 ty0 tr0
  with exn ->
    Report.debugf "@[<v 2>Weaving failed for@ @[%a@]@;<1 -2>on@ %a@]@."
      MProv.pp prov0
      pp tr0;
    raise exn

(** {2 Trimming}

    Trim buds into leaves
*)
let `Memo trim = map_leaves ~bud:(fun _ r -> Leaf r) ()

module Heuristics = struct
  type t = ResSet.t -> tree -> int

  let rec outputs acc = function
    | Leaf res | Bud (_, res) -> ResSet.union res acc
    | Par [] -> acc
    | Par (tr :: trs) ->
      List.fold_left
        (fun acc tr -> ResSet.inter acc (outputs acc tr))
        (outputs acc tr) trs
    | Switch { discr; path; cases; default } ->
      List.fold_left
        (fun acc (_, tr) -> ResSet.union acc (outputs acc tr))
        (Option.fold ~none:acc ~some:(fun tr -> outputs acc tr) default)
        cases
  let default_outputs tr =
    let all_outs = outputs ResSet.empty tr in
    let rec aux = function
      | Leaf res | Bud (_, res) -> res
      | Par [] -> all_outs
      | Par (tr :: trs) ->
        List.fold_left
          (fun acc tr -> ResSet.inter acc (aux tr))
          (aux tr) trs
      | Switch { discr; path; cases; default } ->
        List.fold_left
          (fun acc (_, tr) -> ResSet.inter acc (aux tr))
          (Option.fold ~none:all_outs ~some:aux default)
          cases
    in aux tr
  
  let select_best (f : t) (children : tree list) =
    let rec aux current_max current_bests children =
      let outs = List.fold_left outputs ResSet.empty children in
      match children with
      | [] -> current_bests
      | tr :: rest ->
        let score = f outs tr in
        if score > current_max then aux score [tr] rest
        else if score = current_max then
          aux current_max (tr :: current_bests) rest
        else aux current_max current_bests rest
    in
    List.rev @@ aux min_int [] children

  let choose_with_heuristics heuristics trs0 =
    let rec aux (heuristics : t list) trs = 
      match heuristics with
      | [] -> CCList.hd trs
      | f :: rest ->
        let trs = select_best f trs in
        match trs with
        | [x] -> x
        | _ -> aux rest trs
    in
    let x = aux heuristics trs0 in
    let l = CCList.remove ~eq:(=) ~key:x trs0 in
    assert (List.length l < List.length trs0);
    x, l

  let first_res : t = fun outs tr ->
    if ResSet.is_empty outs then 0 else
    let res0 = ResSet.min_elt outs in
    if ResSet.mem res0 @@ default_outputs tr
    then 0
    else 1

  let small_default : t = fun outs tr ->
    - (ResSet.cardinal @@ default_outputs tr)

  let small_branching_factor : t = fun outs tr ->
    match tr with
    | Switch { discr; path; cases; default } ->
      - (List.length cases) - (match default with None -> 0 | Some _ -> 1)
    | _ -> -1

  let arity : t = fun outs tr ->
    let aux = function
      | Par trs -> List.length trs
      | _ -> 1
    in match tr with
    | Switch { discr; path; cases; default } ->
      let case_arities = List.map (fun (_, tr) -> aux tr) cases
      and deft_arity = Option.fold ~none:0 ~some:aux default in
      List.fold_left (-) deft_arity case_arities
    | _ -> - (aux tr)

  let leaf_edge : t = fun outs tr ->
    let rec is_leaf = function
      | Switch _ -> false
      | Par trs -> List.for_all is_leaf trs
      | _ -> true
    in match tr with
    | Switch { discr; path; cases; default } ->
      let case_leaves = List.map (fun (_, tr) -> if is_leaf tr then 1 else 0) cases
      and deft_leaves =
        Option.fold ~none:0 ~some:(fun tr -> if is_leaf tr then 1 else 0) default in
      List.fold_left (+) deft_leaves case_leaves
    | _ -> if is_leaf tr then 1 else 0

  let fewer_outputs : t = fun outs tr ->
    - (ResSet.cardinal @@ outputs ResSet.empty tr)

  (* max res in outs such that for all res' <= res, res' not in default_outputs tr *)
  let non_default_prefix : t = fun outs tr ->
    match ResSet.min_elt_opt (default_outputs tr) with
    | None -> ResSet.cardinal outs
    | Some res -> ResSet.cardinal @@ ResSet.filter (fun res' -> res' < res) outs

  (* mem-specific heuristic *)
  let more_derefs : t = fun outs tr ->
    let rec collect_discrs = function
      | Switch { discr; path; cases; default } ->
        let case_discrs = List.concat_map (fun (_, tr) -> collect_discrs tr) cases
        and deft_discrs = Option.fold ~none:[] ~some:collect_discrs default in
        (Path.to_rev_list path) :: (deft_discrs @ case_discrs)
      | Par trs -> List.concat_map collect_discrs trs
      | _ -> []
    and count_derefs = List.fold_left (fun acc x -> if x = Path.Deref then 1 + acc else acc) in
    List.fold_left count_derefs 0 @@ List.sort_uniq Stdlib.compare @@ collect_discrs tr

  let choose =
    choose_with_heuristics @@ if !Globals.heuristics
      then [non_default_prefix; small_branching_factor; arity; more_derefs]
      else []
end

(* crazy idea: try them all! *)
module Exhaust = struct

  let measure f (metric : t -> float) trs =
    List.mapi
      (fun i tr ->
        Report.debugf "%i / %i@." i (List.length trs);
        let other_trs = CCList.remove ~eq:(=) ~key:tr trs in
        let score = metric @@ f tr other_trs in
        tr, other_trs, score)
      trs

  let choose_min f (metric : t -> float) trs =
    let l = List.sort
      (fun (_, _, s1) (_, _, s2) -> Float.compare s1 s2)
      (measure f metric trs) in
    match l with
    | (tr, other_trs, _) :: _ -> tr, other_trs
    | _ -> assert false

  let choose_max f (metric : t -> float) trs =
    let l = List.sort
      (fun (_, _, s1) (_, _, s2) -> - (Float.compare s1 s2))
      (measure f metric trs) in
    match l with
    | (tr, other_trs, _) :: _ -> tr, other_trs
    | _ -> assert false

end

(** [specialize res tr] filters out leaves that are not in [res]. *)
let specialize res0 =
  let `Memo f =
    map_leaves
      ~leaf:(fun res -> Leaf (ResSet.inter res res0))
      ()
  in
  f

(** [graft tr tr'] appends [tr'] to [tr]'s leaves *)
let graft =
  let f tr' = 
    let `Memo f =
      map_leaves
        ~leaf:(fun res -> specialize res tr')
        ()
    in f
  in
  (* Deal with empty Par nodes separately so as to avoid yeeting innocent leaves *)
  fun tr tr' -> if tr' = Par [] then tr else f tr' tr

(** Collapsing constructors, only to be used after trimming *)

let par l =
  let rec aux acc = function
    | [] -> CCList.uniq ~eq:(=) acc
    | Par trs :: rest -> aux acc (trs @ rest)
    | h :: rest -> aux (h::acc) rest
  in
  match aux [] l with
  | [tr] -> tr
  | trs -> Par (trs)

let switch ~default discr path cases = 
  match default, cases with
  | None, [] -> assert false
  | Some dt, [] -> dt
  | None, [_, dt] -> dt
  | Some br, cases ->
    if List.for_all (fun (_, next) -> br = next) cases then br
    else Switch { discr; path; cases; default }
  | None, (_, h)::t -> 
    if List.for_all (fun (_, next) -> h = next) t then h
    else Switch { discr; path; cases; default }

(** {2 Collapsing}

    Yeet switch-nodes that inspect already inspected paths
*)
module PosMap = CCMap.Make(struct
    type t = Name.t * Path.t
    let compare = compare
  end)

let collapse_switch : t -> t =
  let rec aux env = function
    | Leaf _ | Bud _ as tr -> tr
    | Par trs -> par (List.map (fun tr -> aux env tr) trs)
    | Switch { discr; path; cases; default } ->
      match PosMap.get (discr, path) env with
      | Some z -> (match List.assoc_opt z cases, default with
          | Some tr, _ -> aux env tr
          | None, Some tr -> aux env tr
          | None, None -> Leaf ResSet.empty)
      | None ->
        let cases =
          List.map (fun (z, tr) ->
              z, aux (PosMap.add (discr, path) z env) tr) cases
        in
        let default = Option.map (aux env) default in
        switch ~default discr path cases
  in
  let `Memo f = HC.memo @@ aux PosMap.empty in
  f

(** {2 Prune}

    Prune dead code
*)
let prune : t -> t =
  let get_each_leaf trs =
    let exception Nope in
    try Some (List.map (function Leaf res -> res  | _ -> raise Nope) trs)
    with Nope -> None
  in
  let f f_rec = 
    function
    | Par trs ->
      let trs = List.map f_rec trs in
      begin match get_each_leaf trs with
        | Some [] -> Par []
        | Some (h::t) -> Leaf (List.fold_left ResSet.inter h t)
        | None -> par trs
      end
    | Switch { cases = []; default = None; _ } -> Leaf ResSet.empty
    | Switch { discr; path; cases; default } ->
      let cases = List.map (fun (z, tr) -> z, f_rec tr) cases
      and default = Option.map f_rec default in
      begin match get_each_leaf (Option.to_list default @ List.map snd cases) with
        | Some (h::t) when List.for_all (fun x -> x = h) t ->
          Leaf h 
        | _ -> switch discr path cases ~default
      end
    | tr -> tr
  in
  let `Memo f = HC.memo_rec f in
  f

(** [sequentialise choose t] removes [Par] nodes through
  the method specified by [choice] (e.g., heuristics) *)
let rec sequentialise choice : t -> t =
  (* hashconsing saves the day! *)
  let f f_rec tr =
    let tr' = match tr with
      | Switch { discr; path; cases; default } ->
        switch discr path
          (CCList.map (fun (z, tr) -> z, f_rec tr) cases)
          ~default:(Option.map f_rec default)
      | Par [] -> Par []
      | Par [tr] -> f_rec tr
      | Par (tr :: other_trs) ->
        (* let tr, other_trs =
          (* quick and dirty fix to avoid selecting benign empty par nodes:
           * just yeet them altogether *)
          let trs = List.filter (function | Par [] -> false | _ -> true) trs in
          begin match choice with
          | `Heuristic -> Heuristics.choose trs
          | `Exhaust to_dt ->
            let metric = fun tr -> Dtree.Metrics.code_size @@ to_dt tr in
            Exhaust.choose_min
              (fun tr other_trs -> f_rec @@ graft tr @@ f_rec @@ par other_trs)
              metric trs
          end
        in *)
        f_rec @@ collapse_switch @@ graft tr @@ f_rec @@ par other_trs
      | Leaf _ | Bud _ -> tr
    in
    prune @@ collapse_switch tr'
  in
  let `Memo f = HC.memo_rec f in
  f

(** {2 Blossom}

    Restores the leaves in [tr] with the outputs in the
    match problem [mp].
*)
let to_target_expr mp env tr : Target.t =
  let aux aux t = match t with
    | Bud _ | Par _ ->
      Target.Fail
    | Leaf res ->
      begin match MatchProblem.get_res res mp with
        | (_p, t) :: _ -> t
        | [] -> Target.Fail
      end
    | Switch { discr; path; cases; default } ->
      let cases =
        List.map (fun (z, tr) ->
            (* refine root_mty with newly available info *)
            (* let w = Mem.Ty.TConst z in *)
            (* let ty' = Mem.Ty.with_spec Mem.Ty.override_spec mty path w in *)
            z, aux tr)
        cases
      and default = Option.map (fun t -> aux t) default in
      let sid = Target.IId discr in
      let sid' = Target.hashed_src (sid, path) in
      let cont = Target.switch sid' cases ?default in
      Target.Let (SubIn (sid', sid, path), cont)
  in
  let `Memo f = HC.memo_rec aux in
  f tr


let pp_arg fmt (id, prov, mty) =
  Fmt.pf fmt "{id: %a; prov: %a; mty: %a}"
    Target.pp_iid id Prov.pp prov Ty.pp mty

let assemble_one
    (logger: t File.logger) env v (Target.IId discr, prov, mty) branches =
  let mty = Ty.filter ~prov env mty in
  let t0 =
    logger#app
      (Fmt.str "Scaffold_%a.dot" Name.pp v) "Scaffold"
      (scaffold env discr Empty) mty
  in
  MatchProblem.fold_ascending (fun i ps _tr trs ->
      let prov = Prov.intersect prov @@ Name.Map.find v ps in
      match prov with
      | None -> Par []
      | Some prov ->
        let mprov = Mem.MProv.to_mem env mty prov in
        logger#app
          (Fmt.str "Weave_%a%i.dot" Name.pp v i) "Weave"
          (weave_pattern i discr mprov env mty) trs
    )
    branches
    t0

(* handle splits in the parent or target mem ty using the pat match compilation alg *)
let assemble ?(optimize=true) (logger: t File.logger) env args branches =
  Report.(enter ~__LINE__ ~__FILE__ "Assemble"
            ~args:["args", d (Name.Map.pp pp_arg) args;
                   "problem", d (MatchProblem.pp Fmt.nop) branches])
  @@ fun _ ->
  match MatchProblem.view branches with
  | [] -> Target.Fail
  | [_prov, t] -> t
  | _ ->
    let match_trees =
      Name.Map.mapi (fun v arg -> assemble_one logger env v arg branches) args
    in
    let tree = share (Par (List.map snd @@ Name.Map.bindings match_trees))
      |> logger#app "Trim.dot" "Trim" trim
      |> logger#app "Collapse.dot" "Final Collapse" collapse_switch
      |> logger#app "Sequentialize.dot" "Sequentialize" (sequentialise `Heuristic)
    in
    let tree =
      if optimize then
        tree
        |> logger#app "Collapse.dot" "Final Collapse" collapse_switch
        |> logger#app "Prune.dot" "Final Prune" prune
      else tree
    in
    to_target_expr branches env tree

let from_provs ?optimize logger env args provs =
  let matchproblem = MatchProblem.from_prov_branches provs in
  assemble ?optimize logger env args matchproblem
