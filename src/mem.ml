(** Objects operating directly on memory values *)

type bitrange = Base_types.bitrange
let pp_bitrange = Base_types.pp_bitrange
module ZSet = Set.Make(Z)

let rec map_nth f l i = match l, i with
  | x :: t, 0 -> f x :: t
  | x :: t, n -> x :: map_nth f t (n-1)
  | [], _ -> invalid_arg "map_nth"

(** Path of content in memory values *)
module Path = struct

  include Base_types.Path

  let rec (++) p1 p2 = match p2 with
    | Empty -> p1
    | Dot (p2', op) -> Dot (p1 ++ p2', op)

  let ($.) pa op = Dot (pa, op)
  
  let rec pop_non_empty = function
    | Empty -> assert false
    | Dot (Empty, op) -> op, Empty
    | Dot (pa, op) ->
      let head, pa' = pop_non_empty pa in
      head, Dot(pa', op)

  let rec pop = function
    | Empty -> None
    | p -> Some (pop_non_empty p)

  let rec push op = function
    | Empty -> Dot (Empty, op)
    | Dot (pa, op') -> Dot (push op pa, op')

  let rec to_rev_list = function
    | Empty -> []
    | Dot(p, op) -> op :: to_rev_list p
  let to_list p = List.rev @@ to_rev_list p
  let rec of_rev_list = function
    | [] -> Empty
    | op::t -> Dot(of_rev_list t, op)
  let of_list l = of_rev_list @@ List.rev l

  exception NotPrefix
  let rec list_cut_prefix l1 l2 = match l1, l2 with
    | h1::t1, h2::t2 when h1 = h2 ->
      list_cut_prefix t1 t2
    | [], l -> l
    | _ -> raise NotPrefix

  let is_prefix prefix pa =
    try
      let _ = list_cut_prefix (to_list prefix) (to_list pa) in
      true
    with NotPrefix -> false
  let is_prefixl prefix pa =
    try
      let _ = list_cut_prefix prefix pa in
      true
    with NotPrefix -> false
  
  let focus prefix pa =
    try of_list @@ list_cut_prefix (to_list prefix) (to_list pa)
    with NotPrefix ->
      Fmt.invalid_arg "Path %a is not a prefix of %a" pp prefix pp pa
  let focusl prefix pa =
    try list_cut_prefix prefix pa
    with NotPrefix ->
      Fmt.invalid_arg "Path %a is not a prefix of %a"
        pp (of_list prefix)
        pp (of_list pa)

end
module PathMap = Base_types.PathMap
type path = Path.t

(** Types describing a memory representation *)
module Ty = struct

  type word = [ `Word ]
  type spec = [ `Spec ]
  type block = [ `Block ]
  type top = [ word | block ]

  type kind = Spec | Word | Block

  include Base_types.MemTy

  let rec get_leaf env (t: t) = match t with
    | TVar (n, specs) when PathMap.is_empty specs ->
      get_leaf env @@ Env.find_mty n env
    | TFrag {src_pos; mem_ty; specs} when PathMap.is_empty specs ->
      get_leaf env mem_ty
    | TWord ws when PathMap.is_empty ws.specs -> Some t
    | TVar _ | TWord _ | TFrag _ -> None
    | TConst _ | TPtr _ | TStruct _ | TSplit _ -> None
  let is_leaf env t = Option.is_some @@ get_leaf env t 
  
  exception Inconsistent_spec : t * t -> exn
  exception Overlapping_provenance : Prov.t * t -> exn

  let rec merge_spec spec1 spec2 = match spec1, spec2 with
    (* | TVar (_n, _spec), spec (\* TOFIX *\) *)
    (* | spec, TVar (_n, _spec) -> (\* TOFIX *\) *)
    (*   spec *)
    | TWord w1, TWord w2 when w1.width = w2.width ->
      let specs = merge_specs w1.specs w2.specs in
      TWord {width = w1.width; specs}
    | TConst z1, TConst z2 when Z.equal z1 z2 -> TConst z1
    | TSplit {discrs; branches}, spec ->
      let branches =
        List.map (fun {lhs; provs; ty} ->
            {lhs; provs; ty = merge_spec ty spec})
          branches
      in
      TSplit {discrs; branches}
    | spec, TSplit {discrs; branches} ->
      let branches =
        List.map (fun {lhs; provs; ty} ->
            {lhs; provs; ty = merge_spec spec ty})
          branches
      in
      TSplit {discrs; branches}
    | (TWord _ | TConst _ | TFrag _),
      (TWord _ | TConst _ | TFrag _)
      -> raise @@ Inconsistent_spec (spec1, spec2)
    | _ -> assert false
  and merge_specs specs1 specs2 =
    PathMap.merge_safe ~f:(fun key -> function
        | `Left s | `Right s -> Some s
        | `Both (s1, s2) -> Some (merge_spec s1 s2)
      ) specs1 specs2
  
  let with_spec : t -> _ -> _ -> t =
    let insert_spec specs path spec =
      PathMap.update path (function _ -> Some spec) specs
    in
    let rec aux = fun ty path spec -> match ty with
      | TVar (n, specs) ->
        let specs = insert_spec specs path spec in
        TVar (n, specs)
      | TFrag {src_pos; mem_ty; specs} ->
        let specs = insert_spec specs path spec in
        TFrag {src_pos; mem_ty; specs}
      | TWord {width; specs} ->
        let specs = insert_spec specs path spec in
        TWord {width; specs}
      | TPtr x ->
        begin match Path.pop path with
          | Some (Deref, path) -> 
            let pointee = aux x.pointee path spec in
            TPtr {x with pointee}
          | _ ->
            let specs = insert_spec x.specs path spec in
            TPtr {x with specs}
        end
      | TStruct tys ->
        begin match Path.pop path with
          | Some (Access i, path) ->
            let tys = map_nth (fun ty -> aux ty path spec) tys i in
            TStruct tys
          | _ -> 
            invalid_arg "with_spec"
        end
      | TSplit { discrs; branches } ->
        (* refine split types by eliminating branches
        that are incompatible with the new spec *)
        begin match spec, CCList.find_idx ((=)path) discrs with
        | TConst w, Some (i, _) ->
          let discrs = CCList.remove_at_idx i discrs
          and branches = List.filter_map
            (fun {lhs; provs; ty} ->
              match List.nth lhs i with
              | Some w' when w' <> w -> None
              | _ ->
                let lhs = CCList.remove_at_idx i lhs
                and ty = aux ty path spec in
                Some {lhs; provs; ty})
            branches in
          begin match branches with
            | [b] -> b.ty
            | _ -> TSplit { discrs; branches }
          end
        | _ ->
          let branches = List.map
            (fun br -> { br with ty = aux br.ty path spec })
            branches in
          TSplit { discrs; branches } end
      | _ -> assert false
    in aux
    
  let with_specs ty (specs : specs) =
    PathMap.fold (fun pa spec ty -> with_spec ty pa spec) specs ty

  let expand_var ?specs n env =
    let mty = Env.find_mty n env in
    match specs with
    | None -> mty
    | Some specs -> with_specs mty specs
  let expand_frag { src_pos; mem_ty; specs } =
    with_specs mem_ty specs

  let rec equiv env ty1 ty2 = match ty1, ty2 with
    | TVar (n, specs), _ ->
      equiv env (expand_var ~specs n env) ty2
    | _, TVar (n, specs) ->
      equiv env ty1 (expand_var ~specs n env)
    | TFrag frag, _ ->
      equiv env (expand_frag frag) ty2
    | _, TFrag frag ->
      equiv env ty1 (expand_frag frag)
    | _ -> ty1 = ty2

  let rec filter_aux ~prov env = function
    | TConst v -> TConst v
    | TVar (n, specs) -> 
      let mty = filter ~prov env (Env.find_mty n env) 
      and specs = filter_specs ~prov specs
      in 
      with_specs mty specs
    | TFrag {src_pos; mem_ty; specs} ->
      let specs = filter_specs ~prov specs in
      TFrag {src_pos; mem_ty; specs}
    | TWord { width; specs } ->
      let specs = filter_specs ~prov specs in
      TWord { width; specs }
    | TPtr {width; align; pointee; specs} ->
      let pointee = filter env ~prov pointee
      and specs = filter_specs ~prov specs in
      TPtr {width; align; pointee; specs}
    | TStruct tys ->
      let mtyss = List.map (filter env ~prov) tys in
      TStruct mtyss
    | TSplit {discrs; branches} ->
      let branches = List.filter_map (fun { provs; ty ; lhs } ->
          if Prov.Set.mem_compat prov provs then
            Some {provs; lhs; ty = filter env ~prov ty}
          else
            None
        )
        branches
      in
      TSplit {discrs; branches}
  and filter ~prov env ty =
    Report.enter ~__LINE__ ~__FILE__ "filter"
      ~args:Report.["prov", d Prov.pp prov;
                    "ty", d pp ty] @@ fun _ ->
    let res = filter_aux ~prov env ty in
    Report.debugf "res = '%a'@." pp res;
    res
  and filter_spec ~prov path = function
    (* | TVar (n, spec) -> (\* TOFIX *\) *)
    (*   [PathMap.singleton path (TVar (n, spec))]  *)
    | TWord {width; specs} ->
      let specs = filter_specs ~prov specs in
      TWord {width; specs}
    | TConst v -> TConst v
    | TFrag {src_pos; mem_ty; specs} ->
      let specs = filter_specs ~prov specs in
      TFrag {src_pos; mem_ty; specs}
    | TSplit {discrs; branches} ->
      let branches = List.filter_map (fun { provs; ty ; lhs } ->
          if Prov.Set.mem_compat prov provs then
            let ty = filter_spec ~prov path ty in
            Some {provs; lhs; ty}
          else
            None
        ) branches
      in
      TSplit {discrs; branches}
    | _ -> assert false
  and filter_specs = fun ~prov specs ->
    PathMap.mapi
      (fun pa ty -> filter_spec ~prov pa ty)
      specs

  let rec specialize_aux ~prov env = function
    | TConst v -> Prov.Map.singleton prov @@ TConst v
    | TVar (n, specs) ->
      Prov.Map.Compat.(
        let* mty = specialize ~prov env (Env.find_mty n env) 
        and& specs = specialize_specs ~prov specs
        in 
        with_specs mty specs)
    | TFrag {src_pos; mem_ty; specs} ->
      Prov.Map.Compat.(
        let* specs = specialize_specs ~prov specs
        and&? () = src_pos
        in
        TFrag {src_pos; mem_ty; specs})
    | TWord { width; specs } ->
      Prov.Map.Compat.(
        let* specs = specialize_specs ~prov specs in
        TWord { width; specs }
      )
    | TPtr {width; align; pointee; specs} ->
      Prov.Map.Compat.(
        let* pointee = specialize env ~prov pointee
        and& specs = specialize_specs ~prov specs in
        TPtr {width; align; pointee; specs}
      )
    | TStruct tys ->
      let mtyss = List.map (specialize env ~prov) tys in
      Prov.Map.Compat.(
        let* mtys = Prov.Map.Compat.mergel mtyss in
        TStruct mtys
      )
    | TSplit {discrs; branches} ->
      Prov.Map.Compat.(
        let** { provs; ty ; lhs = _ } = branches in
        if Prov.Set.mem_compat prov provs then
          let* mty = specialize env ~prov ty
          and& () = Prov.Map.of_set provs
          and&?? () = prov
          in mty
        else
          Prov.Map.empty
      )
  and specialize ~prov env ty =
    Report.enter ~__LINE__ ~__FILE__ "specialize"
      ~args:Report.["prov", d Prov.pp prov;
                    "ty", d pp ty] @@ fun _ ->
    let res = specialize_aux ~prov env ty in
    Report.debugf "res = '%a'@."
      (Prov.Map.pp Prov.pp pp) res;
    res
  and specialize_spec ~prov path = function
    (* | TVar (n, spec) -> (\* TOFIX *\) *)
    (*   [PathMap.singleton path (TVar (n, spec))]  *)
    | TWord {width; specs} ->
      Prov.Map.Compat.(
        let* specs = specialize_specs ~prov specs in
        PathMap.singleton path @@ TWord {width; specs})
    | TConst v ->
      Prov.Map.singleton prov @@ PathMap.singleton path (TConst v)
    | TFrag {src_pos; mem_ty; specs} ->
      Prov.Map.Compat.(
        let* specs = specialize_specs ~prov specs
        and&? () = src_pos
        in
        PathMap.singleton path @@ TFrag {src_pos; mem_ty; specs})
    | TSplit {discrs; branches} ->
      Prov.Map.Compat.(
        let** { provs; ty ; lhs = _ } = branches in
        let* mty = specialize_spec ~prov path ty 
        and& () = Prov.Map.of_set provs
        and&?? () = prov
        in mty
      )
    | _ -> assert false
  and specialize_specs = fun ~prov specs ->
    if PathMap.is_empty specs then
      (* If there are no specs at all, we need to still provide something *)
      Prov.Map.singleton prov PathMap.empty
    else
      let specs_branches = List.map
          (fun (pa, ty) -> specialize_spec ~prov pa ty)
          (PathMap.bindings specs)
      in
      Prov.Map.Compat.(
        let*? _, specs = Prov.Map.Compat.mergel specs_branches in
        try Some (List.fold_left merge_specs PathMap.empty specs)
        with Inconsistent_spec _ -> None
      )

  let rec focus mem_env (pa : Path.t) ty = match Path.pop pa, ty with
    | None, _ -> ty
    | _, TVar (n, specs) ->
      let mty = expand_var ~specs n mem_env in
      focus mem_env pa mty
    | _, TFrag {src_pos; mem_ty; specs} ->
      focus mem_env pa (with_specs mem_ty specs)
    | Some (Bits {offset; width}, pa), TWord {width=w; specs} when
        PathMap.is_empty specs ->
      (* assert (offset+width <= w); *)
      focus mem_env pa (TWord {width; specs=PathMap.empty})
    | _, TWord {width; specs} ->
      let specs = PathMap.filter (fun pa' _ -> Path.is_prefix pa pa') specs in
      let specs = PathMap.of_list @@ List.map
        (fun (pa', spec) -> Path.focus pa pa', spec)
        (PathMap.bindings specs) in
      if PathMap.cardinal specs = 1 then
        snd (PathMap.choose specs)
      else
        TWord {width; specs}
    | Some (Deref, pa), TPtr { pointee; _ } ->
      focus mem_env pa pointee
    | _, TPtr { width; align; pointee; specs } ->
      let specs = PathMap.filter (fun pa' _ -> Path.is_prefix pa pa') specs in
      let specs = PathMap.of_list @@ List.map
        (fun (pa', spec) -> Path.focus pa pa', spec)
        (PathMap.bindings specs) in
      if PathMap.cardinal specs = 1 then
        snd (PathMap.choose specs)
      else
        TWord {width=align; specs}
    | Some (Access i, pa), TStruct fields ->
      focus mem_env pa (List.nth fields i)
    | _, TSplit { discrs; branches } -> (* TODO fix dangling discrs? *)
      let discrs = List.map (Path.focus pa) discrs in
      let branches = List.map
        (fun {lhs; provs; ty} -> {lhs; provs; ty = focus mem_env pa ty})
        branches in
      TSplit { discrs; branches }
    | _ -> assert false

  let rec gather_frags_aux : Env.t -> Path.t -> t -> (Hole.t * t) PathMap.t =
    fun env mpa -> function
    | TConst _ -> PathMap.empty
    | TFrag frag ->
      let mty = expand_frag frag in
      PathMap.singleton mpa (frag.src_pos, mty)
    | TVar (n, specs) ->
      let mty = expand_var ~specs n env in
      gather_frags_aux env mpa mty
    | TWord {width; specs} ->
      (* TODO also gather wn with no specs as (eps as wn) *)
      PathMap.fold
        (fun mpa' mty acc ->
          let pm = gather_frags_aux env Path.(mpa ++ mpa') mty in
          PathMap.union (fun _ -> assert false) acc pm)
        specs
        PathMap.empty
    | TPtr {width; align; pointee; specs} ->
      PathMap.fold
        (fun mpa' mty acc ->
          let pm = gather_frags_aux env Path.(mpa ++ mpa') mty in
          PathMap.union (fun _ -> assert false) acc pm)
        specs
        (gather_frags_aux env Path.(Dot (mpa, Deref)) pointee)
    | TStruct fields ->
      let field_subterms = List.mapi
        (fun i mty -> gather_frags_aux env Path.(Dot (mpa, Access i)) mty)
        fields in
      List.fold_left
        (PathMap.union (fun _ -> assert false))
        PathMap.empty
        field_subterms
    | TSplit {discrs; branches} ->
      assert false
  let gather_frags env ty = gather_frags_aux env Empty ty

  let gather_primitives : Env.t -> t -> t PathMap.t = fun env ->
    let rec aux acc mpos = function
      | TFrag _ -> acc
      | TConst _ -> acc
      | TVar (n, specs) ->
        aux acc mpos @@ expand_var ~specs n env
      | TWord {width; specs} as ty when PathMap.is_empty specs ->
        PathMap.singleton mpos ty
      | TWord {width; specs} ->
        PathMap.fold
          (fun mpos' mty acc -> aux acc Path.(mpos ++ mpos') mty)
          specs
          acc
      | TPtr {width; align; pointee; specs} ->
        let acc = aux acc Path.(mpos $. Deref) pointee in
        PathMap.fold
          (fun mpos' mty acc -> aux acc Path.(mpos ++ mpos') mty)
          specs
          acc
      | TStruct fields ->
        CCList.foldi
          (fun acc i mty -> aux acc Path.(mpos $. Access i) mty)
          acc
          fields
      | TSplit _ -> assert false
    in
    aux PathMap.empty Path.Empty

  let gather_constants : Env.t -> t -> Z.t PathMap.t = fun env ->
    let rec aux acc mpos = function
      | TFrag _ -> acc
      | TVar (n, specs) ->
        aux acc mpos @@ expand_var ~specs n env
      | TConst z ->
        PathMap.add mpos z acc
      | TWord {width; specs} ->
        PathMap.fold
          (fun mpos' mty acc -> aux acc Path.(mpos ++ mpos') mty)
          specs
          acc
      | TPtr {width; align; pointee; specs} ->
        let acc = aux acc Path.(mpos $. Deref) pointee in
        (* let spec_path = Path.Bits {offset = 0; width = align} in *)
        PathMap.fold
          (fun mpos' mty acc -> aux acc Path.(mpos ++ mpos') mty)
          specs
          acc
      | TStruct fields ->
        CCList.foldi
          (fun acc i mty -> aux acc Path.(mpos $. Access i) mty)
          acc
          fields
      | TSplit _ -> assert false
    in
    aux PathMap.empty Path.Empty

  let rec gather_top_splits_aux : Env.t -> Path.t -> t -> _ PathMap.t =
    fun env mpa -> function
      | TFrag _
      | TConst _ ->
        PathMap.empty
      | TVar (n, specs) ->
        let mty = expand_var ~specs n env in
        gather_top_splits_aux env mpa mty
      | TWord {width; specs} ->
        PathMap.fold
          (fun mpa' mty acc ->
             let pm = gather_top_splits_aux env Path.(mpa ++ mpa') mty in
             PathMap.union (fun _ -> assert false) acc pm)
          specs
          PathMap.empty
      | TPtr {width; align; pointee; specs} ->
        PathMap.fold
          (fun mpa' mty acc ->
             let pm = gather_top_splits_aux env Path.(mpa ++ mpa') mty in
             PathMap.union (fun _ -> assert false) acc pm)
          specs
          (gather_top_splits_aux env Path.(Dot (mpa, Deref)) pointee)
      | TStruct fields ->
        let field_subterms = List.mapi
            (fun i mty -> gather_top_splits_aux env Path.(Dot (mpa, Access i)) mty)
            fields in
        List.fold_left
          (PathMap.union (fun _ -> assert false))
          PathMap.empty
          field_subterms
      | TSplit {discrs; branches} ->
        PathMap.singleton mpa (discrs, branches)
  let gather_top_splits env ty = gather_top_splits_aux env Empty ty

  let rec gather_all_splits_aux : Env.t -> t -> _ list =
    fun env -> function
      | TFrag _
      | TConst _ ->
        []
      | TVar (n, specs) ->
        let mty = expand_var ~specs n env in
        gather_all_splits_aux env mty
      | TWord {width; specs} ->
        List.concat_map (gather_all_splits_aux env) @@
        Iter.to_list @@
        PathMap.values specs
      | TPtr {width; align; pointee; specs} ->
        gather_all_splits_aux env pointee @
        (List.concat_map (gather_all_splits_aux env) @@
         Iter.to_list @@
         PathMap.values specs)
      | TStruct fields ->
        List.concat_map (gather_all_splits_aux env) fields
      | TSplit {discrs; branches} ->
        (discrs, branches) ::
        List.concat_map
          (fun { ty ; _ } -> gather_all_splits_aux env ty)
          branches
  let gather_all_splits env ty = gather_all_splits_aux env ty


  module I = struct
    let (|><) oty (path, spec) = with_spec oty path spec
    let (@:) path ty : path * t = path, ty
    let (@=) path z : path * t = path, TConst z
  end

  let word width = TWord {width; specs = PathMap.empty}
  let ptr ~width ~align pointee =
    TPtr {width; align; pointee; specs = PathMap.empty}
  let sub ~sub_ty h = TFrag {src_pos = h; mem_ty = sub_ty; specs = PathMap.empty}
  let var n = TVar (n, PathMap.empty)
end
type ty = Ty.t
include Ty.I

(** Mem ty "shapes" determine what should be allocated *)
(* TODO these guys should be the same as mem provs/patterns *)
module Shape = struct
  type t =
    | SUnk of int
    | SImm of { width: int; value: Z.t }
    | SPtr of { width: int; pointee: t }
    | SWord of { base: t; range: bitrange; spec: t }
    | SStruct of t list

  let rec pp fmt = function
    | SUnk l -> Fmt.pf fmt "?%s" (Name.encode_id l)
    | SImm { width; value } ->
      Fmt.pf fmt "%a%s" Z.pp_print value (Name.encode_id width)
    | SPtr { width; pointee } -> Fmt.pf fmt "&<%i>(%a)" width pp pointee
    | SWord { base; range; spec } ->
      Fmt.pf fmt "%a w %a:%a" pp base pp_bitrange range pp spec
    | SStruct fields ->
      Fmt.pf fmt "{{%a}}" Fmt.(list ~sep:(any ", ") pp) fields

  let rec size_of = function
    | SUnk width | SImm { width; _ } | SPtr { width; _} -> width
    | SWord { base; _ } -> size_of base
    | SStruct fields ->
      List.fold_left (fun acc sh -> acc + size_of sh) 0 fields

  (* TODO wtf is this doing here *)
  let rec size_of_ty (env: Env.t) : Ty.t -> int = function
    | TConst _ -> assert false
    | TVar (n, specs) ->
      size_of_ty env @@ Ty.expand_var ~specs n env
    | TFrag {mem_ty; _} -> size_of_ty env mem_ty
    | TWord {width; _}
    | TPtr {width; _} -> width
    | TStruct fields ->
      List.fold_left (fun acc mty -> acc + size_of_ty env mty) 0 fields
    | TSplit {discrs = _; branches} ->
      List.fold_left
        (fun acc {Ty.ty; _} -> max acc @@ size_of_ty env ty)
        0
        branches

  let rec of_ty (env: Env.t) : Ty.t -> t = function
    | TVar (n, specs) ->
      of_ty env @@ Ty.expand_var ~specs n env
    | TWord { width; specs } ->
      of_specs env width (PathMap.bindings specs)
    | TPtr { width; pointee; align; specs } ->
      let base = SPtr { width; pointee = of_ty env pointee } in
      let spec = of_specs env align (PathMap.bindings specs) in
      SWord { base; range = {offset = 0; width = align}; spec }
    | TStruct fields ->
      SStruct (List.map (of_ty env) fields)
    (* | TFrag frag ->
      of_ty env (Ty.expand_frag frag) *)
    | mty ->
      SUnk (size_of_ty env mty)
  and of_specs env width : (Path.t * ty) list -> t = function
    | [] -> SUnk width
    | [Empty, mty] -> of_spec env width mty
    | (Dot (Empty, Bits range), mty) :: specs ->
      let spec = of_spec env range.width mty in
      let base = of_specs env width specs in
      SWord { base; range; spec }
    | _ -> assert false
  and of_spec env width = function
    | TVar (n, specs) ->
      of_spec env width @@ Ty.expand_var ~specs n env
    | TConst value -> SImm { width; value }
    | TSplit _ -> SUnk width
    | mty -> of_ty env mty
end

type shape = Shape.t
let pp_shape = Shape.pp

(** Values in memory stores *)
module V = struct
  module Metrics = struct
    (* TODO *)
    let all_metrics = []
  end

  type t =
    | VUnk of int
    | VImm of { width: int; value: Z.t }
    | VPtr of { width: int; addr: Name.t }
    | VWord of { base: t; range: bitrange; spec: t }
    | VStruct of t list

  type store = t Name.Map.t

  type loc = { addr: Name.t; mpos: Path.t }

  let rec pp fmt = function
    | VUnk width ->
      Fmt.pf fmt "?%i" width
    | VImm { width; value } ->
      Fmt.pf fmt "(%a)%i" Z.pp_print value width
    | VPtr { width; addr } ->
      Fmt.pf fmt "&<%i>(%s)" width addr
    | VWord { base; range; spec } ->
      Fmt.pf fmt "@[%a @[<hv>with %a =@ %a@]@]" pp base pp_bitrange range pp spec
    | VStruct fields ->
      Fmt.pf fmt "@[<hv 3>{{ %a@;<1 -3>}}@]" Fmt.(list ~sep:Fmt.sp @@ pp ++ any ";") fields

  let pp_store = Name.Map.pp ~sep:Fmt.semi ~bind:(Fmt.any " →@ ") pp

  let rec size_of = function
    | VUnk width | VImm { width; _ } | VPtr { width; _ } -> width
    | VWord { base; _ } -> size_of base
    | VStruct fields -> List.fold_left (fun l v -> l + size_of v) 0 fields

  (* Only keep addresses that are reachable from the mval arg *)
  let filter_store st mval =
    let rec aux store = function
      | VUnk _ | VImm _ ->
        store
      | VPtr { addr; width } ->
        let pointee = Name.Map.find addr st in
        let store = aux store pointee in
        Name.Map.add addr pointee store
      | VWord { base; range; spec } ->
        let store = aux store spec in
        let store = aux store base in
        store
      | VStruct fields ->
        CCList.fold_left aux store fields
    in
    aux Name.Map.empty mval

  
  (* Only keep addresses that are reachable from the mval arg *)
  let rename_value (st, mval) =
    let n = ref 0 in
    let tbl = Hashtbl.create 3 in
    let find_addr k =
      CCHashtbl.get_or_add tbl ~k
        ~f:(fun _ -> Fmt.str "a%i" (CCRef.get_then_incr n))
    in
    let rec aux store = function
      | VUnk _ | VImm _ as mval ->
        store, mval
      | VPtr { addr; width } ->
        let pointee = Name.Map.find addr st in
        let addr = find_addr addr in
        let store, pointee = aux store pointee in
        Name.Map.add addr pointee store, VPtr { addr; width }
      | VWord { base; range; spec } ->
        let store, spec = aux store spec in
        let store, base = aux store base in
        store, VWord { base; range; spec }
      | VStruct fields ->
        let store, fields = CCList.fold_map aux store fields in
        store, VStruct fields
    in
    aux Name.Map.empty mval

  let rec focus st mpath mval = match Path.pop mpath, mval with
    | None, _ -> mval
    | Some (Deref, _), VWord { base; range; spec } ->
      (* FIX ugly bridge between old mem tys and new shapes *)
      focus st mpath base
    | Some (Bits b, mpath'), VImm {width;value} ->
      VImm {width = b.width;
            value = Z.extract value b.offset b.width}
    | Some (Bits b, mpath'), VWord { base; range; spec } ->
      (* Report.infof "focus" "%a on %a (spec is %a = %a)"
        Path.pp mpath pp mval pp_bitrange range pp spec; *)
      if b = range then
        focus st mpath' spec
      else if range.offset <= b.offset && b.width + b.offset <= range.width + range.offset then
        (* FIX ugly bridge between old mem tys and new shapes *)
        focus st mpath spec
      else
        focus st mpath base
    | Some (Mask b, mpath'), VWord { base; range; spec } ->
      if b = range then
        focus st mpath' base
      else
        assert false
    | Some (Deref, mpath), VPtr { width; addr } ->
      begin match Name.Map.find_opt addr st with
      | Some mval -> focus st mpath mval
      | None -> Report.fail "addr %s not found in %a" addr pp_store st
      end
    | Some (Access i, mpath), VStruct fields when i < List.length fields ->
      focus st mpath (List.nth fields i)
    | _ ->
      let store = filter_store st mval in
      Report.fail "Can't focus %a on %a in %a"
        Path.pp mpath pp mval pp_store store

  let read st { addr; mpos } : t = match Name.Map.find_opt addr st with
    | Some mval -> focus st mpos mval
    | None -> Report.fail "Address %s not found in %a" addr pp_store st

  let rec insert st mpath mval mval' : store * t =
    match Path.pop mpath, mval with
    | None, _ -> st, mval'
    | Some (Deref, _), VWord { base; range; spec } ->
      (* FIX ugly bridge between old mem tys and new shapes *)
      let st', base = insert st mpath base mval' in
      st', VWord { base; range; spec }
    | Some (Bits b, mpath'), VWord { base; range; spec } ->
      if b = range then
        let st', spec = insert st mpath' spec mval' in
        st', VWord { base; range; spec }
      else if range.offset <= b.offset && b.width + b.offset <= range.width + range.offset then
        (* FIX ugly bridge between old mem tys and new shapes *)
        let st', spec = insert st mpath spec mval' in
        st', VWord { base; range; spec }
      else
        let st', base = insert st mpath base mval' in
        st', VWord { base; range; spec }
    | Some (Mask b, mpath'), VWord { base; range; spec } ->
      if b = range then
        let st', base = insert st mpath' base mval' in
        st', VWord { base; range; spec }
      else
        assert false
    | Some (Deref, mpath'), VPtr { width; addr } ->
      begin match Name.Map.find_opt addr st with
      | Some mval ->
        let st', mval = insert st mpath' mval mval' in
        Name.Map.add addr mval st', VPtr { width; addr }
      | None -> Report.fail "address %s not found in %a" addr pp_store st
      end
    | Some (Access i, mpath'), VStruct fields when i < List.length fields ->
      let st', mvali = insert st mpath' (List.nth fields i) mval' in
      let fields = List.mapi (fun j mvalj -> if j = i then mvali else mvalj) fields in
      st', VStruct fields
    | _ ->
      Report.fail "Can't insert at %a in %a" Path.pp mpath pp mval

  let write st { addr; mpos } mval : store =
    let mval0 = Name.Map.find addr st in
    let st', mval0' = insert st mpos mval0 mval in
    Name.Map.add addr mval0' st'

  (* TODO when shape_of is implemented, remove this and just compare shapes *)
  (* TODO we need some sort of normalization to order word specs in the same way *)
  let rec equiv (st, mval) (st', mval') = match mval, mval' with
    | VUnk l, VUnk l' -> l = l'
    | VImm imm, VImm imm' ->
      imm.width = imm'.width && Z.equal imm.value imm'.value
    | VPtr { width; addr }, VPtr { width = width'; addr = addr' }
      when width = width' ->
      let mval = read st { addr; mpos = Path.Empty }
      and mval' = read st' { addr = addr'; mpos = Path.Empty } in
      equiv (st, mval) (st', mval')
    | VWord word, VWord word' ->
      word.range = word'.range &&
      equiv (st, word.base) (st', word'.base) &&
      equiv (st, word.spec) (st', word'.spec)
    | VStruct fields, VStruct fields'
      when List.compare_lengths fields fields' = 0 ->
      List.for_all2
        (fun mval mval' -> equiv (st, mval) (st', mval'))
        fields fields'
    | _ -> false


  (** Dot output *)
  module Dot = struct
    type id = Block of int | Sub of id * int
    (* type blob =
      | DWord of { width: int; bits: Z.t }
      | DPtr of { width: int; addr: Name.t }
    type contents =
      | DBlob of blob list
      | DBlock of contents list *)
    type contents =
      | DUnk of int
      | DImm of { width: int; value: Z.t }
      | DPtr of { width: int; addr: Name.t }
      | DWord of { base: contents; specs : (bitrange * contents) list }
      | DStruct of contents list
    type vertex = { id: int; contents: contents }
    type edge = id * int
    
    let as_dot_graph (st, mval) =
      let r = ref 0 in
      let new_id () = incr r; !r in
      let vertices : vertex list ref = ref []
      and edges : edge list ref = ref [] in
      let add_vertex x = vertices := x :: !vertices
      and add_edge x = edges := x :: !edges in
      let rec get_root_vertex mval =
        let id = new_id () in
        add_vertex { id; contents = contents_of_mval (Block id) mval };
        id
      (* This is probably slightly broken for nested structs *)
      and contents_of_mval vertex_id = function
        | VUnk width ->
          DUnk width
        | VImm { width; value } ->
          DImm { width; value }
        | VPtr { width; addr } ->
          let pointee = Name.Map.find addr st in
          let target = get_root_vertex pointee in
          add_edge (vertex_id, target);
          DPtr { width; addr }
        | VWord _ as mval -> 
          let base, specs = contents_of_vword 0 vertex_id mval in
          DWord { base; specs }
        | VStruct fields ->
          DStruct (List.mapi
            (fun i mv -> contents_of_mval (Sub (vertex_id, i)) mv)
            fields)
      and contents_of_vword i vertex_id = function
        | VWord { base; range; spec } ->
          let base, specs = contents_of_vword (i+1) vertex_id base in
          let spec = contents_of_mval (Sub (vertex_id, i)) spec in
          base, (range, spec)::specs
        | mval ->
          contents_of_mval vertex_id mval, []
      in
      let _ = get_root_vertex mval in
      !vertices, !edges

    let pp_vertex ppf { id; contents } =
      let attrs =
        let rec pp_label inside_block ppf x = match x with
          | DUnk width ->
            Fmt.pf ppf "%i uninit bits" width
          | DImm { width; value } ->
            Fmt.pf ppf "%a on %i bits" Z.pp_print value width
          | DPtr { width; addr } ->
            Fmt.pf ppf {|&\<%i\> %a|} width Name.pp addr
          | DWord { base; specs } ->
            let pp_spec ppf (i, (range, f)) =
              Fmt.pf ppf "{ <%i>%a | %a }" i
                pp_bitrange range
                (pp_label true) f
            in
            Fmt.pf ppf "{ %a with | %a }"
              (pp_label true) base
              (Fmt.list ~sep:(Fmt.any " | ") pp_spec)
              (List.mapi (fun i x -> i, x) specs)
          | DStruct fields ->
            let nest = if inside_block
              then Fmt.pf ppf "{ nested struct | { %a } }"
              else Fmt.pf ppf "%a"
            in
            let pp_field ppf (i, f) =
              Fmt.pf ppf "<%i> %a" i
                (pp_label true) f
            in
            nest
              (Fmt.list ~sep:(Fmt.any " | ") pp_field)
              (List.mapi (fun i x -> i, x) fields)
        and shape = match contents with
          | DStruct _ | DWord _ -> "record"
          | _ -> "box"
        in
        [ ("label", Fmt.str "%a" (pp_label false) contents);
          ("shape", shape) ]
      in
      let pp_attr ppf (field, v) = Fmt.pf ppf {|%s="%s"|} field v in
      Fmt.pf ppf "%d [%a];" id (Fmt.list ~sep:(Fmt.any ", ") pp_attr) attrs

    let pp_edge ppf (id, id') =
      let attrs = [] in
      let pp_attr ppf (field, v) = Fmt.pf ppf "%s=%S" field v in
      let rec pp_src ppf = function
        | Block i -> Fmt.pf ppf "%d" i
        | Sub (id, i) -> Fmt.pf ppf "%a:%d" pp_src id i
      in
      Fmt.pf ppf "%a -> %d:w [%a];"
        pp_src id id'
        (Fmt.list ~sep:(Fmt.any ", ") pp_attr)
        attrs

    let pp ppf (name, t) =
      let vertices, edges = as_dot_graph t in
      Fmt.pf ppf {|@[<v2>digraph %S {@,concentrate=true;rankdir="LR";@,%a@,@,%a@,label=%S}@]|}
        name
        (Fmt.list ~sep:Fmt.cut pp_vertex)
        vertices
        (Fmt.list ~sep:Fmt.cut pp_edge)
        edges
        name
  end

  let pp_dot = Dot.pp
end
type value = V.store * V.t
let pp_value fmt (st, v) =
  let st, v = V.rename_value (st, v) in
  if Name.Map.is_empty st then
    V.pp fmt v
  else
    Fmt.pf fmt "@[<v>@[%a@]@ with @[<v>%a@]@]" V.pp v V.pp_store st

(** Memory patterns *)
module MProv = struct
  type t =
    | PAny
    | POr of t * t
    | PValue of Z.t
    | PWord of {width: int; specs: specs}
    | PPtr of {width: int; align: int; pointee: t; specs: specs}
    | PStruct of t list
  and specs = t PathMap.t

  let rec pp fmt : t -> _ = function
    | PAny -> Fmt.pf fmt "_"
    | POr (p, p') -> Fmt.pf fmt "@[%a@]@ | @[%a@]" pp p pp p'
    | PValue z -> Fmt.pf fmt "%a" Z.pp_print z
    | PWord {width; specs} ->
      Fmt.pf fmt "@[Word%d%a@]" width pp_add_specs specs
    | PPtr { width; align; pointee; specs } ->
      Fmt.pf fmt "@[<2>Ptr%d,%d(@,%a@,)%a@]" width align
        pp pointee
        pp_add_specs specs
    | PStruct fields ->
      Fmt.pf fmt "@[<hv 3>{{ %a@;<0 -2>@]}}" Fmt.(list ~sep:comma pp) fields
  and pp_add_specs =
    fun fmt specs ->
    if PathMap.is_empty specs then ()
    else Fmt.pf fmt "@ ⋉ %a"
        (PathMap.pp ~pp_arrow:(Fmt.any ":") ~pp_sep:(Fmt.any " ⋉ ") Path.pp pp) specs

  let rec pp_simple fmt = function
    | PAny -> Fmt.pf fmt "_"
    | POr (p, p') -> Fmt.pf fmt "%a | %a" pp_simple p pp_simple p'
    | PValue z -> Fmt.pf fmt "%a" Z.pp_print z
    | PWord {width; specs} ->
      Fmt.pf fmt "i%d%a" width pp_simple_specs specs
    | PPtr { width; align; pointee; specs } ->
      Fmt.pf fmt "&<%d,%d>(%a)%a"
        width align
        pp_simple pointee
        pp_simple_specs specs
    | PStruct fields ->
      Fmt.pf fmt "{{%a}}" Fmt.(list ~sep:comma pp_simple) fields
  and pp_simple_specs =
    fun fmt specs ->
    if PathMap.is_empty specs then ()
    else Fmt.pf fmt " w %a"
      (PathMap.pp ~pp_arrow:(Fmt.any ":") ~pp_sep:(Fmt.any " w ") Path.pp pp_simple)
      specs
  
  let focus (path0 : path) pat0 =
    let rec aux (pathL:Path.l) pat = match pathL, pat with
      | _, PAny -> PAny
      | [], _ -> pat
      | Deref :: paL, PPtr { pointee; _ } -> aux paL pointee
      | Access i :: paL, PStruct fields -> aux paL (List.nth fields i)
      | pathL, (PWord { specs; _ } | PPtr { specs; _ })
        when
          PathMap.exists
            (fun rpath _ -> Path.is_prefixl (Path.to_list rpath) pathL)
            specs ->
        let rpath', pat' =
          Iter.find_pred_exn
            (fun (rpath, _)  -> Path.is_prefixl (Path.to_list rpath) pathL)
            @@ PathMap.to_iter specs
        in
        let pathL' = Path.focusl (Path.to_list rpath') pathL in
        aux pathL' pat'
      | _ ->
        Report.fail
          "@[<v 2>Incompatible focusing:@ %a@;<1 -2>and@ %a@;<1 -2>in@ %a@;<1 -2>and@ %a@]"
          Path.pp (Path.of_list pathL)
          pp pat
          Path.pp path0
          pp pat0
    in aux (Path.to_list path0) pat0

  exception No_int_values of t
  let possible_ints ty0 = 
    let rec aux = function
      | PAny -> `Any
      | POr (p, p') ->
        (match aux p, aux p' with
         | `Set zs, `Set zs' -> `Set (ZSet.union zs zs')
         | `NonZero, `Set zs | `Set zs, `NonZero when not (ZSet.mem Z.zero zs) -> `NonZero
         | _ -> `Any)
      | PValue z -> `Set (ZSet.singleton z)
      | PWord {specs; _} ->
        if PathMap.mem Empty specs then
          aux @@ PathMap.find Empty specs
        else
          `Any
      | PPtr {specs; _} ->
        if PathMap.mem Empty specs then
          aux @@ PathMap.find Empty specs
        else
          `NonZero
      | PStruct _ ->
        raise @@ No_int_values ty0
    in
    aux ty0

  let rec of_prov env (prov : Prov.t) mty : t =
    Report.(enter ~__LINE__ ~__FILE__
              "of_prov"
              ~args:["prov", d Prov.pp prov; "mty", d Ty.pp mty])
    @@ fun _ ->
    match prov with
    | Top -> PAny
    | _ ->
      let mtys = Ty.specialize ~prov env mty in
      Report.debugf "@[<2>mtys: @[%a@]@]" (Prov.Map.pp Prov.pp Ty.pp) @@ mtys;
      let mpats =
        List.map
          (fun (_prov', ty) -> of_ty env prov ty)
          (Prov.Map.bindings mtys)
      in
      match mpats with
      | [] ->
        assert false
      | [x] -> x
      | x :: l -> List.fold_left (fun acc x -> POr (acc, x)) x l
  and of_ty
    : Env.t -> Prov.t -> Ty.t -> t
    = fun env prov mty -> match mty with
      | TVar (n, specs) ->
        of_ty env prov (Ty.expand_var n env ~specs)
      | TConst v ->
        PValue v
      | TFrag frag ->
        let mty = Ty.expand_frag frag in
        of_prov env (Prov.focus frag.src_pos prov) mty
      | TWord {width; specs} ->
        PWord {width; specs = of_specs env prov specs}
      | TPtr {width; align; pointee; specs} ->
        let specs = of_specs env prov specs in
        let pointee = of_ty env prov pointee in
        PPtr {width; align; pointee; specs}
      | TStruct l ->
        PStruct (List.map (of_ty env prov) l)
      | _ ->
        Report.fail "MProv.of_ty: unhandled type %a@." Ty.pp mty
  and of_specs env prov specs =
    PathMap.map (of_ty env prov) specs

  let to_mem env mty0 prov0 =
    let mem_prov = of_prov env prov0 mty0 in
    Report.debugf "@[<2>prov_to_mem(%a,@ @[%a@]) =@ @[%a@]@]"
      Prov.pp prov0
      Ty.pp mty0
      pp mem_prov;
    mem_prov
  
end
type prov = MProv.t
let pp_prov = MProv.pp

(** Mem exprs as sketched in knf2 (?) *)
module E = struct
  type t =
    | Extract of Path.t
    | Var of Name.t
    | Imm of Z.t
    | Word of {width: int; specs: specs}
    | Ptr of {width: int; align: int; pointee: t; specs: specs}
    | Struct of t list
  and specs = t PathMap.t

  let rec pp fmt = function
    | Extract p -> Path.pp fmt p
    | Var x -> Fmt.string fmt x
    | Imm z -> Z.pp_print fmt z
    | Word {width; specs} ->
      Fmt.pf fmt "@[Word%d%a@]" width pp_add_specs specs
    | Ptr {width; align; pointee; specs} ->
      Fmt.pf fmt "@[<2>Ptr%d,%d(@,%a@,)%a@]" width align
        pp pointee
        pp_add_specs specs
    | Struct fields ->
      Fmt.pf fmt "@[<hv 3>(( %a@;<0 -2>@]))" Fmt.(list ~sep:comma pp) fields
  and pp_add_specs =
    fun fmt specs ->
    if PathMap.is_empty specs then ()
    else Fmt.pf fmt "@ w %a"
        (PathMap.pp ~pp_arrow:(Fmt.any ":") ~pp_sep:(Fmt.any " w ") Path.pp pp) specs

  let rec of_ty : Env.t -> t PathMap.t -> Path.t -> Ty.t -> t =
    fun env subterms mpa -> function
    | TFrag _ as mty -> begin try PathMap.find mpa subterms with
      | Not_found -> let err = Fmt.str "oops %a not in %a for %a" Path.pp mpa (PathMap.pp Path.pp pp) subterms Ty.pp mty in failwith err
      end
    | TVar (n, specs) ->
      let mty = Ty.expand_var ~specs n env in
      of_ty env subterms mpa mty
    | TConst z -> Imm z
    | TWord {width; specs} ->
      let specs = PathMap.mapi
        (fun mpa' mty -> of_ty env subterms Path.(mpa ++ mpa') mty)
        specs in
      Word {width; specs}
    | TPtr {width; align; pointee; specs} ->
      let pointee = of_ty env subterms Path.(Dot (mpa, Deref)) pointee in
      let specs = PathMap.mapi
        (fun mpa' mty -> of_ty env subterms Path.(mpa ++ mpa') mty)
        specs in
      Ptr {width; align; pointee; specs}
    | TStruct fields ->
      let fields = List.mapi
        (fun i mty -> of_ty env subterms Path.(Dot (mpa, Access i)) mty)
        fields in
      Struct fields
    | TSplit _ -> assert false
end

