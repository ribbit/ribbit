type bitword = Z.t

type path = Syntax.path
type t = Syntax.memty
and branch = Syntax.membranch

exception Not_a_type of t
exception Not_a_spec of t
exception Invalid_slice of Base_types.bitrange * int option

let rec size (t:t) = match t with
  | TVar _ -> None
  | TConst _ -> None
  | TWord l -> Some l
  | TPtr { len ; _ } -> Some len
  | TStruct l ->
    List.fold_left (CCOption.map2 (+)) (Some 0) @@ List.map (size) l
  | TFrag (mty, _h) ->
    size mty
  | TSplit { discrs; branches } ->
    List.fold_left
      CCOption.(fun st (b:branch) -> let+ st = st and+ i = size b.ty in max i st)
      (Some 0) branches
  | TWith (t, _, _) -> size t

(* let check_size tyenv t expected_size = *)
(*   match size tyenv t with *)
(*   | Some i when i <= expected_size -> () *)
(*   | size -> raise @@ Wrong_size (t, size, expected_size) *)

let rec pp_path fmt (x:path) = match x with
  | Root -> Fmt.pf fmt "_"
  | Deref path -> Fmt.pf fmt "%a.*" pp_path path
  | Access (path, i) -> Fmt.pf fmt "%a.%i" pp_path path i
  | Extract (path, i, j) -> Fmt.pf fmt "%a.[%i-%i]" pp_path path i j

let rec pp fmt (x:t) = match x with
  | TVar n ->
    Name.pp fmt n
  | TConst v -> 
    Fmt.pf fmt "(= %a)" Z.pp_print v
  | TWord len ->
    Fmt.pf fmt "@[<2>Word%d@]" len
  | TPtr { len; align; pointee } ->
    Fmt.pf fmt "@[<2>@[Ptr%d,%d(%a)@]@]"
      len align
      pp pointee
  | TStruct fields ->
      Fmt.pf fmt "{{%a}}"
        Fmt.(list ~sep:comma pp) fields
  | TFrag (mty, h) ->
    Fmt.pf fmt "%a as %a" Hole.pp h pp mty
  | TSplit { discrs; branches } ->
    Fmt.pf fmt "@[<v 2>@[Split(%a)@] {@,%a@;<0 -2>}@]"
      Fmt.(list ~sep:semi pp_path) discrs
      Fmt.(list ~sep:cut pp_branch) branches
  | TWith (ty, path, ty') ->
    Fmt.pf fmt "%a ⋉ %a : %a"
      pp ty
      pp_path path
      pp ty'
and pp_branch : _ -> Syntax.membranch -> _ =
  fun fmt { lhs; provenance; ty } ->
  Fmt.pf fmt "@[<2>| %a@ (@[%a@]) =>@ @[%a@]@]"
    Fmt.(list ~sep:comma @@ option ~none:(any "_") Z.pp_print) lhs
    Syntax.pp_provenance provenance
    pp ty

let rec validate_path t (p: path) : Mem.Path.t =
  let open Mem.Path in
  match p with
  | Root -> Empty
  | Deref p ->
    validate_path t p $. Deref
  | Access (p, i) ->
    validate_path t p $. Access i
  | Extract (p, offset, width) ->
    begin match size t with
      | Some l when offset+width <= l -> ()
      | s ->
        raise (Invalid_slice ({offset; width}, s))
    end ;
    validate_path t p $. Bits {offset; width}

let validate_provenance p =
  let open CCList.Infix in
  let rec aux (prov : Syntax.provenance) : Prov.t list = match prov with
    | Any ->  [Top]
    | Int i -> [Int i]
    | Tuple ps ->
      let rec auxL = function
        | [] -> [[]]
        | p :: rest ->
          let+ p' = aux p and+ rest' = auxL rest in
          p' :: rest'
      in
      let+ ps = auxL ps in
      Prov.Tup ps      
    | Record ps -> 
      let rec auxL = function
        | [] -> [[]]
        | (k, p) :: rest ->
          let+ p' = aux p and+ rest' = auxL rest in
          (k, p') :: rest'
      in
      let+ ps = auxL ps in
      Prov.Rec ps     
    | Constr (n, p) ->
      let+ p' = aux p in
      Prov.Cons (n, p')
    | Or (p1, p2) ->
      aux p1 @ aux p2
  in
  Prov.Set.of_list @@ aux p  

let validate =
  let rec f_ty (t : t) : Mem.Ty.t  = match t with
    | TVar n ->
      Mem.Ty.var n
    | TConst _ as t ->
      raise (Not_a_type t)
    | TWord width ->
      Mem.Ty.word width
    | TPtr { len = width; align; pointee } ->
      (* check_size (ty, env) pointee (len - align); *)
      Mem.Ty.ptr ~width ~align (f_ty pointee)
    | TStruct l ->
      TStruct (List.map f_ty l)
    | TFrag (mty, h) ->
      Mem.Ty.sub ~sub_ty:(f_ty mty) h
    | TSplit {discrs; branches} ->
      let discrs = List.map (validate_path t) discrs in
      let branches = List.map (f_branch discrs) branches in
      begin (* Ensure provenances are all distincts *)
        let provs = List.map (fun x -> x.Mem.Ty.provs) branches in
        let are_disjoints s1 s2 =
          Prov.Set.for_all (fun p1 ->
              Prov.Set.for_all (fun p2 ->
                  Option.is_none @@ Prov.intersect p1 p2
                ) s2
            ) s1
        in
        let rec check_disjoint = function
          | [] -> ()
          | h::l ->
            if not (List.for_all (are_disjoints h) l) then
               Report.fail "@[<v>[Speclang.validate]@;Type@;<0 2>%a@;is not a valid split specification: Its provenances are not disjoint" pp t;
            check_disjoint l
        in
        check_disjoint provs
      end;
      TSplit {discrs; branches} (* TODO *)
    | TWith (t, path, t') ->
      Mem.(f_ty t |>< validate_path t path @: f_spec t')
  and f_branch discrs {lhs; provenance; ty} =
    let ty0 = f_ty ty in
    let ty = List.fold_left2
      (fun ty p -> function | Some w -> Mem.(ty |>< p @= w) | None -> ty)
      ty0 discrs lhs
    in
    {lhs; provs = validate_provenance provenance; ty}
  and f_spec (t : t) : Mem.Ty.t = match t with
    | TVar n -> Mem.Ty.var n
    | TConst v -> TConst v
    | TWord width -> Mem.Ty.word width
    | TPtr { len = width; align; pointee } ->
      Mem.Ty.ptr ~width ~align (f_ty pointee)
    | TFrag (mty, h) -> Mem.Ty.sub ~sub_ty:(f_spec mty) h
    | TSplit {discrs; branches} ->
      let discrs = List.map (validate_path t) discrs in
      let branches = List.map (f_branch_spec discrs) branches in
      TSplit {discrs; branches} (* TODO *)
    | TWith (t, path, t') ->
      Mem.(f_spec t |>< (validate_path t) path @: f_spec t')
    | TStruct _ as t ->
      Report.fail "@[<v>[Speclang.validate]@;Type@;<0 2>%a@;is not a valid bitword content specification.@]" pp t
  and f_branch_spec _discrs { lhs; provenance; ty } =
    { lhs; provs = validate_provenance provenance ; ty = f_spec ty } (* TODO *)
  in
  f_ty




exception Dont_agree of (string * Types.ty * Mem.Ty.t * (Format.formatter -> unit))

let dont_agree name ty mty =
  Format.kdprintf (fun d -> raise (Dont_agree (name, ty, mty, d)))

let rec check_agreement h env name (ty0 : Types.ty) (mty0 : Mem.Ty.t) =
  let open Mem in
  Report.enter ~__FILE__ ~__LINE__ "check_agreement"
    ~args:Report.[
        "name", d Fmt.string name;
        "ty", d (Types.pp) ty0;
        "mty", d Ty.pp mty0
      ] @@ fun _ ->
  if Hashtbl.mem h (ty0, mty0) then
    Report.debugf "Already seen"
  else begin
    Hashtbl.add h (ty0, mty0) ();
    if Types.is_leaf env ty0 && Ty.is_leaf env mty0 then
      ()
    else begin
      let ty0 = Types.unroll env ty0 in
      (* Distinguishability *)
      Report.enter ~__FILE__ ~__LINE__ "distinguishability" begin fun _ ->
        match ty0 with
        | Sum l ->
          let single_sums = List.map (fun (c, ty) -> Types.Sum[c,ty]) l in
          let all_type_pairs =
            CCList.product (fun x y -> x,y) single_sums single_sums
            |> CCList.filter (fun (x,y) -> x < y)
            |> CCList.map (fun p -> (p, false))
            |> CCHashtbl.of_list
          in
          let splits = Ty.gather_all_splits env mty0 in
          splits |> List.iter (fun (discr, branches) ->
              branches |> List.iter (fun {Ty. lhs = _; provs; ty = _ } ->
                  Report.debugf "Found branch %a"
                    Prov.Set.pp provs;
                  Hashtbl.iter (fun (ty1, ty2) b ->
                      if not b then
                        let fit_ty1 =
                          Prov.Set.exists
                            (fun prov -> Types.is_compatible env prov ty1) provs
                        and fit_ty2 =
                          Prov.Set.exists
                            (fun prov -> Types.is_compatible env prov ty2) provs
                        in
                        let distinguishes = fit_ty1 <> fit_ty2 in
                        Hashtbl.replace all_type_pairs (ty1, ty2) distinguishes
                    )
                    all_type_pairs
                )
            );
          let all_uncovered_type_pairs =
            List.filter (fun (_,b) -> not b) @@ CCHashtbl.to_list all_type_pairs
          in 
          if not @@ CCList.is_empty all_uncovered_type_pairs then
            dont_agree name ty0 mty0
              "@[<v 2>there are no provenance distinguishes the following type pairs:@ %a@]"
              Fmt.(vbox @@ list @@ Fmt.hbox @@ pair ~sep:(any "@ and@ ") Types.pp Types.pp)
              (List.map fst all_uncovered_type_pairs)
        | _ -> ()
      end;
      (* Coverage *)
      Report.enter ~__FILE__ ~__LINE__ "coverage" begin fun _ ->
        let branches = Ty.specialize ~prov:Top env mty0 in
        branches |> Prov.Map.iter (fun prov mty ->
            Report.debugf "Found branch %a with memory type %a"
              Prov.pp prov Ty.pp mty;
            if not @@ Types.is_compatible env prov ty0 then begin
              Report.debugf "Dead branch %a with type %a"
                Prov.pp prov Types.pp ty0;
              ()
            end else begin
              let ty = Types.specialize env prov ty0 in
              let all_covered_holes =
                let frags = Ty.gather_frags env mty in
                let hfrags =
                  Hole.Set.of_iter @@ Iter.map fst @@ PathMap.values frags
                in
                if not @@ PathMap.is_empty @@ Ty.gather_primitives env mty then
                  Hole.Set.add Empty hfrags
                else
                  hfrags
              in
              let rec check_full_coverage holeset pa sty =
                if Hole.Set.mem pa holeset then ()
                else match sty with
                  | Types.Type _ ->
                    if not @@
                      Hole.Set.exists (fun pa' -> Hole.is_prefix pa pa') holeset
                    then
                      dont_agree name ty0 mty0
                        "there is no fragment covering variable %a at path %a in the branch %a"
                        Types.pp sty Hole.pp pa Prov.pp prov
                    else
                      check_full_coverage holeset pa (Types.unroll env sty)
                  | Word { width; values } ->
                    if not @@
                      Hole.Set.exists (fun pa' -> Hole.is_prefix pa pa') holeset
                    then
                      dont_agree name ty0 mty0
                        "there is no fragment covering path %a in the branch %a"
                        Hole.pp pa Prov.pp prov
                    else
                      (* TODO Check proper interval coverage *)
                      ()
                  | Product tys ->
                    List.iteri (fun i ty -> 
                        check_full_coverage holeset Hole.(pa $. HAccess i) ty
                      ) tys
                  | Record tys -> 
                    List.iter (fun (s, ty) -> 
                        check_full_coverage holeset Hole.(pa $. HField s) ty
                      ) tys
                  | Sum tys -> 
                    List.iter (fun (c, ty) -> 
                        check_full_coverage holeset Hole.(pa $. HConstr c) ty
                      ) tys
              in
              check_full_coverage all_covered_holes Empty ty
            end
          )
      end;
      (* Subterm coherence *)
      Report.enter ~__FILE__ ~__LINE__ "subterm coherence" begin fun _ -> 
        let branches = Ty.specialize ~prov:Top env mty0 in
        branches |> Prov.Map.iter (fun prov mty ->
            Report.debugf "Found branch %a with memory type %a"
              Prov.pp prov Ty.pp mty;
            if not @@ Types.is_compatible env prov ty0 then begin
              Report.debugf "Dead branch %a with type %a"
                Prov.pp prov Types.pp ty0;
              ()
            end else begin
              let ty = Types.specialize env prov ty0 in
              let frags = Ty.gather_frags env mty in
              frags |> PathMap.iter (fun mpa (pa, sub_mty) ->
                  let pa_prov = Prov.of_path env ty pa in
                  Report.debugf "Found fragment (%a as %a)"
                    Hole.pp pa Ty.pp sub_mty;
                  if Types.is_compatible env pa_prov ty then
                    let sub_ty = Types.focus pa env ty in
                    check_agreement h env name sub_ty sub_mty
                  else
                    dont_agree name ty0 mty0
                      "%a is not a valid source path, it doesn't correspond to any source type"
                      Hole.pp pa
                )
            end
          )
      end;
      (* Provenance coherence *)
      Report.enter ~__FILE__ ~__LINE__ "provenance coherence" begin fun _ -> 
        let splits = Ty.gather_top_splits env mty0 in
        splits |> PathMap.iter (fun _ (discr, branches) ->
            branches |> List.iter (fun {Ty. lhs = _; provs; ty = _ } ->
                Report.debugf "Found branch %a"
                  Prov.Set.pp provs;
                provs |> Prov.Set.iter (fun prov ->
                    let mtys = Ty.specialize ~prov env mty0 in
                    mtys |> Prov.Map.iter (fun prov sub_mty ->
                        if Types.is_compatible env prov ty0 then
                          let sub_ty = Types.specialize env prov ty0 in
                          check_agreement h env name sub_ty sub_mty
                        else
                          ()
                          (* dont_agree name ty0 mty0 *)
                          (*   "%a is not a valid split provenance, \ *)
                          (*    it doesn't correspond to any source type" *)
                          (*   Prov.pp prov *)
                      )
                  )
              )
          )
      end;
    end
  end

let check_agreement env s tys mty =
  check_agreement (Hashtbl.create 17) env s tys mty;
  ()

(** Error handling *)
let prepare_error = function
  | Dont_agree (name, tys, mty, t) ->
    Some (
      Report.errorf "@[<hv>@[<hv>In type %s, the type expressions@;<1 2>@[%a@]@ @]and the memory type@;<1 2>@[%a@]@ @[don't agree:@ %t@]@]"
        name
        (Types.pp) tys
        Mem.Ty.pp mty
        t)
  | Invalid_slice ({ offset; width }, _t) ->
    Some (
      Report.errorf "@[<hv>@[<hv>Invalid offset [%i:+%i]"
        offset width)    
  | _ -> None
let () = Report.register_report_of_exn prepare_error
