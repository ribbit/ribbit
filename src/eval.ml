
let eval_unop (unop : Syntax.unop) (v : Syntax.value) : Syntax.value =
  match unop, v with
  | Minus, VInt v -> VInt (Z.neg v)
  | Not, v when Builtins.Bool.is_value v ->
    Builtins.Bool.wrap not v
  | _ ->
    Report.fail "Operator %a can't be applied to value %a"
      Syntax.pp_unop unop Value.pp v

let eval_binop (binop : Syntax.binop)
  : Syntax.value -> Syntax.value -> Syntax.value =
  let open Builtins in
  let cmp f v v' = Bool.to_ @@ f (Int.of_ v) (Int.of_ v') in
  let int' f v v' = Int.to_ @@ f (Int.of_ v) (Z.to_int @@ Int.of_ v') in 
  match binop with
  | Add -> Int.wrap2 Z.add
  | Sub -> Int.wrap2 Z.sub
  | Mul -> Int.wrap2 Z.mul
  | Div -> Int.wrap2 Z.div
  | Leq -> cmp (<=)
  | Le -> cmp (<)
  | Geq -> cmp (>=)
  | Ge -> cmp (>)
  | Equal -> cmp (=)
  | And -> Bool.wrap2 (&&)
  | Or -> Bool.wrap2 (||)
  | ShiftLeft -> int' Z.shift_left
  | ShiftRight -> int' Z.shift_right
  | BitAnd -> Int.wrap2 Z.logand
  | BitOr -> Int.wrap2 Z.logor
  | BitXor -> Int.wrap2 Z.logxor

let rec expr (decls: Declenv.t) venv : Syntax.expr -> Syntax.value =
  function
  | EPrim1 (unop, e) ->
    let v = expr decls venv e in
    eval_unop unop v
  | EPrim2 (binop, e, e') ->
    let v = expr decls venv e in
    let v' = expr decls venv e' in
    eval_binop binop v v'
  | EApp (f, args) ->
    let venv = Name.Map.fold (fun x e acc ->
        let v = expr decls venv e in
        Name.Map.add x v acc)
      args
      venv
    in
    begin match Name.Map.get f decls with
      | None ->
        Report.fail "Function %s not found during source evaluation" f
      | Some Expr _ ->
        Report.fail "%s is not a function, it can't be applied." f
      | Some Fun body -> 
        expr decls venv body.source
    end 
  | EMatch (e, cls) ->
    let v = expr decls venv e in    
    matching decls venv cls v
  | ELet (x, e, e') ->
    let v = expr decls venv e in
    expr decls (Name.Map.add x v venv) e'
  | EVar x ->
    begin match Name.Map.get x venv with
      | Some v -> v
      | None -> Report.fail "Value %s not found during source evaluation" x
    end
  | EInt z -> VInt z
  | ETuple l ->
    let l = List.map (expr decls venv) l in
    VTuple l
  | ERecord l ->
    let l = List.map (fun (n, v) -> n, expr decls venv v) l in
    VRecord l
  | EConstr (constr, arg) ->
    Syntax.VConstr (constr, expr decls venv arg)
  | ECstr (e, _ty) ->
    expr decls venv e

and matching decls venv clauses0 v0 : Syntax.value =
  let rec match_pat env p v : (Syntax.value Name.Map.t) option =
    match p, v with
    | Syntax.PAny, _ -> Some env
    | PVar x, _ -> Some (Name.Map.add x v env)
    | PInt z, Value.VInt z' when Z.equal z z' -> Some env
    | PTuple ps, VTuple vs when List.length ps = List.length vs ->
      List.fold_left2 (fun acc p v -> match acc with
          | None -> None
          | Some env -> match_pat env p v)
        (Some env)
        ps vs
    | PRecord ps, VRecord vs when List.length ps = List.length vs ->
      List.fold_left2 (fun acc (x, p) (x', v) ->
          if x = x' then match acc with
            | None -> None
            | Some env -> match_pat env p v
          else None)
        (Some env)
        ps vs
    | PConstr (k, p), VConstr (k', v) when k = k' -> match_pat env p v
    | POr (p, p'), _ -> begin match match_pat env p v with
      | Some env -> Some env
      | None -> match_pat env p' v
      end
    | _ -> None
  in
  let rec match_all clauses v = match clauses with
    | [] ->
      Report.fail "Not matching pattern during source evaluation"
    | {Syntax. pattern; expr = e} :: t ->
      begin match match_pat venv pattern v0 with
        | Some env -> expr decls env e
        | None -> match_all t v
      end
  in
  match_all clauses0 v0
