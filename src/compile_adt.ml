module Ty = Mem.Ty
module Path = Mem.Path
module PathMap = Mem.PathMap
type name = Name.t

let (@-@) = Target.concat_prog

let explore
  : Env.t -> Prov.t -> Types.ty -> Ty.t -> _
  = fun env prov ty mty f ->
    Ty.specialize ~prov env mty
    |> Prov.Map.rewrite (fun (prov_i, mty_i) ->
        let ty_i = Types.specialize env prov ty in
        let frags_i = Ty.gather_frags env mty_i in
        let prims_i = Ty.gather_primitives env mty_i in
        f prov_i ty_i mty_i frags_i prims_i
      ) 

type in_args = {id: Target.in_id; ty: Types.ty; mty: Ty.t; prov: Prov.t}

type out_args = {id: Target.out_id; ty: Types.ty; mty: Ty.t}

let pp_in_args fmt {id; ty; mty; prov} =
  Fmt.pf fmt "{id: %a; ty: %a; mty: %a; prov: %a}"
    Target.pp_iid id Types.pp ty Ty.pp mty Prov.pp prov

let pp_out_args fmt {id; ty; mty} =
  Fmt.pf fmt "{id: %a; ty: %a; mty: %a}" Target.pp_oid id Types.pp ty Ty.pp mty

type side =
  | Out
  | In
let pp_side fmt = function
  | Out -> Fmt.pf fmt "Rebuild"
  | In -> Fmt.pf fmt "Seek"

type input_hash =
  { occurs: Hole.t Hole.Map.t; ty: Types.ty; mty: Ty.t; prov: Prov.t }
type memo_hash = {
  side: side; inputs: input_hash list;
  out_ty: Types.ty; out_mty: Ty.t; expr: PathExpr.t }

let pp_input_hash fmt { occurs; ty; mty; prov } =
  Fmt.pf fmt "(%a) < %a : %a as %a"
    (Hole.Map.pp Hole.pp Hole.pp) occurs
    Prov.pp prov Types.pp ty Ty.pp mty
let pp_memo_hash fmt { side; inputs; out_ty; out_mty; expr } =
  Fmt.pf fmt "Memo: %a(%a,@ out : %a as %a,@ %a)"
    pp_side side Fmt.(list ~sep:comma pp_input_hash) inputs
    Types.pp out_ty Ty.pp out_mty PathExpr.pp expr

(* replace every variable name with its occurrences in expr,
   yeet those that do not appear in expr *)
let hash_args side in_args (out_args:out_args) expr =
  let inputs = Name.Map.fold (fun x {id; ty; mty; prov} inputs ->
      let occurs = PathExpr.occurrences x expr in
      if Hole.Map.is_empty occurs then
        inputs
      else
        { occurs; ty; mty; prov } :: inputs)
    in_args
    []
  in
  let expr = PathExpr.erase_vars expr in
  { side; inputs; out_ty = out_args.ty; out_mty = out_args.mty; expr }

type memo_status = NotCalled | Called
type memo_table = (memo_hash, Target.fun_id * memo_status) Hashtbl.t
let pp_memo_table =
  CCHashtbl.pp pp_memo_hash
    (fun fmt (Target.FId f, _) -> Name.pp fmt f)

let rec memoize :
  side -> memo_table -> _ -> _ -> Env.t ->
  in_args Name.Map.t -> out_args ->
  PathExpr.t -> Target.program =
  fun side memo_tab rebuild -> 
  fun logger env xins xout expr ->
  let in_ids = Name.Map.map (fun {id; ty; mty; prov} -> id) xins in
  let h = hash_args side xins xout expr in
  match Hashtbl.find_opt memo_tab h with
    | Some (f, _) ->
      Hashtbl.replace memo_tab h (f, Called);
      let call = Target.Call (f, in_ids, xout.id, Success) in 
      [], call
    | None ->
      let f = Target.fresh_fun_id () in
      Hashtbl.add memo_tab h (f, NotCalled);
      let funs, body =
        rebuild logger env xins xout expr
      in
      match Hashtbl.find memo_tab h with
      | _, Called ->
        let call = Target.Call (f, in_ids, xout.id, Success) in
        let fdef = f, in_ids, xout.id, body in
        fdef :: funs, call
      | _, NotCalled ->
        Hashtbl.remove memo_tab h;
        funs, body

(** [refine y sh sh'] emits target code that, given a memory value of
    shape [sh] stored in the location bound to [y], allocates and casts
    memory so that it assumes the new shape [sh']. *)
let rec refine (y : Target.out_id) sh sh' = let open Mem.Shape in
  match sh, sh' with
  | _ when sh = sh' ->
    Target.Success
  | SUnk width0, SImm { width; value } when width0 = width ->
    Target.Write (y, Imm value, Success)
  | SUnk width0, SPtr { width; pointee } when width0 = width ->
    let width' = size_of pointee in
    let next = refine y (SPtr { width; pointee = SUnk width' }) sh' in
    Target.Write (y, Alloc width', next)
  | SUnk width, SWord { base; range; spec } when size_of base = width ->
    let next =
      refine y (SWord { base = SUnk width; range; spec = SUnk range.width }) sh'
    in
    Target.Cast (y, ShSpec (width, range), next)
  | SUnk width, SStruct fields
    when List.fold_left (fun n sh -> n + size_of sh) 0 fields = width ->
    let next = refine y (SStruct (List.map (fun sh -> SUnk (size_of sh)) fields)) sh' in
    Target.Cast (y, ShStruct (List.map size_of fields), next)
  | SPtr ptr, SPtr ptr' when ptr.width = ptr'.width ->
    let path_pointee = Path.(Empty $. Deref) in
    let y_pointee = Target.hashed_dest (y, path_pointee) in
    let next = refine y_pointee ptr.pointee ptr'.pointee in
    Target.Let (SubOut (y_pointee, y, path_pointee), next)
  | SWord word, SWord word' when word.range = word'.range ->
    let pbase = Path.(Empty $. Mask word.range) in
    let ybase = Target.hashed_dest (y,pbase) in
    let ebase = refine ybase word.base word'.base in
    let pspec = Path.(Empty $. Bits word.range) in
    let yspec = Target.hashed_dest (y,pspec) in
    let espec = refine yspec word.spec word'.spec in
    Target.concat
      (Let (SubOut (ybase, y, pbase), ebase))
      (Let (SubOut (yspec, y, pspec), espec))
  | SStruct fields, SStruct fields'
    when List.compare_lengths fields fields' = 0 ->
    CCList.foldi2 (fun irexp i sh sh' ->
        let path_i = Path.(Empty $. Access i) in
        let y_i = Target.hashed_dest (y, path_i) in
        let irexp_i = refine y_i sh sh' in
        Target.concat
          irexp 
          (Target.Let (SubOut (y_i, y, path_i), irexp_i)))
      Target.Success
      fields fields'
  | _ ->
    Report.fail "cannot refine shape %a to %a" Mem.Shape.pp sh Mem.Shape.pp sh'

let refine (y : Target.out_id) sh sh' = 
  Report.(enter ~__FILE__ ~__LINE__
            "Refine"
            ~args:[
              "x", d Target.pp_oid y;
              "sh old", d Mem.Shape.pp sh;
              "sh new", d Mem.Shape.pp sh';
            ]) @@ fun _ ->
  refine y sh sh'

let extract env0 xin0 path0 =
  let exception Cannot_extract in
  let rec aux_extract :
    in_args -> Hole.t -> Path.t * Types.ty * Ty.t = fun xin path ->
    Report.(enter ~__FILE__ ~__LINE__
              "Extract"
              ~args:[
                "in args", d pp_in_args xin;
                "hole", d Hole.pp path;
              ]) @@ fun _ ->
    match path with
    | Empty ->
      (* This is exactly the right fragment *)
      Path.Empty, xin.ty, xin.mty
    | _ ->
      let branches = explore env0 xin.prov xin.ty xin.mty @@
        fun prov_i ty_i mty_i frags_i prims_i ->
        Report.(enter ~__FILE__ ~__LINE__
                  "Exploring In"
                  ~args:["prov", d Prov.pp prov_i;
                         "ty", d Types.pp ty_i;
                         "mty", d Ty.pp mty_i]) @@ fun _ ->
        (* Collect the candidates in prims_i *)
        let candidate_from_primitive (mpath_j, mty_j) =
          Report.debugf "Primitive Type (%a → %a)" Path.pp mpath_j Ty.pp mty_j;
          assert (Types.is_leaf env0 ty_i);
          let mpath_in_prim =
            let rec mk_bitrange_mpath = function
              | Hole.Empty -> Path.Empty
              | Hole.Dot (p, HBits {offset; width}) ->
                Path.(mk_bitrange_mpath p $. Bits {offset;width})
              | _ -> assert false
            in
            mk_bitrange_mpath path
          in
          let mpath = Path.(mpath_j ++ mpath_in_prim) in
          Some (1, lazy (
              let id = Target.hashed_src (xin.id, mpath) in
              let xin' =
                {id;
                 ty = Types.focus path env0 ty_i;
                 mty = Ty.focus env0 mpath_in_prim mty_j;
                 prov = Top}
              in
              let mp, ty, mty =
                aux_extract xin' Empty
              in
              Path.(mpath ++ mp), ty, mty
            ))
        in
        (* Collect the candidates in frags_i *)
        let candidate_from_fragment (mpath_j, (path_j, mty_j)) =
          Report.debugf "Fragment (%a → (%a as %a))" Path.pp mpath_j Hole.pp path_j Ty.pp mty_j;
          if Hole.is_prefix path_j path then begin
            Report.debugf "Candidate Prefix %a" Hole.pp path_j;
            Some (2, lazy (
                let id = Target.hashed_src (xin.id, mpath_j) in
                let xin' =
                  {id;
                   ty = Types.focus path_j env0 ty_i;
                   mty = mty_j;
                   prov = Prov.focus path_j prov_i}
                in
                let path' = Hole.focus path_j path in
                let mp, ty, mty = aux_extract xin' path' in
                Path.(mpath_j ++ mp), ty, mty
              ))
          end
          else None
        in
        let candidates =
          List.filter_map candidate_from_fragment (PathMap.bindings frags_i)
          @ List.filter_map candidate_from_primitive (PathMap.bindings prims_i)
        in
        (* Sort the candidates by how cheap they look *)
        (* TODO Improve this sorting for better heuristics *)
        let best_candidate =
          candidates
          |> List.sort CCOrd.(map fst int)
          |> CCList.head_opt
        in
        let (pa, _, _) as res = match best_candidate with
          | Some (_, lazy res) -> res
          | None -> raise Cannot_extract
        in
        Report.debugf "Computed %a → %a" Prov.pp prov_i Path.pp pa;
        Some (prov_i, res)
      in
      match Prov.Map.bindings branches with
      | [] -> assert false
      | (_, ((p, ty, mty) as sol)) :: rest
        when List.for_all (fun (_,sol') -> sol = sol') rest ->
        Report.debugf "mpath=%a" Path.pp p;
        p, ty, mty
      | _ -> raise Cannot_extract
  in
  try Some (aux_extract xin0 path0)
  with Cannot_extract -> None

(** [seek logger env recmap in_args out_args path next] builds a target expr that
    seeks the fragment specified by out_args located at path within the input source
    val by destructing the input memory value specified by in_args
    writes the result in out_args.id and continues with next. *)
let rec seek :
  memo_table -> _ -> Env.t ->
  in_args Name.Map.t -> out_args ->
  PathExpr.t -> Target.program =
  fun memo_tab ->
  memoize In memo_tab @@ fun logger env xins xout expr ->
  Report.(enter ~__FILE__ ~__LINE__
            "Seek"
            ~args:[
              "memo_tab", d pp_memo_table memo_tab;
              "in args", d (Name.Map.pp pp_in_args) xins;
              "out args", d pp_out_args xout;
              "expr", d PathExpr.pp expr;
            ]) @@ fun _ ->
  match expr with
  | EPos (root, path) -> begin
      let xin = Name.Map.find root xins in
      (* Path and prov should always be compatible *)
      assert (Prov.compat_path path xin.prov);
      (* First check for immediate resolution *)
      match extract env xin path with
      | Some (mpath, ty, mty) when Ty.equiv env mty xout.mty ->
        (* This is exactly the right fragment *)
        let in_temp = Target.hashed_src (xin.id, mpath) in
        let res =
          Target.Let (SubIn (in_temp, xin.id, mpath),
                      Target.Write (xout.id, CopyIn in_temp, Success)) in
        Report.debugf "contexpr=%a" Target.pp res;
        [], res
      | Some (mpath, ty, mty) when
          Ty.is_leaf env xin.mty &&
          Ty.is_leaf env xout.mty
        ->
        (* This is the right fragment, but we need to cast a constant *)
        (* So far, we have only one type of atoms *)
        let in_temp = Target.hashed_src (xin.id, mpath) in
        let res =
          Target.Let (SubIn (in_temp, xin.id, mpath),
                      Target.Write (xout.id, CopyIn in_temp, Success)) in
        Report.debugf "contexpr=%a" Target.pp res;
        [], res
      | _ ->
      (* For each branch in mty_in *)
      let branches = explore env xin.prov xin.ty xin.mty @@
        fun prov_i ty_i mty_i frags_i prims_i ->
        Report.(enter ~__FILE__ ~__LINE__
                  "Explore"
                  ~args:["prov", d Prov.pp prov_i;
                         "ty", d Types.pp ty_i;
                         "mty", d Ty.pp mty_i]) @@ fun _ ->
        (* Collect the candidates in prims_i *)
        let candidate_from_primitive (mpath_j, mty_j) =
          Report.debugf "Primitive Type (%a → %a)" Path.pp mpath_j Ty.pp mty_j;
          assert (Types.is_leaf env ty_i);
          if path = Hole.Empty then
            None
          else 
            let mpath_in_prim =
              let rec mk_bitrange_mpath = function
                | Hole.Empty -> Path.Empty
                | Hole.Dot (p ,HBits {offset; width}) ->
                  Path.(mk_bitrange_mpath p $. Bits {offset;width})
                | _ -> assert false
              in
              mk_bitrange_mpath path
            in
            let mpath = Path.(mpath_j ++ mpath_in_prim) in
            Some (1, lazy (
                let id = Target.hashed_src (xin.id, mpath) in
                let xin' =
                  {id;
                   ty = Types.focus path env ty_i;
                   mty = Ty.focus env mpath_in_prim mty_j;
                   prov = Top}
                in
                let funs, res = seek
                    memo_tab logger env
                    (Name.Map.singleton root xin') xout
                    (EPos (root, Empty))
                in
                funs, Target.Let (SubIn (id, xin.id, mpath), res)
              ))
        in
        (* Collect the candidates in frags_i *)
        let candidate_from_fragment (mpath_j, (path_j, mty_j)) =
          Report.debugf "Fragment (%a → (%a as %a))" Path.pp mpath_j Hole.pp path_j Ty.pp mty_j;
          if Hole.is_prefix path_j path then begin
            Report.debugf "Candidate Prefix %a" Hole.pp path_j;
            (* This is the right fragment, but with a different memory type *)
            Some (2, lazy (
                let id = Target.hashed_src (xin.id, mpath_j) in
                let xin' =
                  {id; ty = Types.focus path_j env ty_i;
                   mty = mty_j; prov = Prov.focus path_j prov_i}
                in
                let path' = Hole.focus path_j path in
                let funs, res = seek
                  memo_tab logger env
                  (Name.Map.singleton root xin') xout
                  (EPos (root, path'))
                in
                funs, Target.Let (SubIn (id, xin.id, mpath_j), res)
              ))
          end
          else None
        in
        (* Try to destruct the output *)
        let out_candidate = lazy (
          let xin' = {id = xin.id; ty = ty_i; mty = mty_i; prov = prov_i} in
          let funs, res = rebuild
            memo_tab logger env
            (Name.Map.singleton root xin') xout
            (EPos (root, path))
          in
          funs, res)
        in
        let candidates =
          (3, out_candidate)
          :: List.filter_map candidate_from_fragment (PathMap.bindings frags_i)
          @ List.filter_map candidate_from_primitive (PathMap.bindings prims_i)
        in
        (* Sort the candidates by how cheap they look *)
        (* TODO Improve this sorting for better heuristics *)
        let best_candidate =
          candidates
          |> List.sort CCOrd.(map fst int)
          |> CCList.head_opt
        in
        let funs, res = match best_candidate with
          | Some (_, lazy (funs, res)) -> funs, res
          | None -> failwith "No candidate!"
        in
        Report.debugf "Computed %a → %a" Prov.pp prov_i Target.pp res;
        Some (prov_i, (funs, res))
      in
      let funs = Prov.Map.fold (fun _ (funs, _) acc -> funs @ acc) branches [] in
      let branches = List.map (fun (prov, (_, prog)) ->
          Name.Map.singleton root prov, prog)
        (Prov.Map.bindings branches)
      in
      let res = MemoryTree.from_provs logger env
        (Name.Map.singleton root (xin.id, xin.prov, xin.mty))
        branches
      in
      Report.debugf "contexpr=%a" Target.pp res;
      funs, res
    end
  | e ->
    rebuild memo_tab logger env xins xout e

(* [rebuild logger env recmap in_args out_args path next] builds a target expr that
   rebuilds the fragment specified by out_args by destructing the output memory type,
   located at path within the input source value,
   with source and destination locations specified by in_args and out_args,
   and continues with the next continuation specified by cont_args. *)
and rebuild :
  memo_table -> _ -> Env.t ->
  in_args Name.Map.t -> out_args ->
  PathExpr.t -> Target.program =
  fun memo_tab ->
  memoize Out memo_tab @@ fun logger env xins xout expr ->
  Report.(enter ~__FILE__ ~__LINE__
            "Rebuild"
            ~args:[
              "memo_tab", d pp_memo_table memo_tab;
              "in args", d (Name.Map.pp pp_in_args) xins;
              "out args", d pp_out_args xout;
              "expr", d PathExpr.pp expr;
           ]) @@ fun _ ->
  match expr with
  | EInt z when Mem.Ty.is_leaf env xout.mty ->
    let next = Target.Write (xout.id, Imm z, Success) in
    Report.debugf "Constant %a.@ Emitted %a" PathExpr.pp expr Target.pp next;
    [], next
  (* special case: atoms may need to be broken down even further *)
  (* TODO think about how this case integrates with the rest of rebuild/seek *)
  | EPos (root, path) when Mem.Ty.is_leaf env xout.mty ->
    (* this looks like seek, but seek just defers to rebuild in this case *)
    let xin = Name.Map.find root xins in
    (* For each branch in mty_in (not a typo) *)
    let branches = explore env xin.prov xin.ty xin.mty @@
      fun prov_i ty_i mty_i frags_i prims_i ->
      Report.(enter ~__FILE__ ~__LINE__
                "Explore"
                ~args:["prov", d Prov.pp prov_i;
                       "ty", d Types.pp ty_i;
                       "mty", d Ty.pp mty_i]) @@ fun _ ->
      (* we want to call seek if a prefix fragment exists *)
      if not (PathMap.is_empty prims_i) ||
         PathMap.exists (fun _ (h, _) -> Hole.is_prefix h path) frags_i
      then
        Some (prov_i, seek memo_tab logger env xins xout expr)
      (* otherwise, we have to break down the atom (FISSION, booooom) *)
      else
        let rec split_atom_path (h:Hole.t) : Hole.t * Hole.t =
          match Hole.pop h with
          | None -> Empty, Empty
          | Some (HBits _, _) -> Empty, h
          | Some (op, h) ->
            let before, after = split_atom_path h in
            Hole.push op before, after
        in
        let path_before_atom, path_after_atom = split_atom_path path in
        let rec flatten_range range0 (h:Hole.t) : Base_types.bitrange =
          match Hole.pop h with
          | None -> range0
          | Some (HBits {offset; width}, h) ->
            let offset = range0.offset + offset in
            flatten_range { offset; width } h
          | Some (_, _) -> assert false
        in
        let width = match Types.focus path_before_atom env ty_i with
          | Types.Word { width; _ } -> width
          | _ -> assert false
        in
        let range = flatten_range { offset = 0; width } path_after_atom in
        (* TODO this would be way less painful with n-ary word spec *)
        (* gather all fragments in the input that are relevant to this atom *)
        let funs, next, _ = PathMap.fold
          (fun mpath_j (path_j, mty_j) (funs, next, cur_oid) ->
            let path_j_before_atom, path_j_after_atom = split_atom_path path_j in
            (* is this fragment part of the atom we're interested in? *)
            if path_j_before_atom = path_before_atom then
              let range_j = flatten_range { offset = 0; width } path_j_after_atom in
              (* does this piece overlap with the one we're trying to rebuild? *)
              if range_j.offset <= range.offset + range.width &&
                range_j.offset + range_j.width >= range.offset
              then
                (* discard irrelevant bits and figure out where to copy relevant bits *)
                let (extract_range, insert_range) : Base_types.bitrange * Base_types.bitrange =
                  let end_ = range.offset + range.width
                  and end_j = range_j.offset + range_j.width in
                  let end_delta = end_j - end_
                  and offset_delta = range_j.offset - range.offset in
                  if offset_delta < 0 then
                    if end_delta < 0 then
                      (* o_j  o  o_j+w_j  o+w *)
                      let width = range_j.width + offset_delta in
                      { offset = -offset_delta; width },
                      { offset = 0; width }
                    else
                      (* o_j  o  o+w  o_j+w_j *)
                      { offset = -offset_delta; width = range.width },
                      { offset = 0; width = range.width }
                  else
                    if end_delta < 0 then
                      (* o  o_j  o_j+w_j  o+w *)
                      { offset = 0; width = range_j.width },
                      { offset = offset_delta; width = range_j.width }
                    else
                      (* o  o_j  o+w  o_j+w_j *)
                      let width = range.width - offset_delta in
                      { offset = 0; width },
                      { offset = offset_delta; width }
                in
                let extract_path = Hole.(path_j $. HBits {offset=extract_range.offset; width=extract_range.width})
                and extract_mpath = Path.(mpath_j $. Bits extract_range) in
                let extract_id = Target.hashed_src (xin.id, extract_mpath) in
                let extract_xin =
                  { id = extract_id; ty = Types.focus extract_path env ty_i;
                    mty = mty_j; prov = Prov.Top }
                in
                let insert_id = Target.hashed_dest (cur_oid, Path.(Empty $. Bits insert_range))
                and rest_id = Target.hashed_dest (cur_oid, Path.(Empty $. Mask insert_range)) in
                let insert_xout =
                  { id = insert_id; ty = Types.Word {width=insert_range.width;values=()};
                    mty = Ty.TWord {width = insert_range.width; specs = PathMap.empty} }
                in
                let funs', next' =
                  seek memo_tab logger env (Name.Map.singleton root extract_xin)
                  insert_xout (PathExpr.EPos (root, Hole.(Empty $. HBits {offset=extract_range.offset;width=extract_range.width})))
                in
                let next' = Target.Cast (cur_oid, ShSpec (width, insert_range),
                  Target.Let (SubOut (insert_id, cur_oid, Path.(Empty $. Bits insert_range)),
                  Target.Let (SubOut (rest_id, cur_oid, Path.(Empty $. Mask insert_range)),
                  next'))) in
                funs @ funs', Target.concat next next', rest_id
              else
                funs, next, cur_oid
            else
              funs, next, cur_oid)
          frags_i
          ([], Target.Success, xout.id)
        in
        (* recast to int *)
        let res = Target.concat next @@ Target.Cast (xout.id, ShInt width, Success) in
        Report.debugf "Computed %a → %a" Prov.pp prov_i Target.pp res;
        Some (prov_i, (funs, res))
      in
      let funs = Prov.Map.fold (fun _ (funs, _) acc -> funs @ acc) branches [] in
      let branches = List.map (fun (prov, (_, prog)) ->
          Name.Map.singleton root prov, prog)
        (Prov.Map.bindings branches)
      in
      let res = MemoryTree.from_provs logger env
        (Name.Map.singleton root (xin.id, xin.prov, xin.mty))
        branches
      in
      Report.debugf "target expr = %a" Target.pp res;
      funs, res
  | _ ->
    let out_pos_tbl = PathExpr.gather_pos expr in
    (* Filter inputs that are actually used in expr *)
    let xins = Name.Map.filter (fun root _ ->
        Hole.Map.exists (fun _ (root', _) -> root' = root) out_pos_tbl)
      xins
    in
    let in_provs = Name.Map.map (fun (xin:in_args) -> xin.prov) xins in
    let out_prov =
        let p = PathExpr.prov_of expr in
        Prov.remap_out in_provs p out_pos_tbl
    in
    let out_shape = Mem.Shape.of_ty env xout.mty in
    let branches = explore env out_prov xout.ty xout.mty @@
      fun prov_i ty_i mty_i frags_i prims_i ->
        Report.(enter ~__FILE__ ~__LINE__
                  "Explore"
                  ~args:[ "prov", d Prov.pp prov_i;
                          "ty", d Types.pp ty_i;
                          "mty", d Ty.pp mty_i;
                        ]) @@ fun _ ->
        (* actual allocation takes place at the very beginning, see end of func *)
        let shape_i = Mem.Shape.of_ty env mty_i in
        let allocs_and_casts = refine xout.id out_shape shape_i in
        (* The equivalent input prov for this branch *)
        let in_provs' = Name.Map.mapi (fun root prov ->
            Prov.remap_in root prov prov_i out_pos_tbl)
          in_provs
        in
        assert (Name.Map.for_all (fun x p -> Prov.compat p @@ Name.Map.find x in_provs') in_provs);
        let xins' = Name.Map.mapi (fun root (xin:in_args) ->
            {xin with prov = Name.Map.find root in_provs'})
          xins
        in
        (* For each primitive type: *)
        let prim_programs = PathMap.fold (fun mpos_j mty_j prog_rest ->
            Report.debugf "@[Primitive Type (%a → %a)@]@," Path.pp mpos_j Ty.pp mty_j;
            assert (Types.is_leaf env ty_i);
            let id = Target.hashed_dest (xout.id, mpos_j) in
            let xout' = {id; ty = ty_i; mty = mty_j} in
            let prog = match expr with
              | EPos (root, pos_var) ->
                seek memo_tab logger env xins' xout' expr
              | e ->
                rebuild memo_tab logger env xins' xout' expr
            in
            (* alloc/cast stuff that we'll need in all cases *)
            let allocs_and_casts_j =
              let shape_j = Mem.Shape.of_ty env mty_j in
              refine xout'.id Mem.Shape.(SUnk (size_of_ty env mty_j)) shape_j
            in
            prog_rest
            @-@ ([], Target.comment Success
                   "Primitive in dest %a" Target.pp_oid xout'.id)
            @-@ ([], Target.Let (SubOut (xout'.id, xout.id, mpos_j), Success))
            @-@ ([], allocs_and_casts_j)
            @-@ prog
          )
            prims_i
            ([], Target.Success)
        in
        (* For each fragment: *)
        let fragment_programs = PathMap.fold (fun mpos_j (pos_j, mty_j) prog_rest ->
            Report.debugf "@[Fragment (%a → (%a as %a))@]@," Path.pp mpos_j Hole.pp pos_j Ty.pp mty_j;
            let id = Target.hashed_dest (xout.id, mpos_j) in
            let xout' = {id; ty = Types.focus pos_j env ty_i; mty = mty_j} in
            let prog = match PathExpr.focus_below_expr pos_j expr with
              | EPos (root, pos_var), o ->
                (* TOCHECK *)
                let pos_in = match o with
                  | None -> pos_var
                  | Some p -> Hole.(pos_var ++ p)
                in
                seek memo_tab logger env xins' xout' (EPos (root, pos_in))
              | e, None ->
                rebuild memo_tab logger env xins' xout' e
              | _ -> assert false
            in
            (* alloc/cast stuff that we'll need in all cases;
               right now the fragment dest is opaque *)
            let allocs_and_casts_j =
              let shape_j = Mem.Shape.of_ty env mty_j in
              refine xout'.id Mem.Shape.(SUnk (size_of_ty env mty_j)) shape_j
            in
            prog_rest
            @-@ ([], Target.comment Success
                   "Fragment %a in dest %a" Hole.pp pos_j Target.pp_oid xout'.id)
            @-@ ([], Target.Let (SubOut (xout'.id, xout.id, mpos_j), Success))
            @-@ ([], allocs_and_casts_j)
            @-@ prog
          )
            frags_i
            ([], Target.Success)
        in
        let p = 
          ([], Target.comment Success "Shape %a to %a"
             Mem.Shape.pp out_shape Mem.Shape.pp shape_i)
          @-@ ([], allocs_and_casts)
          @-@ prim_programs
          @-@ fragment_programs
        in
        Report.debugf "Computed %a → %a" Prov.pp prov_i Target.pp_program p;
        Some (prov_i, (in_provs', p))
    in
    let args =
      Name.Map.map
        (fun ({id; mty; prov; _} : in_args) ->
           id, prov, mty)
        xins        
    in
    let funs =
      Prov.Map.fold (fun _ (_, (funs, _)) acc -> funs @ acc) branches []
    in
    let branches = List.map
      (fun (_, (provs, (_, prog))) -> provs, prog)
      (Prov.Map.bindings branches)
    in
    let expr = MemoryTree.from_provs logger env args branches in
    funs, expr

let extract_or_seek ~base logger env xins ty_out mty_out (root, path) =
  let xin = Name.Map.find root xins in
  match extract env xin path with
  | Some (Empty, ty, mty) when Ty.equiv env mty mty_out ->
    xin.id, ([], Target.Success)
  | Some (mpath, ty, mty) when Ty.equiv env mty mty_out ->
    let in_temp = Target.hashed_src ~base (xin.id, mpath) in
    let prog =
      ([], Target.Let (SubIn (in_temp, xin.id, mpath), Success))
    in
    in_temp, prog
  | _ ->
    let src_arg = Target.fresh_in_id ~base () in
    let shape = Mem.Shape.of_ty env mty_out in
    let dest_arg = Target.fresh_out_id ~base () in
    let memo_tab = Hashtbl.create 17 in
    let xout = { id = dest_arg ; ty = ty_out ; mty = mty_out } in
    let prog = 
      ([], Let (NewOut (dest_arg, Mem.Shape.size_of shape),
                refine dest_arg Mem.Shape.(SUnk (size_of shape)) shape))
      @-@
      seek memo_tab logger env xins xout (EPos (root, path))
      @-@ ([], Freeze (dest_arg, src_arg, Success))
    in
    src_arg, prog

let rec clause_to_mem logger env (xins0:in_args Name.Map.t) discrs dest (provs, expr) =
  (* Report.(enter ~__LINE__ ~__FILE__ "Rebuild"
            ~args:["ty_in", d Types.pp ty_in;
                   "expr", d PathExpr.pp expr]
         ) @@ fun _ -> *)
  let provmap =
    Name.Map.of_list (List.combine discrs provs)
  in
  let xins =
    Name.Map.fold (fun discr prov xins ->
        let xin : in_args = Name.Map.find discr xins in
        Name.Map.add discr {xin with prov} xins
      )
      provmap xins0
  in
  let prog =
    ([], Target.comment Success "case %a"
       (Fmt.list ~sep:Fmt.comma Prov.pp) provs) @-@
    expr_to_mem logger env xins dest expr
  in 
  provmap, prog
  

and match_to_mem logger env (xins:in_args Name.Map.t) dest {TypedExpr. matchee; clauses} =
  Report.(enter ~__LINE__ ~__FILE__ "match2mem") @@ fun _ ->
  let match_outs =
    List.map 
      (clause_to_mem logger env xins matchee dest)
      clauses
  in
  let funs =
    CCList.flat_map
      (fun (_, (funs, _)) -> funs)
      match_outs
  in
  let match_outs = List.map (fun (provmap, (_, out)) -> provmap, out) match_outs in
  let args =
    Name.Map.of_list @@
    List.map (fun discr ->
        let xin = Name.Map.find discr xins in
        discr, (xin.id, xin.prov, xin.mty)
      )
      matchee
  in
  let e =
    let matchproblem = MatchProblem.from_prov_branches match_outs in
    MemoryTree.assemble ~optimize:true logger env args matchproblem
  in
  Report.debugf "match2mem result: %a" Target.pp e;
  funs, e

and expr_to_mem logger env xins dest (expr : TypedExpr.t) =
  match expr.desc with
  | PathExpr pexpr ->
    let memo_tab = Hashtbl.create 17 in
    let xout = { id = dest ; ty = expr.ty ; mty = expr.mty } in
    seek memo_tab logger env xins xout pexpr
  | Let (n, e1, e2) ->
    let sid1, prog1 =
      match e1.desc with
      | PathExpr (EPos (n', p)) -> 
        let src_arg, prog =
          extract_or_seek ~base:n logger env xins e1.ty e1.mty (n', p)
        in
        src_arg, prog
      | _ ->
        let sid1 = Target.fresh_in_id ~base:n () in
        let did1 = Target.fresh_out_id ~base:n () in
        let sh1 = Mem.Shape.of_ty env e1.mty in
        let prog = 
          ([], Let (NewOut (did1, Mem.Shape.size_of sh1),
                    refine did1 Mem.Shape.(SUnk (size_of sh1)) sh1))
          @-@ expr_to_mem logger env xins did1 e1
          @-@ ([], Freeze (did1, sid1, Success))
        in
        sid1, prog
    in
    let prog1 =
      if prog1 = ([], Success) then prog1
      else
        ([], Target.comment Success "let %a = %a" Name.pp n TypedExpr.pp e1)
        @-@ prog1
    in        
    let prog2 =
      let env = Env.add_var n (Val e1.ty) env in
      let xins = Name.Map.add n
          { id=sid1; ty=e1.ty; mty=e1.mty; prov = Top } xins
      in
      let did2 = dest in
      expr_to_mem logger env xins did2 e2
    in
    prog1
    @-@ prog2
  | Call (f, args) ->
    let sids =
      Name.Map.map (fun var -> (Name.Map.find var xins).id) args
    in          
    ([], Target.Call (FId f, sids, dest, Success))
  | Matching m ->
    ([], Target.comment Success "match %a"
       (Fmt.list ~sep:Fmt.comma Name.pp) m.matchee)
    @-@ match_to_mem
      logger env xins
      dest
      m
  | Prim (prim, args) ->
    let build_arg n =
      let ty = match Env.find_var n env with
        | Val ty -> ty
        | Fun _ ->
          Report.fail "%a is a function, a value was expected" Name.pp n
      in
      let word_mty = Reprs.Wordlike.mem_ty env ty in
      let src_arg, prog =
        extract_or_seek ~base:n logger env xins ty word_mty (n, Empty)
      in
      if prog = ([], Success) then
        (src_arg, prog)
      else
        let prog =
          ([], Target.comment
             Success "prim %a argument %a " Target.pp_prim prim Name.pp n)
          @-@ prog
        in
        src_arg, prog
    in
    let src_args, code_args =
      let l = List.map build_arg args in
      let srcs, codes = List.split l in
      srcs, CCList.reduce_exn Target.concat_prog codes
    in
    
    let ty_out = expr.ty in
    let word_mty_out = Reprs.Wordlike.mem_ty env ty_out in
    let bare_shape_out = Mem.Shape.of_ty env word_mty_out in

    let prim_call = 
      let src_out = Target.fresh_in_id () in
      let xin =
        { id = src_out; ty = ty_out ; mty = word_mty_out ; prov = Top }
      in
      match extract env xin Empty with
      | Some (Empty, _, mty) when Ty.equiv env mty expr.mty ->
        ([], Target.Prim (prim, dest, src_args, Success))        
      | _ ->
        let memo_tab = Hashtbl.create 17 in
        let xout = { id = dest ; ty = ty_out ; mty = expr.mty } in
        let n' = Name.fresh () in
        let xins = Name.Map.add n' xin xins in
        let dest_out = Target.fresh_out_id () in
        let conversion_out = 
          seek memo_tab logger env xins xout (EPos (n', Empty))
        in
        ([], Let (NewOut (dest_out, Mem.Shape.size_of bare_shape_out),
                  refine dest_out Mem.Shape.(SUnk (size_of bare_shape_out)) bare_shape_out))
        @-@ ([], Prim (prim, dest_out, src_args, Success))
        @-@ ([], Freeze (dest_out, src_out, Success))
        @-@ conversion_out
    in
    code_args
    (* @-@ ([], Target.comment Success "prim %a output" Target.pp_prim prim) *)
    @-@ prim_call

