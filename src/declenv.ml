type decl =
  | Expr of {
      source : Syntax.expr;
      target : Target.program;
      as_arg : Compile_adt.in_args;
    }
  | Fun of {
      source : Syntax.expr;
      args : Compile_adt.in_args Name.Map.t;
      res : Compile_adt.out_args;
      target : Target.program;
    }      

include Name.Map

type t = decl Name.Map.t
