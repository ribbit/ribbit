(** Provenances, basically trees in which leaves are tags and nodes carry constrs *)

include Base_types.Prov

let rec depth = function
  | Top | Int _ -> 0
  | Tup l -> List.fold_left (fun acc x -> max (depth x) acc) 0 l
  | Rec l -> List.fold_left (fun acc (_, x) -> max (depth x) acc) 0 l
  | Cons (_, arg) -> 1 + depth arg

let rec normalise = function
  | Top -> Top
  | Int i -> Int i
  | Tup [] | Rec [] -> Top
  | Tup [x] -> normalise x
  | Tup l -> Tup (List.map normalise l)
  | Rec l ->
    let l = List.sort (fun (n, _) (m, _) -> String.compare n m) l in
    Rec (List.map (fun (n, x) -> n, normalise x) l)
  | Cons (k, x) -> Cons (k, normalise x)

let rec intersect x1 x2 = match normalise x1, normalise x2 with
  | Top, x | x, Top -> Some x
  | Int i, Int i' when i = i' -> Some (Int i)
  | Tup l, Tup l' when (List.length l) = (List.length l') ->
    let l = List.map2 intersect l l' in
    if List.exists ((=)None) l then None else Some (Tup (List.map Option.get l))
  | Rec l, Rec l' when (List.length l) = (List.length l') ->
    let l = List.map2
      (fun (n, x) (m, y) -> n, if n = m then intersect x y else None)
      l l'
    in
    if List.exists (fun (_, x) -> x = None) l then None
    else Some (Rec (List.map (fun (n, x) -> n, Option.get x) l))
  | Cons (k, x), Cons (k', x') when k = k' ->
    Option.map (fun x -> Cons (k, x)) (intersect x x')
  | _ -> None

let intersect_list = List.fold_left
  (fun acc x -> match acc with
    | Some x' -> intersect x x'
    | None -> None)
  (Some Top)

let compat x1 x2 = intersect x1 x2 <> None

let compat_path pa x =
  let rec aux l x = match l, x with
    | [], _ | _, Top -> true
    | (Hole.HAccess i) :: l, Tup xs ->
      List.length xs > i && aux l (List.nth xs i)
    | (HField n) :: l, Rec fields ->
      List.mem_assoc n fields && aux l (List.assoc n fields)
    | (HConstr k) :: l, Cons (k', x) -> k = k' && aux l x
    | _ -> false
  in aux (Hole.to_list pa) x

let rec is_prefix_of x1 x2 = match normalise x1, normalise x2 with
  | Top, _ -> true
  | Tup l1, Tup l2 ->
    (List.length l1) = (List.length l2) && List.for_all2 is_prefix_of l1 l2
  | Rec l1, Rec l2 ->
    (List.length l1) = (List.length l2) &&
    List.for_all2 (fun (n, x) (m, y) -> n = m && is_prefix_of x y) l1 l2
  | Cons (k, x), Cons (k', x') ->
    k = k' && is_prefix_of x x'
  | _ -> false

let rec focus (pa0: Hole.t) x = match pa0 with
  | Empty -> x
  | Dot (pa, op) -> match op, focus pa x with
    | _, Top -> Top
    | HAccess i, Tup l -> List.nth l i
    | HField n, Rec l -> List.assoc n l
    | HConstr k, Cons (k', x) when k = k' -> x
    | HConstr k, Cons (k', x) -> 
      Report.fail "Prov.focus %a incompatible with %a.@ Constructor %s is not compatible with %s."
        Hole.pp pa pp x k k'
    | _, prov' ->
      Report.fail "Prov.focus %a incompatible with %a.@ focus(%a,%a) = %a is incompatible with %a"
        Hole.pp pa0 pp x
        Hole.pp pa pp x pp prov' Hole.pp_op op

let prefix_src_pa env (ty0: Types.ty) (pa0: Hole.t) prov =
  let rec aux env (ty0: Types.ty) (pa : Hole.l)  = match pa with
    | [] | [HBits _] -> prov
    | HBits _ :: _ -> failwith "shouldn't have ops after bitrange"
    | HConstr k :: pa ->
      let ty = Types.focus Hole.(Empty $. HConstr k) env ty0 in
      Cons (k, aux env ty pa )
    | HAccess i :: pa ->
      begin match Types.unroll env ty0 with
        | Product tys when i < List.length tys ->
          let l =
            List.mapi
              (fun j ty -> if j = i then aux env ty pa  else Top) tys
          in
          Tup l
        | _ -> 
          Report.fail "Incompatible path %a and type %a = %a@."
            Hole.pp pa0 Types.pp ty0 Types.pp (Types.unroll env ty0)
      end
    | HField n :: pa ->
      begin match Types.unroll env ty0 with
        | Record fields when List.mem_assoc n fields ->
          let fields =
            List.map
              (fun (m, ty) -> m, if m = n then aux env ty pa  else Top)
              fields
          in
          Rec fields
        | _ -> 
          Report.fail "Incompatible path %a and type %a = %a@."
            Hole.pp pa0 Types.pp ty0 Types.pp (Types.unroll env ty0)
      end
  in
  aux env ty0 (Hole.to_list pa0)

let of_path env ty p = prefix_src_pa env ty p Top

let rec of_src_val : Value.t -> t = function
  | VInt z -> Top
  | VTuple l -> Tup (List.map of_src_val l)
  | VRecord l -> Rec (List.map (fun (n, x) -> n, of_src_val x) l)
  | VConstr (constr, arg) -> Cons (constr, of_src_val arg)

let pp_prov = pp

(* refine prov_in such that it is equivalent to prov_out w.r.t. variable provs, with posmap mapping pos_outs to pos_ins *)
let remap_in root prov_in prov_out posmap =
  let posmap = Hole.Map.bindings posmap in
  let rec aux p_in = function
    | Top -> begin
        let provs_out =
          List.filter_map (fun (p_out, (root', p)) ->
              if root = root' && Hole.is_prefix p p_in then
                Some (focus (Hole.focus p p_in) @@ focus p_out prov_out)
              else None)
            posmap
        in
        match provs_out with
        | [] -> Top
        | prov_out::rest ->
          assert (List.for_all ((=) prov_out) rest);
          prov_out
      end
    | Tup l ->
      Tup (List.mapi (fun i p -> aux Hole.(p_in $. HAccess i) p) l)
    | Rec l ->
      Rec (List.map (fun (x, p) -> x, aux Hole.(p_in $. HField x) p) l)
    | Cons (k, p) ->
      Cons (k, aux Hole.(p_in $. HConstr k) p)
    | Int i ->
      Int i
  in
  aux Hole.Empty prov_in

(* refine prov_out such that it is equivalent to prov_in w.r.t. variable provs, with posmap mapping pos_outs to pos_ins *)
let remap_out prov_ins prov_out posmap =
  let posmap = Hole.Map.bindings posmap in
  let rec aux p_out = function
    | Top -> begin
        let provs_ins = List.filter_map (fun (p, (root, p_in)) ->
            if Hole.is_prefix p p_out then
              let prov_in = match Name.Map.find_opt root prov_ins with
                | Some p -> p
                | None -> Report.fail "remap_out: root %s not found" root
              in
              Some (focus (Hole.focus p p_out) @@ focus p_in prov_in)
            else None)
          posmap
        in
        match provs_ins with
        | [] -> Top
        | [prov_in] -> prov_in
        | _ -> assert false
      end
    | Tup l ->
      Tup (List.mapi (fun i p -> aux Hole.(p_out $. HAccess i) p) l)
    | Rec l ->
      Rec (List.map (fun (x, p) -> x, aux Hole.(p_out $. HField x) p) l)
    | Cons (k, p) ->
      Cons (k, aux Hole.(p_out $. HConstr k) p)
    | Int i ->
      Int i
  in
  aux Hole.Empty prov_out

module Set = struct

  include Base_types.ProvSet

  let mem_compat x l =
    let x = normalise x in
    exists (fun y -> compat x @@ normalise y) l

  let pp = pp pp_prov
end

module Map = struct
  include CCMap.Make(Base_types.Prov)

  let filter_key f pm1 =
    fold (fun k v res ->
        match f k with
        | Some k -> add k v res
        | None -> res
      )
      pm1
      empty

  let rewrite f pm = of_list @@ List.filter_map f @@ to_list pm

  (* concat_map is left_leaning, since it's used to collect branches
     from splits *)
  let concat_map f l =
    List.fold_left (union (fun _k x1 _x2 -> Some x1)) empty
    @@ List.map f l

  let of_set pset =
    Set.fold (fun k p -> add k () p) pset empty

  (** Allow to write "for comprehension" on provenance maps
      with built-in compatibility restrictions *)
  module Compat = struct

    let merge f pm1 pm2 =
      fold (fun k1 v1 res ->
          fold (fun k2 v2 res ->
              match intersect k1 k2 with
              | Some k -> add k (f v1 v2) res
              | None -> res
            )
            pm2
            res
        )
        pm1
        empty

    let rec mergel pms =
      match pms with
      | [] -> empty
      | [pm] -> map (fun x -> [x]) pm
      | pm::pms -> merge List.cons pm @@ mergel pms
    
    let (let*) pm f = map f pm
    let (let**) pm f = concat_map f pm
    let (let*?) pm f = filter_map (fun k v -> f (k, v)) pm
        
    (* Trick for filtering 'and' operators *)
    let add_unit pm = map (fun v -> v,()) pm
    
    let (and&) pm1 pm2 = merge (fun v1 v2 -> v1,v2) pm1 pm2
    let (and&?) pm path =
      let intersect_path path p =
        if compat_path path p then Some p else None
      in
      add_unit @@ filter_key (intersect_path path) pm
    let (and&??) pm prov =
      add_unit @@ filter_key (intersect prov) pm
  end
end
