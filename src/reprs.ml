open Types
open Mem
open Syntax

exception Unsuported_type of (string * Types.ty)

module type S = sig
  val name : string
  val mem_ty : Env.t -> Types.ty -> Mem.ty
end

module Wordlike : S = struct
  let name = "word"
  open Mem

  let rec mem_ty ty_env ty =
    let open Ty in match ty with
    | Type t ->
      mem_ty ty_env (Env.find_ty t ty_env)
    | Word { width; values } ->
      Ty.sub ~sub_ty:(Ty.word width) Hole.Empty
    | Sum cases ->
      let unit_cases, non_unit_cases =
        List.partition (fun (_, ty) -> ty = Types.Product []) cases
      in
      begin if not @@ CCList.is_empty non_unit_cases then
        raise @@ Unsuported_type (name, ty);
      end;
      let size =
        int_of_float @@ ceil (log (float @@ List.length unit_cases) /. log 2.)
      in
      TSplit {
        discrs = [Empty]; 
        branches = List.mapi
            (fun j (tag, _) ->
               let z = Z.of_int j in
               let provs = Prov.Set.singleton @@ Prov.Cons (tag, Top) in
               let ty = Ty.word size |>< Path.Empty @= z in
               {lhs = [Some z]; provs; ty })
            unit_cases;
      }
    | _ ->
      raise @@ Unsuported_type (name, ty)
end


module Clike : S = struct
  let name = "c"
  open Mem

  let rec mem_ty ty_env ty =
    let open Ty in match ty with
    | Type t ->
      Ty.var t
    | Word { width; values } ->
      Ty.sub ~sub_ty:(Ty.word width) Hole.Empty
    | Record fields ->
      let fields = List.map (fun (n, sub_ty) ->
          let sub_ty = mem_ty ty_env sub_ty in
          Ty.sub ~sub_ty Hole.(Empty $. HField n))
          fields
      in
      TStruct fields
    | Product tys ->
      let fields = List.mapi
          (fun pos sub_ty ->
             let sub_ty = mem_ty ty_env sub_ty in
             Ty.sub ~sub_ty Hole.(Empty $. HAccess pos))
        tys in
      TStruct fields
    | Sum cases ->
      let unit_cases, non_unit_cases =
        List.partition (fun (_, ty) -> ty = Types.Product []) cases
      in
      if not @@ CCList.is_empty non_unit_cases then
        raise @@ Unsuported_type (name, ty);
      let size =
        int_of_float @@ ceil (log (float @@ List.length unit_cases) /. log 2.)
      in
      let mk_branch pos (constr, _ty) = 
        let z = Z.of_int pos in
        let provs = Prov.Set.singleton @@ Prov.Cons (constr, Top) in
        let ty = Ty.word size |>< Path.Empty @= z in
        {lhs = [Some z]; provs; ty }
      in
      TSplit {
        discrs = [Empty]; 
        branches = List.mapi mk_branch unit_cases;
      }
end


module Unboxed : S = struct
  let name = "unboxed"
  open Mem

  let rec mem_ty ty_env ty =
    let open Ty in match ty with
    | Type t ->
      Ty.var t
    | Word { width; values } ->
      Ty.sub ~sub_ty:(Ty.word width) Hole.Empty
    | Record fields ->
      let fields = List.map (fun (n, sub_ty) ->
          let sub_ty = mem_ty ty_env sub_ty in
          Ty.sub ~sub_ty Hole.(Empty $. HField n))
          fields
      in
      TStruct fields
    | Product tys ->
      let fields = List.mapi
          (fun pos sub_ty ->
             let sub_ty = mem_ty ty_env sub_ty in
             Ty.sub ~sub_ty Hole.(Empty $. HAccess pos))
        tys in
      TStruct fields
    | Sum cases
      when List.for_all (function (_, Product []) -> true | _ -> false) cases ->
      let size =
        int_of_float @@ ceil (log (float @@ List.length cases) /. log 2.)
      in
      let mk_branch pos (constr, _ty) = 
        let z = Z.of_int pos in
        let provs = Prov.Set.singleton @@ Prov.Cons (constr, Top) in
        let ty = Ty.word size |>< Path.Empty @= z in
        {lhs = [Some z]; provs; ty }
      in
      TSplit {
        discrs = [Empty]; 
        branches = List.mapi mk_branch cases;
      }
    | Sum cases ->
      let mk_branch pos (constr, ty) = 
        let z = Z.of_int pos in
        let provs = Prov.Set.singleton @@ Prov.Cons (constr, Top) in
        let header = Ty.word 64 |>< Empty @= z in
        let fields = match ty with
          | Types.Product tys ->
            List.mapi (fun pos sub_ty ->
                let sub_ty = mem_ty ty_env sub_ty in
                let h = Hole.(Empty $. HConstr constr $. HAccess pos) in
                Ty.sub ~sub_ty h)
              tys
          | sub_ty ->
            let sub_ty = mem_ty ty_env sub_ty in
            [Ty.sub ~sub_ty Hole.(Empty $. HConstr constr)]
        in
        { lhs = [Some z];
          provs;
          ty = TStruct (header :: fields);
        }
      in
      TSplit {
        discrs = [Path.(Empty $. Access 0)];
        branches = List.mapi mk_branch cases;
      }

end


module OCamem : S = struct
  let name = "caml"
  open Mem

  let lowest_bit = Path.(Empty $. Bits {offset = 0; width = 1})
  let shift_right w =
    assert (w <= 63);
    Path.(Empty $. Bits {offset = 1; width = w})
  let first_field = Path.(Empty $. Access 0)

  let ocaml_ptr pointee =
    Ty.ptr ~width:64 ~align:3 pointee
    |>< lowest_bit @= Z.zero

  let rec mem_ty ty_env ty =
    let open Ty in match ty with
    | Type t ->
      Ty.var t
    | Word { width; values  }->
      if width <= 63 then
        Ty.word 64
        |>< lowest_bit @= Z.one
        |>< shift_right width @: (Ty.sub ~sub_ty:(Ty.word width) Hole.Empty)
      else
        let header = Ty.word 64 |>< Empty @= Z.zero in
        ocaml_ptr @@ TStruct [header; Ty.sub ~sub_ty:(Ty.word width) Hole.Empty]
    | Record fields ->
      let header = Ty.word 64 |>< Empty @= Z.zero in
      let fields = List.map (fun (n, sub_ty) ->
          let sub_ty = mem_ty ty_env sub_ty in
          Ty.sub ~sub_ty Hole.(Empty $. HField n))
          fields
      in
      ocaml_ptr (TStruct (header :: fields))
    | Product tys ->
      let header = Ty.word 64 |>< Empty @= Z.zero in
      let fields = List.mapi
          (fun pos sub_ty ->
             let sub_ty = mem_ty ty_env sub_ty in
             Ty.sub ~sub_ty Hole.(Empty $. HAccess pos))
        tys in
      ocaml_ptr (TStruct (header :: fields))
    | Sum cases ->
      let unit_cases, non_unit_cases =
        List.partition (fun (_, ty) -> ty = Types.Product []) cases
      in
      let unit_branch =
        let split = TSplit {
          discrs = [Empty]; 
          branches = List.mapi
            (fun j (tag, _) ->
               let z = Z.of_int j in
               let provs = Prov.Set.singleton @@ Prov.Cons (tag, Top) in
               {lhs = [Some z]; provs; ty = TConst z})
            unit_cases;
        } in
        Ty.word 64
        |>< lowest_bit @= Z.one
        |>< shift_right 63 @: split
      and unit_pklass =
        Prov.Set.of_list @@ List.map (fun (k, _) -> Prov.Cons (k, Top)) unit_cases
      and non_unit_branch =
        let pointee =
          TSplit {
            discrs = [first_field];
            branches =
              List.mapi
                (fun pos (constr, ty) ->
                   let z = Z.of_int pos in
                   let provs = Prov.Set.singleton @@ Prov.Cons (constr, Top) in
                   let header = Ty.word 64 |>< Empty @= z in
                   let fields = match ty with
                     | Types.Product tys ->
                       List.mapi (fun pos sub_ty ->
                           let sub_ty = mem_ty ty_env sub_ty in
                           let h = Hole.(Empty $. HConstr constr $. HAccess pos) in
                           Ty.sub ~sub_ty h)
                         tys
                     | sub_ty ->
                       let sub_ty = mem_ty ty_env sub_ty in
                       [Ty.sub ~sub_ty Hole.(Empty $. HConstr constr)]
                   in
                   { lhs = [Some z];
                     provs;
                     ty = TStruct (header :: fields);
                   }
                )
                non_unit_cases;
          }
        in
        Ty.ptr ~width:64 ~align:3 pointee
        |>< lowest_bit @= Z.zero
      and non_unit_pklass =
        Prov.Set.of_list @@ List.map (fun (k, _) -> Prov.Cons (k, Top)) non_unit_cases
      in
      begin match unit_cases, non_unit_cases with
        | _, [] -> unit_branch
        | [], _ -> non_unit_branch
        | _ ->
          TSplit {
            discrs = [lowest_bit];
            branches = [
              { lhs = [Some Z.one]; provs = unit_pklass; ty = unit_branch};
              { lhs = [Some Z.zero]; provs = non_unit_pklass; ty = non_unit_branch}
            ]
          }
      end

end

module Sprotch : S = struct
  let name = "sprotch"
  open Mem

  let lowest_bit = Path.(Empty $. Bits {offset = 0; width = 1})
  let shift_right = Path.(Empty $. Bits {offset = 1; width = 63})
  let first_field = Path.(Empty $. Access 0)

  (* output type: same as Types.ty but sum variants are lists of constructors *)
  let inline_variants ty_env ty =
    let max_variant_count = 5 in
    let rec aux pos env : Types.ty -> _ = function
      | Type x ->
        begin match Name.Map.find_opt x env with
        | Some ty -> ty
        | None ->
          let env = Name.Map.add x (`Type (x, pos)) env in
          aux pos env @@ Env.find_ty x ty_env
        end
      | Word wspec ->
        `Word wspec
      | Product tys ->
        `Product (List.mapi (fun i ty -> aux Hole.(pos $. HAccess i) env ty) tys)
      | Sum l ->
        let do_one_variant count (k, ty) =
          let ty = aux Hole.(pos $. HConstr k) env ty in
          let count, inlined_vars = match ty with
            | `Sum l ->
              count + List.length l,
              List.map (fun (constrs, ty) -> k :: constrs, ty) l
            | _ ->
              count + 1,
              [[k], ty]
          in
          let non_inlined_var = [k], ty in
          count, (non_inlined_var, inlined_vars)
        in
        let count, l = List.fold_left_map do_one_variant 0 l in
        let l = if count > max_variant_count
          then List.map fst l
          else List.concat_map snd l
        in
        `Sum l
      | ty ->
        raise @@ Types.Unhandled_polymorphic_type ty
    in
    aux Hole.Empty Name.Map.empty ty

  (* input: whatever inline_variants gave out *)
  (* output: products and sum variants contain lists of source path * ty pairs *)
  let inline_tuples ty_env ty =
    let rec aux pos env = function
      | `Type (x, _) ->
        begin match Name.Map.find_opt x env with
        | Some ty -> ty
        | None ->
          let env = Name.Map.add x (`Type (x, pos)) env in
          aux pos env @@ Name.Map.find x ty_env
        end
      | `Word wspec ->
        `Word wspec
      | `Product tys ->
        let ty_tups = List.mapi
          (fun i ty ->
            let head_path = Hole.(pos $. HAccess i) in
            match aux head_path env ty with
              | `FlatProduct tys -> tys
              | ty -> [head_path, ty])
          tys in
        `FlatProduct (List.concat ty_tups)
      | `Sum l ->
        let do_one_variant (constrs, ty) =
          let prov = List.fold_left
            (fun prov k -> Prov.Cons (k, prov))
            Prov.Top (List.rev constrs) in
          let head_path = List.fold_left
            (fun h k -> Hole.(h $. HConstr k))
            pos (List.rev constrs) in
          let tys = match aux head_path env ty with
            | `FlatProduct tys -> tys
            | ty -> [head_path, ty]
          in
          (prov, tys)
        in
        `FlatSum (List.map do_one_variant l)
    in
    aux Hole.Empty Name.Map.empty ty

  let rec mem_ty ty_env ty0 =
    let env = Env.map_tys (fun _ ty _ -> inline_variants ty_env ty) ty_env in
    let ty = inline_tuples env @@ inline_variants ty_env ty0 in
    let mty = match ty with
    | `Type (x, pos) ->
      Ty.sub ~sub_ty:(Ty.var x) pos
    | `Word _ ->
      Ty.word 64 |>< lowest_bit @= Z.one |>< shift_right @: Ty.word 63
    | `FlatProduct tys ->
      let header = Ty.word 64 |>< Empty @= Z.zero in
      let fields = List.map
          (fun (h, ty) ->
             let sub_ty = Types.focus h ty_env ty0 in
             let sub_ty = mem_ty ty_env sub_ty in
             Ty.sub ~sub_ty h)
        tys in
      Ty.ptr ~width:64 ~align:3 (TStruct (header :: fields)) |>< lowest_bit @= Z.zero
    | `FlatSum l ->
      let unit_provs, non_unit_provs = List.partition (fun (_, l) -> l = []) l in
      let unit_branch =
        let split = Ty.TSplit {
          discrs = [Empty]; 
          branches = List.mapi
            (fun j (prov, _) ->
               let z = Z.of_int j in
               let provs = Prov.Set.singleton prov in
               Ty.{ lhs = [Some z]; provs; ty = TConst z })
            unit_provs;
        } in
        Ty.word 64 |>< lowest_bit @= Z.one |>< shift_right @: split
      and unit_prov_set = Prov.Set.of_list @@ List.map fst unit_provs in
      let non_unit_branch =
        let pointee = Ty.TSplit {
          discrs = [first_field];
          branches = List.mapi
            (fun pos (prov, tys) ->
               let z = Z.of_int pos in
               let provs = Prov.Set.singleton prov in
               let header = Ty.word 64 |>< Empty @= z in
               let fields = List.map
                   (fun (h, ty) ->
                      let sub_ty = Types.focus h ty_env ty0 in
                      let sub_ty = mem_ty ty_env sub_ty in
                      Ty.sub ~sub_ty h)
                 tys in
               Ty.{ lhs = [Some z]; provs; ty = TStruct (header :: fields) })
            non_unit_provs;
        } in
        Ty.ptr ~width:64 ~align:3 pointee |>< lowest_bit @= Z.zero
      and non_unit_prov_set = Prov.Set.of_list @@ List.map fst non_unit_provs in
      begin match unit_provs, non_unit_provs with
      | _, [] -> unit_branch
      | [], _ -> non_unit_branch
      | _ ->
        TSplit {
          discrs = [lowest_bit];
          branches = [
            { lhs = [Some Z.one]; provs = unit_prov_set; ty = unit_branch };
            { lhs = [Some Z.zero]; provs = non_unit_prov_set; ty = non_unit_branch }];
        }
      end
    in
    (*Peahell.Report.eprintf "Mem ty of %a is %a@." Types.pp_ty ty Ty.pp mty;*)
    mty

  (* Expand tuples within tuples *)
  let rec flatten_fields ty_env ty =
    match ty with
    | Types.Product tys ->
      List.concat @@ List.mapi
        (fun pos ty -> List.map
          (fun (hole, ty) -> Hole.(Empty $. HAccess pos), ty) @@
          flatten_fields ty_env ty)
        tys
    | ty -> [Hole.Empty, ty]

  (* Collapse a type's variant hierarchy into a list of
   * arbitrarily deep provenances. Each prov is an individual combination of
   * nested variants from the original type (and its subtypes).
   * Each prov is associated with a list of subtypes, each of which
   * also carries its position in the original type. *)
  let rec flatten_variants ty_env = failwith "TODO"

  and distrib_variants ty_env ty = failwith "TODO"

  let expand_nonrec_vars ty_env =
    let rec aux seen_vars : Types.ty -> _ * Types.ty = function
      | Types.Type x as ty0 ->
        if Name.Set.mem x seen_vars then seen_vars, ty0
        else
          let seen, ty = aux (Name.Set.add x seen_vars) @@ Name.Map.find x ty_env in
          seen, if Name.Set.mem x seen then ty0 else ty
      | Types.Word wspec ->
        seen_vars, Types.Word wspec
      | Types.Product tys ->
        let seen_sets, tys = List.split @@ List.map (aux seen_vars) tys in
        List.fold_left Name.Set.union seen_vars seen_sets, Types.Product tys
      | Types.Sum l ->
        let seen, l = List.fold_left
          (fun (seen, l) (k, ty) ->
            let seen', ty = aux seen ty in
            Name.Set.union seen seen', l @ [k, ty])
          (seen_vars, []) l in
        seen, Types.Sum l
      | ty -> raise @@ Types.Unhandled_polymorphic_type ty
    in fun ty -> let _, ty = aux Name.Set.empty ty in ty

end

type repr = (module S)
let available_reprs : repr list = [
  (module OCamem : S);
  (module Sprotch : S);
  (module Clike : S);
  (module Wordlike : S);
  (module Unboxed : S);
]

let default_repr = ref (module Unboxed : S)

let rec get_repr_by_name s = function
  | [] -> Report.fail "Unknown repr %s" s
  | (module M : S) as t :: rest ->
    if M.name = s then t else get_repr_by_name s rest
let get_repr_by_name s = get_repr_by_name s available_reprs

let default_mem_ty env ty =
  let (module M : S) = !default_repr in 
  M.mem_ty env ty

let compute_mty_decl repr_name env_ty body_ty =
  match repr_name with
  | None -> default_mem_ty env_ty body_ty
  | Some s ->
    let (module M : S) = get_repr_by_name s in
    M.mem_ty env_ty body_ty

let find_mty ?(repr = !default_repr) ty_env ty = match ty with
  | Types.Type constructor ->
    Ty.var constructor
  | _ ->
    let (module M : S) = repr in
    M.mem_ty ty_env ty
      
