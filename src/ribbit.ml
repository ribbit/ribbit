open Globals

let name = "ribbit"
type input = Syntax.program
let options = List.concat_map
    (fun (keys, spec, doc) -> List.map (fun key -> key, spec, doc) keys)
    [
      (["-o"; "-p"; "--path"], Arg.Set_string File.outfile_path,
       "Set the location of output files (e.g., dot). Default to '.'");
      (["-g"; "--graph"], Arg.Set dot_output, "Output graphs");
      (["-s"; "--show"], Arg.Set show_output, "Show graphs");
      (["-i"; "--ir";], Arg.Set ir_output, "Output text IR");
      (["-M"; "--metrics"], Arg.Set metrics, "Compute static performance metrics");
      (["-H"; "--heuristics"], Arg.Bool ((:=) heuristics), "Use heuristics. Default to true");
      (["-r"; "--repr"],
       Arg.Symbol (
         List.map (fun (module M:Reprs.S) -> M.name) Reprs.available_reprs,
         (fun s -> Reprs.default_repr :=  Reprs.get_repr_by_name s)),
       "Select the default memory layout");
    ]

type environment = {
  typs : Env.t;
  decls : Declenv.t ;
  values : Value.t Name.Map.t;
  mvalues : (Target.in_id * Mem.value) Name.Map.t;
}
let initial_environment = {
  typs = Env.empty;
  decls = Name.Map.empty;
  values = Name.Map.empty;
  mvalues = Name.Map.empty;
}

let read_more str =
  let i = ref (String.length str - 1) in
  while !i >= 0 && List.mem str.[!i] [' '; '\n'; '\t'; '\r'] do decr i done ;
  !i < 1 || (str.[!i] <> ';' || str.[!i - 1] <> ';')

let file_parser =
  let f _name lexbuf =
    Peahell.Input.wrap (Parser.program Lexer.token) lexbuf
  in
  Some f
let toplevel_parser =
  let f lexbuf =
    Peahell.Input.wrap (Parser.toplevel_phrase Lexer.token) lexbuf
  in
  Some f
let expect_parser =
  let f _name lexbuf =
    Peahell.Input.wrap (Parser.expect_file Lexer.token) lexbuf
  in
  Some ( "(*EXPECT", "EXPECT*)", f)

let () =
  File.file_emitter := (fun ~filename ~title ~mime pf ->
      File.native_file_emitter ~filename ~title ~mime pf;
      if !show_output then
        if mime = Some "dot" then
          ignore @@ Unix.system @@ Fmt.str "xdot %s" @@ Filename.quote filename
    )

let exec_type_decls (decls : Syntax.typedecl list) env =
  let new_typs = Typing.tydecls env.typs decls in
  Report.infof "Type declarations" "%a@," Env.pp new_typs;
  { env with typs = Env.union env.typs new_typs }

let exec_func_decl (fundecl : Syntax.fundecl) env =
  let {Syntax. name; _ } = fundecl in
  Report.infof "Function declaration"
    "let %s(%a)@ = %a"
    name
    (Name.Map.pp Fmt.nop) fundecl.arg_tys
    Syntax.pp_expr fundecl.body;
  let arg_tys, body = Typing.fundecl env.typs fundecl in
  Report.infof "Function elaboration"
    "@[<hv 2>let @[%s(%a)@] : @[%a@] =@ @[%a@]@]"
    name
    (Name.Map.pp Types.pp) arg_tys
    Types.pp body.ty
    TypedExpr.pp body;
  let args = Name.Map.map (fun ty ->
      let id = Target.fresh_in_id () in
      let mty = Reprs.find_mty env.typs ty in
      Compile_adt.{ id; ty; mty; prov = Prov.Top })
    arg_tys
  in
  let res =
    let id = Target.OId Name.unique_result in
    let ty = body.ty in
    let mty = body.mty in
    Compile_adt.{ id; ty; mty }
  in
  let arg_typs = Name.Map.map (fun (a:Compile_adt.in_args) -> a.ty) args in
  let typs = Env.add_var name (Fun (arg_typs, res.ty)) env.typs in
  let target =
    let typs' = Name.Map.fold (fun x ty typs ->
        Env.add_var x (Val ty) typs)
        arg_typs
        typs
    in
    let logger = new File.logger ~mime:"dot" MemoryTree.pp_dot in
    Compile_adt.expr_to_mem logger typs' args res.id body
  in
  if !dot_output then begin
    let title = "Compiled " ^ name in
    let iids = Name.Map.map (fun (a:Compile_adt.in_args) -> a.id) args in
    File.emitf
      ~mime:"dot" ~title ~filename:("target_"^name^"_noopt.dot")
      "%a" Target.Dot.pp_fun (title, FId name, iids, res.id, target)
  end;
  let target = Target_optims.go target in
  if !dot_output then begin
    let title = "Optimized " ^ name in
    let iids = Name.Map.map (fun (a:Compile_adt.in_args) -> a.id) args in
    File.emitf
      ~mime:"dot" ~title ~filename:("target_"^name^".dot")
      "%a" Target.Dot.pp_fun (title, FId name, iids, res.id, target)
  end;
  if !ir_output then begin
    let OId dest = res.id in
    Report.debugf "%s(%a) -> %a =@ @[<2>%a@]@," name
      (Name.Map.pp @@ fun fmt (x:Compile_adt.in_args) -> Target.pp_iid fmt x.id) args
      Name.pp dest
      Target.pp_program target
  end;
  let env =
    let decls =
      Declenv.add name
        (Declenv.Fun { args; source = fundecl.body; res; target })
        env.decls
    in
    { env with typs; decls }
  in
  env

let exec_value_decl (name, ty, e : Name.t * Syntax.type_expr * Syntax.expr) env =
  Report.infof "Value Declaration" "Compiling %a" Syntax.pp_expr e;
  let te = Typing.valdecl env.typs e ty in
  Report.infof "Value Declaration" "Elaborated to %a" TypedExpr.pp te;
  (* Define each attribute of this new value *)
  let iid = Target.fresh_in_id ()
  and oid = Target.fresh_out_id () in
  let sh = Mem.Shape.of_ty env.typs te.mty in
  (* Emit its compiled constructor and source/dest location initializers *)
  let logger = new File.logger ~mime:"dot" MemoryTree.pp_dot in
  let xins =
    Declenv.filter_map (fun _ -> function Declenv.Expr v -> Some v.as_arg | _ -> None)
      env.decls
  in
  let allocate = Target.Let (NewOut (oid, Mem.Shape.size_of sh),
    Compile_adt.refine oid Mem.Shape.(SUnk (size_of sh)) sh)
  and freeze = Target.Freeze (oid, iid, Success)
  and target =
    Compile_adt.expr_to_mem logger env.typs xins oid te
  in
  let target = Target_optims.go target in
  let funs, code = target in
  let code = Target.concat allocate @@ Target.concat code freeze in
  if !ir_output then begin
    Report.debugf "%s =@ @[<2>%a@]@," name      
      Target.pp_program (funs, code)
  end;
  if !dot_output then begin
    let title = "Compiled " ^ name in
    File.emitf
      ~mime:"dot" ~title ~filename:("target_"^name^".dot")
      "%a" Target.Dot.pp_decl (title, (funs, code))
  end;
  
  (* Eval source constructor *)
  let src_res =
    CCResult.guard @@ fun () ->
    Eval.expr env.decls env.values e
  in
  (* Eval compiled constructor *)
  let target_vals = Name.Map.fold (fun _ (sid, mval) acc ->
      (sid, mval) :: acc)
      env.mvalues
      []
  in
  let target_funs = Declenv.fold (fun f decl acc ->
      match decl with
      | Declenv.Expr _ -> acc
      | Fun { args; res; source = _; target } ->
        let funs, code = target in
        let iids = Name.Map.map
          (fun Compile_adt.{ id; ty; mty; prov } -> id)
          args
        in
        (Target.FId f, iids, res.Compile_adt.id, code) :: funs @ acc
    )
      env.decls
      []
  in
  let target_res = Target.eval_value (target_funs @ funs) target_vals iid code in
  (* Compare source and target evaluation results *)
  Report.infof "Value Declaration"
    "@[%s = %a = %a@]@;<1 -2>represented as@,@[<2>@ %a@]" name
    Syntax.pp_expr e
    Fmt.(result ~error:(any "fail") ~ok:Value.pp) src_res
    Fmt.(option ~none:(any "fail") Mem.pp_value) target_res;
  let typs = Env.add_var name (Val te.ty) env.typs in
  match src_res, target_res with
  | Error _, None -> { env with typs }
  | Ok _, None
  | Error _, Some _ ->
    Report.fail "Evaluation results don't coincide"
  | Ok v, Some mv ->
    let funs', code' =
      let te = Typing.expr env.typs (Syntax.expr_of_val v) (Some te.ty) in
      Compile_adt.expr_to_mem
        logger env.typs xins oid
        te
    in
    let code' = Target.concat allocate @@ Target.concat code' freeze in
    let expected_mv = Option.get @@
      Target.eval_value (target_funs @ funs') target_vals iid code'
    in
    if not (Mem.V.equiv mv expected_mv) then
      Report.fail "Evaluation results don't coincide.@ @[<v2>Expected memory value was@ %a@]" Mem.pp_value expected_mv
    else
      (* Success! Add this new value to our environment *)
      if !dot_output then begin
        let title = "Value " ^ name in
        File.emitf
          ~mime:"dot" ~title ~filename:("mval_"^name^".dot")
          "%a" Mem.V.pp_dot (title, mv)
      end;
    let target = funs, code in
    let as_arg = Compile_adt.{ id = iid; ty = te.ty; mty = te.mty; prov = Prov.of_src_val v } in
    let decls =
      Declenv.add name (Declenv.Expr { source = e; target; as_arg })
        env.decls
    in
    let values = Name.Map.add name v env.values in
    let mvalues = Name.Map.add name (iid, mv) env.mvalues in
    { typs; decls; values; mvalues }

let prepare_bench f ins env =
  (* if !llvm_output then
    let fdecl = Name.Map.find f env.funs in
    let llf = Option.get fdecl.llvm in
    let llvs = List.map (fun x -> Option.get (Name.Map.find x env.vals).llvm) ins in
    Some {mty_in = fdecl.mty_in; llf; llvs}
  else *) None

let exec _import env0 statements =

  let builtins env =
    let env = exec_type_decls [Builtins.Bool.tydecl] env in
    env
  in
  let type_decls = List.filter_map
    (function Syntax.TypeDecl decl -> Some decl | _ -> None)
    statements
  in 
  let rec exec_decls (commands : Syntax.command list) env = match commands with
    | FunDecl fdecl :: rest ->
      let env = exec_func_decl fdecl env in
      exec_decls rest env
    | ValDecl vdecl :: rest ->
      let env = exec_value_decl vdecl env in
      exec_decls rest env
    (* handled separately *)
    | (TypeDecl _ | Bench (_, _)) :: rest ->
      exec_decls rest env
    | [] ->
      env
  in
  let env =
    env0
    |> builtins
    |> exec_type_decls type_decls
    |> exec_decls statements
  in

  if !metrics then begin
    File.emitf ~mime:"csv" ~title:"Value metrics" ~filename:"value_metrics.csv"
      "name,%a@.%a"
      Fmt.(list ~sep:(any ",") @@ pair ~sep:nop string nop)
      Mem.V.Metrics.all_metrics
      (Fmt.iter_bindings Name.Map.iter
         (fun fmt (x, row) ->
            let row = List.map (fun (_, f) -> f x) Mem.V.Metrics.all_metrics in
            Fmt.pf fmt "%s,%a@." x Fmt.(list ~sep:(any ",") int) row))
      env.decls;
  end;

  env
