type t = string

let compare = String.compare
let equal = String.equal

let pp = Fmt.string
let ppk = pp

module Set = CCSet.Make(String)
module Map = struct
  include CCMap.Make(String)
  let pp ?(sep=(Fmt.any ",@ ")) ?(bind=(Fmt.any ":@ ")) ppv =
    Fmt.iter_bindings ~sep iter
      (fun fmt (k,v) -> Fmt.pf fmt "@[<2>%a%a@[%a@]@]" ppk k bind () ppv v)
end

let unique_discr = "Δ"
let unique_result = "∇"

let indices = [|"₀";"₁";"₂";"₃";"₄";"₅";"₆";"₇";"₈";"₉"|]
let rec encode_id i =
  if i < 10 then indices.(i)
  else
    let d = i mod 10 and i' = i / 10 in
    encode_id i' ^ indices.(d)

let ids : (t, int) Hashtbl.t = Hashtbl.create 17
let fresh_with_prefix : string -> unit -> t = fun prefix _ ->
  let s =
    match Hashtbl.find_opt ids prefix with
    | None -> prefix
    | Some id -> prefix ^ (encode_id id)
  in
  CCHashtbl.incr ids prefix;
  s
let fresh : unit -> t = fresh_with_prefix "x"
