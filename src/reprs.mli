(** Generic Memory Representations *)

(** A generic memory representation is a function from types to memory types *)
module type S = sig
  val name : string
  val mem_ty : Env.t -> Types.ty -> Mem.ty
end

(** The list of available generic representations *)
val available_reprs : (module S) list

val get_repr_by_name : string -> (module S)

(** The current default representation *)
val default_repr : (module S) ref

(** [compute_mty_decl repr_name env ty] compute the memory type corresponding
    to the type definition [ty], using the representation [repr_name] if given,
    or the default one.
*)
val compute_mty_decl : string option -> Env.t -> Types.ty -> Mem.Ty.t

(** [find_mty env ty] finds the already computed memory type associated
    to [ty] if it exists, or computes a new one using the repr [repr]. *)
val find_mty : ?repr:(module S) -> Env.t -> Types.ty -> Mem.Ty.t


(** Existing representations *)

module OCamem : S
module Sprotch : S
module Clike : S
module Wordlike : S
module Unboxed : S
