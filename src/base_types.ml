module Types = struct 
  type ty =
    | Type of Name.t
    | Word of word_spec
    | Product of ty list
    | Record of (Name.t * ty) list
    | Sum of sum_spec
  and word_spec = {width: int; values: unit}
  and sum_spec = (Name.t * ty) list

  (** Printers **)
  let rec pp fmt = function
    | Type s ->
      Fmt.pf fmt "%a" Name.pp s
    | Word { width; values } ->
      Fmt.pf fmt "i%i" width
    | Record [] ->
      Fmt.pf fmt "{}"
    | Record l ->
      Fmt.pf fmt "@[<hv 2>{ %a@]@ }"
        Fmt.(list ~sep:(any ",@ ") @@ pair ~sep:(any ": ") Name.pp pp)
        l
    | Product [] ->
      Fmt.pf fmt "()"
    | Product l -> 
      Fmt.pf fmt "@[<hv 2>(%a@]@,)"
        Fmt.(list ~sep:(any ",@ ") pp)
        l
    | Sum l ->
      Fmt.pf fmt "enum {@[<hov>%a@]}" Fmt.(list ~sep:comma pp_constructor) l
  and pp_constructor fmt = function
    | n, Product [] -> Fmt.pf fmt "%a" Name.pp n
    | n, Product tys -> Fmt.pf fmt "%a(%a)" Name.pp n Fmt.(list ~sep:comma pp) tys
    | n, ty -> Fmt.pf fmt "%a(%a)" Name.pp n pp ty
  and pp_with_parens fmt t =
    match t with
    | Word _ | Type (_) | Sum ([]|[_]) | Product ([]|[_]) | Record [] ->
      pp fmt t
    | Sum _ | Product _ | Record _ ->
      Fmt.parens pp fmt t

  let pp_decl fmt (name, ty) = match ty with
    | Record _ | Product _ ->
      Fmt.pf fmt "struct %s %a" name pp ty
    | Sum l ->
      Fmt.pf fmt "enum %s {@[<hov>%a@]}"
        name
        Fmt.(list ~sep:comma pp_constructor) l
    | _ ->
      Fmt.pf fmt "type %s = %a"
        name pp ty
end
  
type bitrange = {offset: int; width: int}
let pp_bitrange fmt {offset; width} =
  Fmt.pf fmt "[%i:+%i]" offset width

module Path = struct
  type op =
    | Deref
    | Access of int
    | Bits of bitrange
    | Mask of bitrange (* TODO is this sufficient? *)
  type t =
    | Empty
    | Dot of t * op
  type l = op list

  let equal : t -> t -> bool = Stdlib.(=)
  let compare : t -> t -> int = Stdlib.compare

  let pp_op fmt = function
    | Deref -> Fmt.pf fmt "*"
    | Access i -> Fmt.int fmt i
    | Bits b -> pp_bitrange fmt b
    | Mask b -> Fmt.pf fmt "!%a" pp_bitrange b

  let rec pp_suffix fmt = function
    | Empty -> ()
    | Dot (t, op) ->
      Fmt.pf fmt "%a.%a" pp_suffix t pp_op op
  
  let pp fmt path = Fmt.pf fmt "_%a" pp_suffix path
end
module PathMap = CCMap.Make(Path)

module Prov = struct
  type t =
    | Top
    | Tup of t list
    | Rec of (Name.t * t) list
    | Cons of Name.t * t
    | Int of Z.t

  let equal : t -> t -> bool = Stdlib.(=)
  let compare : t -> t -> int = Stdlib.compare
                                  
  let rec pp fmt = function
    | Top -> Fmt.pf fmt "_"
    | Int i -> Z.pp_print fmt i
    | Rec [] ->
      Fmt.pf fmt "{}"
    | Rec l ->
      Fmt.pf fmt "@[<hv 2>{ %a@]@ }"
        Fmt.(list ~sep:(any ",@ ") @@ pair ~sep:(any ": ") Name.pp pp)
        l
    | Tup [] ->
      Fmt.pf fmt "()"
    | Tup l -> 
      Fmt.pf fmt "@[<hv 2>(%a@]@,)"
        Fmt.(list ~sep:(any ",@ ") pp)
        l
    | Cons (k, Tup []) -> Fmt.pf fmt "@[%s@]" k
    | Cons (k, Tup l) ->
      Fmt.pf fmt "@[%s(%a)@]" k Fmt.(list ~sep:(any ",@ ") pp) l
    | Cons (k, arg) -> Fmt.pf fmt "@[%s(%a)@]" k pp arg
end
module ProvSet = CCSet.Make(Prov)

module MemTy = struct
  type t =
    | TVar of Name.t * specs
    | TConst of Z.t
    | TWord of {width: int; specs: specs}
    | TPtr of {width: int; align: int; pointee: t; specs: specs}
    | TStruct of t list
    | TFrag of frag
    | TSplit of {discrs: Path.t list; branches: branch list}
  and frag = {src_pos: Hole.t; mem_ty: t; specs: specs}
  and branch = {lhs: Z.t option list; provs: ProvSet.t; ty: t}
  and specs = t PathMap.t

  let rec pp fmt = function
    | TVar (n, specs) ->
      Fmt.pf fmt "%a%a" Name.pp n pp_add_specs specs
    | TConst v ->
      Fmt.pf fmt "(= %a)" Z.pp_print v
    | TWord {width; specs} ->
      Fmt.pf fmt "@[<2>w%d%a@]" width pp_add_specs specs
    | TPtr {width; align; pointee; specs} ->
      Fmt.pf fmt "@[<2>@[&<%d,%d>(%a)@]@,%a@]"
        width align
        pp pointee
        pp_add_specs specs
    | TStruct fields ->
      Fmt.pf fmt "@[<2>{{%a}}@]" Fmt.(list ~sep:comma pp) fields
    | TFrag {src_pos; mem_ty; specs} ->
      Fmt.pf fmt "@[<2>@[%a@] as@ @[%a%a@]@]"
        Hole.pp src_pos pp mem_ty pp_add_specs specs
    | TSplit {discrs; branches} ->
      Fmt.pf fmt "@[<v 2>@[split(%a)@] (@,%a@;<0 -2>)@]"
        Fmt.(list ~sep:comma Path.pp) discrs
        Fmt.(list ~sep:comma pp_branch) branches
  and pp_add_specs fmt specs =
    if PathMap.is_empty specs then ()
    else Fmt.pf fmt " w %a" (PathMap.pp ~pp_sep:(Fmt.any " w ") ~pp_arrow:(Fmt.any ":") Path.pp pp) specs
  and pp_branch fmt {lhs; provs; ty} =
    Fmt.pf fmt "@[<2>%a@ from@ @[%a@] =>@ @[%a@]@]"
      Fmt.(list ~sep:comma @@ option ~none:(any "_") Z.pp_print) lhs
      (ProvSet.pp Prov.pp) provs
      pp ty
end
