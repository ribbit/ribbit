(** Source Types *)
include Base_types.Types

exception Unhandled_polymorphic_type of ty
exception Constructor_not_in_type of Name.t * ty

(** Utilities *)

let rec unroll (env : Env.t) (ty : ty) : ty =
  match ty with
  | Type sym ->
    unroll env @@ Env.find_ty sym env
  | _ -> ty

(* TODO Make a better equiv procedure *)
let equiv env ty1 ty2 =
  (* unroll env ty1 = unroll env ty2 *)
  let rec aux acc ty1 ty2 = match ty1, ty2 with
    | Type n1, Type n2 -> (* TODO this is not exhaustive (parallel non-confluence) *)
      n1 = n2 || List.mem (n1, n2) acc ||
      aux ((n1, n2) :: acc) (Env.find_ty n1 env) (Env.find_ty n2 env)
    | Type n, ty ->
      aux acc (Env.find_ty n env) ty
    | ty, Type n ->
      aux acc ty (Env.find_ty n env)
    | Word {width = l1; _}, Word {width = l2; _} ->
      l1 = l2
    | Product l1, Product l2 ->
      List.fold_left2 (fun b ty1 ty2 -> b && aux acc ty1 ty2) true l1 l2
    | Record l1, Record l2 -> (* TODO sort by field name *)
      List.fold_left2 (fun b (x1, ty1) (x2, ty2) -> b && x1 = x2 && aux acc ty1 ty2) true l1 l2
    | Sum l1, Sum l2 -> (* TODO sort by variant name *)
      List.fold_left2 (fun b (k1, ty1) (k2, ty2) -> b && k1 = k2 && aux acc ty1 ty2) true l1 l2
    | _ -> false
  in
  aux [] ty1 ty2

let rec is_leaf env ty = match ty with
  | Type n -> is_leaf env (Env.find_ty n env) 
  | Word _ -> true
  | Product _
  | Record _
  | Sum _ -> false

let find_constr_in_type constr ty0 =
  match ty0 with
  | Sum l ->
    begin try List.assoc constr l with
      | Not_found -> raise (Constructor_not_in_type (constr, ty0))
    end
  | _ -> raise (Constructor_not_in_type (constr, ty0))

exception Incompatible_types of (Base_types.Prov.t * ty)

let rec check_compatible env (prov : Base_types.Prov.t) (ty0 : ty) =
  match prov, unroll env ty0 with
  | Top, _ -> ()
  | Tup ps, Product tys when List.length ps = List.length tys ->
    List.iter2 (check_compatible env) ps tys
  | Rec ps, Record tys ->
    let ps = Name.Map.of_list ps in
    let tys = Name.Map.of_list tys in
    let _ = Name.Map.merge_safe ps tys
        ~f:(fun s -> function
            | `Both (p, ty) -> Some (check_compatible env p ty)
            | _ -> raise (Incompatible_types (prov, ty0))
          )
    in
    ()
  | Cons (cons, p), Sum tys ->
    begin match List.assoc_opt cons tys with
      | Some ty -> check_compatible env p ty
      | None -> raise @@ Constructor_not_in_type (cons, ty0)
    end
  | _ -> raise (Incompatible_types (prov, ty0))

let is_compatible env (prov : Base_types.Prov.t) (ty0 : ty) =
  try check_compatible env prov ty0 ; true with
  | Incompatible_types _ -> false
  | Constructor_not_in_type _ -> false

let rec specialize env (prov : Base_types.Prov.t) (ty0 : ty) =
  match prov, unroll env ty0 with
  | Top, _ -> ty0
  | Tup ps, Product tys when List.length ps = List.length tys ->
    Product (List.map2 (specialize env) ps tys)
  | Rec ps, (Record tys as ty) ->
    let ps = Name.Map.of_list ps in
    let tys = Name.Map.of_list tys in
    let map = Name.Map.merge_safe ps tys
        ~f:(fun s -> function
            | `Both (p, ty) -> Some (specialize env p ty)
            | _ -> raise (Incompatible_types (prov, ty))
          )
    in
    Record (Name.Map.to_list map)
  | Cons (cons, p), Sum tys ->
    begin match List.assoc_opt cons tys with
      | Some ty -> Sum [cons, specialize env p ty]
      | None -> raise @@ Constructor_not_in_type (cons, ty0)
    end
  | _, ty -> (* raise (Incompatible_types (prov, ty)) *)
    let err = Fmt.str "specialize: prov %a is incompatible with type %a"
      Base_types.Prov.pp prov pp ty
    in
    failwith err

let specializel env prov tys =
  List.map (specialize env prov) tys

let rec focus (h0 : Hole.t) (env : Env.t) ty0 =
  match Hole.pop h0, unroll env ty0 with
  | None, _ -> ty0
  | Some (HField n, hole), Record l ->
    focus hole env (List.assoc n l)
  | Some (HAccess pos, hole), Product l ->
    focus hole env (List.nth l pos)
  | Some (HConstr constr, hole), (Sum sum as ty) ->
    focus hole env (find_constr_in_type constr ty)
  | Some (HBits {offset; width}, hole), Word spec
    when (offset + width <= spec.width) ->
    Word {width; values=()}
  | _, Type _ ->
    assert false
  | _, ty ->
    Report.fail "@[<v 2>Mismatched holexpr types:@ %a@;<0 -2>doesn't fit with@ %a@]@." Hole.pp h0 pp ty

let rec gather_leaves_aux env pa ty0 = match ty0 with
  | Type _ -> Hole.Map.singleton pa ty0
  | Word _ -> Hole.Map.singleton pa ty0
  | Product tys ->
    let field_subterms = List.mapi
        (fun i ty -> gather_leaves_aux env Hole.(pa $. HAccess i) ty)
        tys in
    List.fold_left
      (Hole.Map.union (fun _ -> assert false))
      Hole.Map.empty
      field_subterms
  | Record tys -> 
    let field_subterms = List.map
        (fun (field, ty) -> gather_leaves_aux env Hole.(pa $. HField field) ty)
        tys in
    List.fold_left
      (Hole.Map.union (fun _ -> assert false))
      Hole.Map.empty
      field_subterms
  | Sum tys -> 
    let field_subterms = List.map
        (fun (c, ty) -> gather_leaves_aux env Hole.(pa $. HConstr c) ty)
        tys in
    List.fold_left
      (Hole.Map.union (fun _ -> assert false))
      Hole.Map.empty
      field_subterms
let gather_leaves env ty = gather_leaves_aux env Empty ty

(** Error handling *)
let prepare_error = function
  | Unhandled_polymorphic_type ty ->
    Some (Report.errorf
            "@[<v 2> Polymorphic types in this position are not supported@;<1 2>%a@]"
            pp ty)
  | Constructor_not_in_type (t, ty) ->
    Some (Report.errorf
            "@[<v 2> The constructor '%a' doesn't belong to the type@;<1 2>%a@]"
            Name.pp t pp ty)
  | _ -> None
let () = Report.register_report_of_exn prepare_error
