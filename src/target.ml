(** DPS IR *)

type name = Name.t

type fun_id = FId of name
let fresh_fun_id () = FId (Name.fresh_with_prefix "f" ())

type in_id = IId of name
let pp_iid fmt (IId x) = Name.pp fmt x
let fresh_in_id ?(base="") () =
  IId (Name.fresh_with_prefix ("s"^base) ())
let hashed_src =
  let h : (in_id * Mem.path, in_id) Hashtbl.t = Hashtbl.create 17 in
  fun ?base (s,p) ->
    CCHashtbl.get_or_add h
      ~k:(s,p)
      ~f:(fun _ -> fresh_in_id ?base ())

type out_id = OId of name
let pp_oid fmt (OId y) = Name.pp fmt y
let fresh_out_id ?(base="") () =
  OId (Name.fresh_with_prefix ("d"^base) ())

module InIDMap = struct
  include CCMap.Make(struct
    type t = in_id
    let compare (IId x) (IId x') = String.compare x x' end)
  let pp ?(sep=(Fmt.any ",@ ")) ?(bind=(Fmt.any ":@ ")) ppv =
    Fmt.iter_bindings ~sep iter
      (fun fmt (k,v) -> Fmt.pf fmt "@[<2>%a%a@[%a@]@]" pp_iid k bind () ppv v)
end
module OutIDMap = struct
  include CCMap.Make(struct
    type t = out_id
    let compare (OId y) (OId y') = String.compare y y' end)
  let pp ?(sep=(Fmt.any ",@ ")) ?(bind=(Fmt.any ":@ ")) ppv =
    Fmt.iter_bindings ~sep iter
      (fun fmt (k,v) -> Fmt.pf fmt "@[<2>%a%a@[%a@]@]" pp_oid k bind () ppv v)
end

(* TODO move to Mem.V *)
type addr_id = AId of name
let pp_addr fmt (AId a) = Name.pp fmt a
let fresh_addr () = AId (Name.fresh_with_prefix "a" ())
      
let hashed_dest =
  let h : (out_id * Mem.path, out_id) Hashtbl.t = Hashtbl.create 17 in
  fun (loc,p) ->
    CCHashtbl.get_or_add h
      ~k:(loc,p)
      ~f:(fun _ -> fresh_out_id ())

type rhs = (* what we may write to output locations *)
  | CopyIn of in_id
  | Imm of Z.t
  | Alloc of int

let pp_rhs fmt = function
  | CopyIn (IId s) -> Name.pp fmt s
  | Imm z -> Z.pp_print fmt z
  | Alloc width -> Fmt.pf fmt "alloc(%i)" width

type val_bind =
  | SubIn of in_id * in_id * Mem.path
  | SubOut of out_id * out_id * Mem.path
  | NewOut of out_id * int

let pp_val_bind fmt = function
  | SubIn (IId new_id, IId old_id, mpath) ->
    Fmt.pf fmt "in %s = %s%a" new_id old_id Mem.Path.pp_suffix mpath
  | SubOut (OId new_id, OId old_id, mpath) ->
    Fmt.pf fmt "out %s = %s%a" new_id old_id Mem.Path.pp_suffix mpath
  | NewOut (OId id, width) ->
    Fmt.pf fmt "out %s = new(%i)" id width

type prim =
  | Not | Neg
  | Add | Sub | Mul | Div
  | Leq | Le | Geq | Ge
  | Equal
  | And | Or
  | Xor
  | ShiftLeft | ShiftRight

let pp_prim fmt = function
  | Not -> Fmt.pf fmt "NOT"
  | Neg -> Fmt.pf fmt "NEG"
  | Add -> Fmt.pf fmt "ADD"
  | Sub -> Fmt.pf fmt "SUB"
  | Mul -> Fmt.pf fmt "MUL"
  | Div -> Fmt.pf fmt "DIV"
  | Leq -> Fmt.pf fmt "LEQ"
  | Le -> Fmt.pf fmt "LE"
  | Geq -> Fmt.pf fmt "GEQ"
  | Ge -> Fmt.pf fmt "GE"
  | Equal -> Fmt.pf fmt "EQ"
  | And -> Fmt.pf fmt "AND"
  | Or -> Fmt.pf fmt "OR"
  | Xor -> Fmt.pf fmt "XOR"
  | ShiftLeft -> Fmt.pf fmt "SL"
  | ShiftRight -> Fmt.pf fmt "SR"

type shallow_shape = (* what we may refine ``wildcard'' shapes to *)
  | ShSpec of int * Base_types.bitrange
  | ShStruct of int list
  | ShInt of int (* this one is different: composite2int *)

let pp_shallow_shape fmt = function
  | ShSpec (width0, { offset; width }) ->
    Fmt.pf fmt "?%i with %a:?%i" width0 Base_types.pp_bitrange {offset;width} width
  | ShStruct l ->
    Fmt.pf fmt "{{%a}}" (Fmt.list ~sep:Fmt.comma (fun fmt n -> Fmt.pf fmt "?%i" n)) l
  | ShInt width ->
    Fmt.pf fmt "w%i" width

type t =
  | Fail
  | Success
  | Comment of string * t
  | Call of fun_id * in_id Name.Map.t * out_id * t (* f(xs, y); t *)
  | Prim of prim * out_id * in_id list * t (* OP y x x' ...; t *)
  | Let of val_bind * t (* let in/out x = x'.π^ or new(l) in t *)
  | Freeze of out_id * in_id * t (* use output y as an input x in t *)
  | Write of out_id * rhs * t (* y := rhs; t *)
  | Cast of out_id * shallow_shape * t (* refine _l to {{_l, _l}} or _l |>< _l; t *)
  | Switch of in_id * ((Z.t * t) list) * (t option)

let comment next = Fmt.kstr (fun s -> Comment (s, next))

let switch discr ?default branches =
  match default, branches with
  | None, [] -> assert false
  | Some br, _ ->
    if List.for_all (fun (_, next) -> br = next) branches then br
    else Switch (discr, branches, default)
  | None, (_, h)::t -> 
    if List.for_all (fun (_, next) -> h = next) t then h
    else Switch (discr, branches, default)

let pp_iids = Fmt.list ~sep:Fmt.sp (fun fmt (IId s) -> Name.pp fmt s)
let pp_iidmap = InIDMap.pp (fun fmt (IId s) -> Name.pp fmt s)
let rec pp fmt =
  let pp_branch = Fmt.pair ~sep:(Fmt.any " -> ") Z.pp_print pp in
  function
  | Fail ->
    Fmt.pf fmt "fail"
  | Success ->
    Fmt.pf fmt "success"
  | Comment (str, next) ->
    Fmt.pf fmt "@[<v>@[(* %s *)@]@ %a@]" str pp next
  | Call (FId f, iids, OId y, next) ->
    Fmt.pf fmt "@[<v>@[<hv 2>call %s %a %s@;<1 -2>@];@ %a@]"
      f (Name.Map.pp pp_iid) iids y pp next
  | Prim (prim, OId d, l, next) ->
    Fmt.pf fmt "@[<v> %s = %a %a;@ %a@]" d pp_prim prim pp_iids l pp next
  | Let (bind, next) ->
    Fmt.pf fmt "@[<v>@[<hv 2>let %a@;<1 -2>@];@ %a@]" pp_val_bind bind pp next
  | Write (OId y, rhs, next) ->
    Fmt.pf fmt "@[<v>write %s <- %a;@ %a@]" y pp_rhs rhs pp next
  | Freeze (OId y, IId x, next) ->
    Fmt.pf fmt "@[<v>freeze %s to %s;@ %a@]" y x pp next
  | Cast (OId id, shape, next) ->
    Fmt.pf fmt "@[<v>cast %s to %a;@ %a@]" id pp_shallow_shape shape pp next
  | Switch (IId discr, branches, None) ->
    Fmt.pf fmt "@[<v 2>switch %s {@,%a@]@,}" discr
      Fmt.(list ~sep:cut pp_branch) branches
  | Switch (IId discr, branches, Some default) ->
    Fmt.pf fmt "@[<v 2>switch %s {@,%a@,_ -> %a@]@,}" discr
      Fmt.(list ~sep:cut pp_branch) branches
      pp default

let rec pp_head_instr fmt =
  let pp_branch = Fmt.pair ~sep:(Fmt.any " -> ") Z.pp_print pp_head_instr in
  function
  | Fail ->
    Fmt.pf fmt "fail"
  | Success ->
    Fmt.pf fmt "success"
  | Comment (str, _) ->
    Fmt.pf fmt "(* %s *)" str
  | Call (FId f, iids, OId d, _) ->
    Fmt.pf fmt "@[<v>@[<hv 2>call %s %a %s@;<1 -2>@]@]"
      f (Name.Map.pp pp_iid) iids d
  | Let (bind, _) ->
    Fmt.pf fmt "@[<v>@[<hv 2>let %a@;<1 -2>@]@]" pp_val_bind bind 
  | Write (OId d, rhs, _) ->
    Fmt.pf fmt "@[<v>write %s <- %a;@]" d pp_rhs rhs
  | Cast (OId id, shape, _) ->
    Fmt.pf fmt "@[<v>cast %s to %a@]" id pp_shallow_shape shape
  | Freeze (OId d, IId s, _) ->
    Fmt.pf fmt "@[<v>transfer %s to %s;@]" d s
  | Prim (prim, OId d, l, _) ->
    Fmt.pf fmt "@[<v> %s = %a %a@]" d pp_prim prim pp_iids l
  | Switch (IId discr, branches, None) ->
    Fmt.pf fmt "@[<v 2>switch %s {@,%a@]@,}" discr
      Fmt.(list ~sep:cut pp_branch) branches
  | Switch (IId discr, branches, Some default) ->
    Fmt.pf fmt "@[<v 2>switch %s {@,%a@,_ -> %a@]@,}" discr
      Fmt.(list ~sep:cut pp_branch) branches
      pp_head_instr default

type fundef = fun_id * in_id Name.Map.t * out_id * t
type program = fundef list * t

let pp_fundef fmt (FId f, sids, OId d, body) =
  Fmt.pf fmt "@[<hv 2>let rec %s %a %s = %a@;<1 -2>in@]@ "
    f (Name.Map.pp pp_iid) sids d pp body
let pp_program =
  Fmt.(pair (vbox @@ list ~sep:cut pp_fundef) pp)

let fold ~f_fail ~f_success ~f_comment ~f_call ~f_let ~f_write ~f_prim ~f_freeze ~f_cast ~f_switch e =
  let tbl = Hashtbl.create 15 in
  let rec aux e =
    match e with
    | Fail -> f_fail ()
    | Success -> f_success ()
    | _ ->
      match Hashtbl.find_opt tbl e with
      | None ->
        begin let acc = match e with
            | Fail -> f_fail ()
            | Success -> f_success ()
            | Comment (str, next) -> f_comment str (aux next)
            | Call (f, xs, y, next) -> f_call f xs y (aux next)
            | Prim (prim, y, rhs, next) -> f_prim prim y rhs (aux next)
            | Let (bind, next) -> f_let bind (aux next)
            | Write (y, rhs, next) -> f_write y rhs (aux next)
            | Cast (y, sh, next) -> f_cast y sh (aux next)
            | Freeze (y, x, next) -> f_freeze y x (aux next)
            | Switch (discr_id, branches, default) ->
              let branches = List.map (fun (lhs, cont) -> lhs, aux cont) branches in
              let default = Option.map aux default in
              f_switch discr_id branches default
          in
          Hashtbl.add tbl e acc;
          acc
        end
      | Some x -> x
  in
  aux e

let default_fold
  ?(f_fail = fun _ -> Fail)
  ?(f_success = fun _ -> Success)
  ?(f_comment = fun str next -> Comment (str, next))
  ?(f_let = fun bind next -> Let (bind, next))
  ?(f_write = fun d rhs next -> Write (d, rhs, next))
  ?(f_cast = fun d sh next -> Cast (d, sh, next))
  ?(f_prim = fun prim d s next -> Prim (prim, d, s, next))
  ?(f_freeze = fun d s next -> Freeze (d, s, next))
  ?(f_call = fun f sids d next -> Call (f, sids, d, next))
  ?(f_switch = (fun s brs default -> switch s ?default brs))
  () =
  fold ~f_fail ~f_success ~f_comment ~f_let ~f_write ~f_cast ~f_freeze ~f_call ~f_switch ~f_prim


(** Dot output *)
module Dot = struct
  type instr =
    | Comment of string
    | LetSrc of name * Mem.path * name
    | LetDest of name * Mem.path * name
    | Write of name * rhs
    | Alloc of name * int
    | Prim of prim * name * name list
    | Cast of name * shallow_shape
    | Freeze of name * name
  type terminator = 
    | Decl of name
    | LetFun of name * (name Name.Map.t) * name
    | Fail
    | Success
    | Call of name * (name Name.Map.t) * name
    | Switch of name * (int * Z.t list) list
  type node = instr list * terminator option
  type arrow = int option

  let as_dot_graph entry (funs, e) =
    let curr_id = ref 0 in
    let vertices : (int, node) Hashtbl.t = Hashtbl.create 17 in
    let add_term term =
      incr curr_id;
      Hashtbl.add vertices !curr_id ([], Some term);
      !curr_id
    in
    let edges : (int * int, arrow list) Hashtbl.t = Hashtbl.create 17 in
    let add_edge = CCHashtbl.add_list edges in
    let add_instr next x =
      incr curr_id;
      add_edge (!curr_id , next) None;
      Hashtbl.add vertices !curr_id ([x], None);
      !curr_id
    in
    let f_fail () = add_term Fail
    and f_success () = add_term Success
    and f_comment s next = add_instr next @@ Comment s
    and f_call (FId f) iids (OId y) next =
      let xs = Name.Map.map (fun (IId x) -> x) iids in
      let block = add_term @@ Call (f, xs, y) in
      add_edge (block, next) None;
      block
    and f_prim prim (OId d) l next =
      add_instr next @@ Prim (prim, d, List.map (fun (IId s) -> s) l)
    and f_let bind next =
      match bind with
      | SubIn (IId x', IId x, mpath) ->
        add_instr next @@ LetSrc (x', mpath, x)
      | SubOut (OId y', OId y, mpath) ->
        add_instr next @@ LetDest (y', mpath, y)
      | NewOut (OId y, l) ->
        add_instr next @@ Alloc (y, l)
    and f_write (OId d) rhs next =
      add_instr next @@ Write (d, rhs)
    and f_cast (OId d) shape next =
      add_instr next @@ Cast (d, shape)
    and f_freeze (OId d) (IId s) next =
      add_instr next @@ Freeze (d, s)
    and f_switch (IId discr) branches default =
      let cases =
        List.map (fun (z, x) -> Some z, x) branches @
        match default with
        | Some x -> [None, x]
        | None -> []
      in
      let cases =
        CCList.group_succ  ~eq:(fun (_,x) (_,y) -> x = y) cases
        |> List.mapi (fun i l ->
            let vs, targets = List.split l in
            let target = List.hd targets in
            i, List.filter_map Fun.id vs, target
          )
      in
      let id =
        add_term @@
        Switch (discr, List.map (fun (i, lhs, _) -> i, lhs) cases)
      in
      List.iter (fun (i, lhs, x) -> add_edge (id, x) (Some i)) cases;
      id
    in
    let expr_to_dot = fold ~f_fail ~f_success ~f_comment ~f_call ~f_freeze ~f_let ~f_write ~f_prim ~f_cast ~f_switch in
    let n0 = add_term entry in
    let n = expr_to_dot e in
    add_edge (n0, n) None;
    List.iter (fun (FId f, iids, OId d, body) ->
        let body_id = expr_to_dot body in
        let xs = Name.Map.map (fun (IId x) -> x) iids in
        let id = add_term @@ LetFun (f, xs, d) in
        add_edge (id, body_id) None)
      funs;
    (vertices, edges)

  let simplify (vertices, edges : (int, node) Hashtbl.t * (int * int, arrow list) Hashtbl.t) =
    let has_node_with_single_input () =
      CCHashtbl.to_iter edges
      |> Iter.find_map (fun ((v1, v2), edge0) ->
           let out_edges1 =
             CCHashtbl.keys_list edges |> List.filter (fun (v,_) -> v = v1)
           in
           let in_edges2 =
             CCHashtbl.keys_list edges |> List.filter (fun (_,v) -> v = v2)
           in
           let instrs1, term1 = Hashtbl.find vertices v1 in
           let instrs2, term2 = Hashtbl.find vertices v2 in
           if List.length in_edges2 = 1 &&
              List.length out_edges1 = 1 &&
              term1 = None &&
              List.for_all Option.is_none edge0
           then
             Some (v1, instrs1, v2, instrs2, term2)
           else
             None
        )
    in
    let rec aux () =
      match has_node_with_single_input () with
      | None -> (vertices, edges)
      | Some (v1, instrs1, v2, instrs2, term2) ->  begin
          Hashtbl.remove edges (v1,v2);
          Hashtbl.replace vertices v1 (instrs1 @ instrs2, term2);
          Hashtbl.remove vertices v2;
          List.iter (fun ((v,v'), edge)  ->
              if v = v2 then begin
                Hashtbl.remove edges (v,v');
                Hashtbl.replace edges (v1, v') edge;
              end
            )
            (CCHashtbl.to_list edges);
          aux ()
        end
    in
    aux ()

  let pp_lhs fmt = function
    | [] -> Fmt.pf fmt "_"
    | l -> Fmt.pf fmt "%a" (Fmt.list ~sep:Fmt.comma Z.pp_print) l

  let pp_bindings fmt bindings = 
    let f fmt (i,n) =
      Fmt.pf fmt {|<%i> %a|} i Name.pp n
    in
    Fmt.pf fmt "%a"
      (Fmt.list ~sep:Fmt.nop f)
      bindings

  let escape_graphviz s =
    CCString.replace ~sub:"{" ~by:"\\{" @@
    CCString.replace ~sub:"}" ~by:"\\}" @@
    CCString.replace ~sub:"<" ~by:"\\<" @@
    CCString.replace ~sub:">" ~by:"\\>" @@
    s

  let rec_color = "orange"
  let eff_color = "deepskyblue"

  let pp_name_map = Name.Map.pp ~bind:(Fmt.any ":") Name.pp

  let pp_term ppf = function
    | Fail -> Fmt.pf ppf "fail"
    | Success -> Fmt.pf ppf "success"
    | Call (f, ss, d) -> Fmt.pf ppf "call %s %a %s\\l" f pp_name_map ss d
    | LetFun (f, ss, d) -> Fmt.pf ppf "Let rec %s = λ %a %s." f pp_name_map ss d
    | Decl n -> Fmt.pf ppf "Let %s =" n
    | Switch (discr, cases) ->
      let f fmt (i,x) = Fmt.pf fmt "<%i> %a" i pp_lhs x in
      Fmt.pf ppf "{Switch %s\\l | {%a}}" discr Fmt.(list ~sep:(any " | ") f) cases

  let pp_instr ppf = function
    | Comment s -> Fmt.pf ppf "/* %s */" s
    | LetSrc (x, mpos, y) -> Fmt.pf ppf "letᵢ %s = %s%a" x y Mem.Path.pp_suffix mpos
    | LetDest (x, mpos, y) -> Fmt.pf ppf "letₒ %s = %s%a" x y Mem.Path.pp_suffix mpos
    | Write (d, rhs) ->
      Fmt.pf ppf "%s := %a" d pp_rhs rhs
    | Alloc (y, l) ->
      Fmt.pf ppf "new out %s (%i bits)" y l
    | Cast (x, shape) ->
      Fmt.pf ppf "Cast %s as %a" x pp_shallow_shape shape
    | Prim (prim, x, l) ->
      Fmt.pf ppf "@[<h>%s = %a %a@]" x pp_prim prim (Fmt.list ~sep:Fmt.sp Name.pp) l
    | Freeze (d, s) ->
      Fmt.pf ppf "Freeze %s to %s" d s

  let mk_label (instrs, term) =
    match instrs, term with
    | [], Some term -> Fmt.str "%a" pp_term term
    | l, None ->
      escape_graphviz @@ 
      Fmt.str "@[<v>%a@]" Fmt.(list @@ (pp_instr ++ any "\\l")) l
    | l, Some term -> 
      let body = 
        escape_graphviz @@ 
        Fmt.str "@[<v>%a@]" Fmt.(list @@ (pp_instr ++ any "\\l")) l
      in
      Fmt.str "{%s|{%a}}" body pp_term term

  let pp_vertex ppf (id, node) =
    let label = mk_label node in       
    let attrs =
      match node with
      | ([], Some Decl _) | ([], Some LetFun _) ->
        [ ("label", label);
          (* ("style","filled"); *)
          (* ("fillcolor", eff_color); *)
          ("shape", "ellipse") ]
      | _ ->
        [ ("label", label);
          (* ("style","filled"); *)
          (* ("fillcolor", eff_color); *)
          ("shape", "Mrecord") ]
    in
    let pp_attr ppf (field, v) = Fmt.pf ppf {|%s="%s"|} field v in
    Fmt.pf ppf "%d [%a];" id (Fmt.list ~sep:(Fmt.any ", ") pp_attr) attrs

  let pp_edges ppf ((id, id'), arrows) =
    let pp_port fmt = function
      | Some i -> Fmt.pf fmt ":%i" i
      | None -> Fmt.nop fmt ()
    in
    List.iter (function
        | port -> Fmt.pf ppf {|%d%a:s -> %d:n;|} id pp_port port id'
      )
      arrows

  let pp_with_entry ppf name entry t =
    let vertices, edges = simplify @@ as_dot_graph entry t in
    Fmt.pf ppf
      {|@[<v>digraph G {
@,@,concentrate=true;rankdir="TB";label="%s";@,
node [fontname="DejaVu Sans Mono"];@,@,
%a@,@,
%a@,
}@]|}
      name
      (Fmt.hashtbl ~sep:(Fmt.any "@,@,") pp_vertex)
      vertices
      (Fmt.hashtbl ~sep:(Fmt.any "@,") pp_edges)
      edges

  let pp_decl ppf (name, t) =
    pp_with_entry ppf name (Decl name) t

  let pp_fun ppf (name, FId f, args, OId out, t) =
    let xs = Name.Map.map (fun (IId x) -> x) args in
    pp_with_entry ppf name (LetFun (f, xs, out)) t
end

(*** Simplification/optimization passes ***)

let concat t1 t2 =
  default_fold ~f_success:(fun () -> t2) () t1

let concat_prog (funs1, next1) (funs2, next2) =
  funs1 @ funs2, concat next1 next2

(* alpha-ren in func bodies to avoid name collisions after beta red *)
(* let refresh_bindings =
  let f_let bind next = match bind with
    | Src (y, x, mpath) ->
      let y' = fresh_src () in
      Let (Src (y', x, mpath), subst_src y y' next)
    | Dest (y, x, mpath) ->
      let y' = fresh_dest () in
      Let (Dest (y', x, mpath), subst_dest y y' next)
  and f_alloc a sh next =
    let a' = fresh_addr () in
    Alloc (a', sh, subst_addr a a' next)
  in
  default_fold ~f_let ~f_alloc () *)

(* let hijack_return cont =
  default_fold ~f_success:(fun _ -> cont) ()

let rec expand_fun_body f ss d body =
  let f_apply f' ss' d' next =
    if f' = f then
      hijack_return next @@ subst_dest d d' @@
        Name.Map.fold
          (fun x s body -> subst_src s (Name.Map.find x ss') body)
          ss
          body
    else
      Call (f', ss', d', next)
  in
  default_fold ~f_apply ()

let rec expand_fun_bodies funs =
  let f_apply f ss d next =
    match List.find_opt (fun (f', _, _, _) -> f' = f) funs with
    | Some (f, ss0, d0, body) -> 
      (* refresh_bindings *) body
      |> Name.Map.fold (fun x s0 body -> subst_src s0 (Name.Map.find x ss) body) ss0
      |> subst_dest d0 d
      |> expand_fun_bodies funs
      |> hijack_return next
    | None -> Call (f, ss, d, next)
  in
  default_fold ~f_apply ()

let call_cycles funs =
  let rec aux chain = function
    | Fail | Success -> []
    | Comment (_, next) | Let (_, next) | Write (_, _, next)
    | Alloc (_, _, next) | Cast (_, _, next) | Freeze (_, _, next)
    | Prim (_,_,_,next)
      ->
      aux chain next
    | Call (f, _, _, next) ->
      let next_cycles = aux chain next in
      if List.mem f chain then
        f :: next_cycles
      else
        let _, _, _, body = List.find (fun (f', _, _, _) -> f' = f) funs in
        let body_cycles = aux (f :: chain) body in
        body_cycles @ next_cycles
    | Switch (_, brs, def) ->
      let brs_cycles = List.concat_map (fun (_, e) -> aux chain e) brs in
      let def_cycles = Option.fold ~none:[] ~some:(aux chain) def in
      brs_cycles @ def_cycles
  in
  aux []

let inline_calls (funs, next) =
  let cycles = call_cycles funs next in
  let cycle_roots, other_funs = List.partition
    (fun (f, _, _, _) -> List.mem f cycles)
    funs
  in
  let funs = List.map (fun (f, s, d, body) ->
      f, s, d, expand_fun_bodies other_funs body)
    cycle_roots
  in
  funs, expand_fun_bodies other_funs next *)

module Metrics = struct
  (* TODO *)
  let all_metrics = []
end


(*** Interpreter ***)

type eval_state = {
  in_locs: (addr_id * Mem.path) InIDMap.t;
  out_locs: (addr_id * Mem.path) OutIDMap.t;
  store: Mem.V.store;
}

let pp_state fmt {in_locs; out_locs; store} =
  Fmt.pf fmt "@[input locs:@ @[<hov 2>{@ %a@ }@]@] @,\
              @[output locs:@ @[<hov 2>{@ %a@ }@]@] @,\
              @[store:@ @[<hov 2>{@ %a@ }@]@]"
    (InIDMap.pp ~sep:Fmt.semi ~bind:(Fmt.any "@ ->@ ") @@
      Fmt.pair ~sep:Fmt.nop pp_addr Mem.Path.pp_suffix)
    in_locs
    (OutIDMap.pp ~sep:Fmt.semi ~bind:(Fmt.any "@ ->@ ") @@
      Fmt.pair ~sep:Fmt.nop pp_addr Mem.Path.pp_suffix)
    out_locs
    Mem.V.pp_store store

let rec get_imm mval =
  match mval with
  | Mem.V.VImm { width; value } ->
    width, value
  (* FIX same ugly bridge between old mtys and new mvals *)
  | VWord { base; range = {offset = 0; width}; spec }
    when width = Mem.V.size_of base ->
    get_imm spec
  (* | VWord (i, s) ->
    let rec aux = function
      | Mem.V.Imm z -> z
      | Word specs ->
        let rg = {Mem.Path. offset=0; width=i} in
        begin match Mem.V.BitMap.get rg specs with
          | None -> 
            Report.fail "get_imm(%a)" Mem.V.pp mval
          | Some w -> aux w
        end
      | Ptr _ ->
        Report.fail "get_imm(%a)" Mem.V.pp mval
    in
    i, aux s *)
  | _ ->
    Report.fail "get_imm(%a)" Mem.V.pp mval

let eval_prim prim (mvals : Mem.V.t list) =
  let imms = List.map get_imm mvals in
  let of_bool b = if b then Z.one else Z.zero in
  let i, z = match prim, imms with
    | Not, [i, z] -> i, Z.lognot z
    | Neg, [i, z] -> i, Z.neg z
    | Add, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 + z2)
    | Sub, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 - z2)
    | Mul, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 * z2)
    | Div, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 / z2)
    | Leq, [i1, z1; i2, z2] when i1 = i2 -> i1, of_bool Z.(z1 <= z2)
    | Le, [i1, z1; i2, z2] when i1 = i2 -> i1, of_bool Z.(z1 < z2)
    | Geq, [i1, z1; i2, z2] when i1 = i2 -> i1, of_bool Z.(z1 >= z2)
    | Ge, [i1, z1; i2, z2] when i1 = i2 -> i1, of_bool Z.(z1 > z2)
    | Equal, [i1, z1; i2, z2] when i1 = i2 -> i1, of_bool Z.(z1 = z2)
    | And, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 land z2)
    | Or, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 lor z2)
    | Xor, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 lxor z2)
    | ShiftLeft, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 lsl to_int z2)
    | ShiftRight, [i1, z1; i2, z2] when i1 = i2 -> i1, Z.(z1 asr to_int z2)
    | _, _ -> assert false  
  in
  Mem.V.(VImm {width = i; value = z})

let lookup_in_loc state iid =
  try InIDMap.find iid state.in_locs with
  | Not_found ->
    Report.fail "Undefined Input Location %a" pp_iid iid

let lookup_out_loc state oid =
  try OutIDMap.find oid state.out_locs with
  | Not_found ->
    Report.fail "Undefined Output Location %a" pp_oid oid

let read store (AId addr, mpos) =
  Mem.V.read store { addr; mpos }

let write store (AId addr, mpos) mval =
  Mem.V.write store { addr; mpos } mval

let merge_store st1 st2 =
  Name.Map.union (fun k v1 v2 -> Some v2) st1 st2


let eval state (funs, code) : bool * eval_state =
  let rec aux state code =
    (* Report.infof "Eval step" "Instruction: %a@ Input state: %a" pp_head_instr code pp_state state; *)
    match code with
    | Fail ->
      false, state
    | Success ->
      true, state
    | Comment (str, next) ->
      aux state next
    | Prim (prim, oid, iids, next) ->
      let ivals = List.map (fun iid ->
          let loc = lookup_in_loc state iid in
          read state.store loc)
        iids
      in
      let res = eval_prim prim ivals in
      (* let mval = Name.Map.find loc state.store in
      let store, mval' =
        Mem.V.subst_mval_in_mval
          (eval_prim prim svals)
          state.store mpath mval
      in
      let store = Name.Map.add loc mval' store in *)
      let store = write state.store (lookup_out_loc state oid) res in
      aux {state with store} next
    | Call (FId f, iargs, oarg, next) ->
      (* lookup function params and body *)
      let iparams, oparam, body =
        match List.find_opt (fun (FId f', _, _, _) -> f' = f) funs with
        | Some (_, iparams, oparam, body) -> iparams, oparam, body
        | None ->
          Report.fail "unbound function symbol %s in {%a}" f
            (Fmt.list @@ fun fmt (FId f, _, _, _) -> Name.pp fmt f) funs
      in
      (* bind each param to its arg loc *)
      let ienv = Name.Map.fold (fun x iid ienv ->
          let iloc = lookup_in_loc state iid in
          let iparam = Name.Map.find x iparams in
          InIDMap.add iparam iloc ienv)
        iargs
        InIDMap.empty
      in
      let oenv = OutIDMap.singleton oparam (lookup_out_loc state oarg) in
      (* eval body in fresh location binding environments *)
      let status, state' =
        aux { in_locs = ienv; out_locs = oenv; store = state.store } body
      in
      (* restore previous location bindings but keep modified store *)
      let state = { state with store = state'.store } in
      if status then aux state next else status, state
    | Let (SubIn (x', x, mpath), next) ->
      let (addr, mpath0) = InIDMap.find x state.in_locs in
      let mpath' = Mem.Path.(mpath0 ++ mpath) in
      if InIDMap.mem x' state.in_locs then
        assert (lookup_in_loc state x' = (addr, mpath'));
      let in_locs = InIDMap.add x' (addr, mpath') state.in_locs in
      aux {state with in_locs} next
    | Let (SubOut (y', y, mpath), next) ->
      let (addr, mpath0) = OutIDMap.find y state.out_locs in
      let mpath' = Mem.Path.(mpath0 ++ mpath) in
      if OutIDMap.mem y' state.out_locs then
        assert (lookup_out_loc state y' = (addr, mpath'));
      let out_locs = OutIDMap.add y' (addr, mpath') state.out_locs in
      aux {state with out_locs} next
    | Let (NewOut (y, width), next) ->
      assert (not @@ OutIDMap.mem y state.out_locs);
      let (AId addr) = fresh_addr () in
      let store = Name.Map.add addr (Mem.V.VUnk width) state.store in
      let out_locs = OutIDMap.add y (AId addr, Mem.Path.Empty) state.out_locs in
      aux {state with out_locs; store} next
    | Freeze (y, x, next) ->
      assert (not @@ InIDMap.mem x state.in_locs);
      let loc = lookup_out_loc state y in
      let out_locs = OutIDMap.remove y state.out_locs in
      let in_locs = InIDMap.add x loc state.in_locs in
      aux {state with in_locs; out_locs} next
    | Cast (y, sh, next) ->
      let loc = lookup_out_loc state y  in
      let mval = read state.store loc in
      begin match sh with
      | ShStruct widths ->
        let width = List.fold_left (+) 0 widths
        and mval' = Mem.V.VStruct (List.map (fun l -> Mem.V.VUnk l) widths) in
        begin match mval with
        | Mem.V.VUnk width' when width = width' ->
          let store = write state.store loc mval' in
          aux {state with store} next
        | _ ->
          Report.fail "cannot cast %a to %a" Mem.V.pp mval Mem.V.pp mval'
        end
      | ShSpec (width, range) ->
        let mval' = Mem.V.VWord { base = VUnk width; range; spec = VUnk range.width } in
        begin match mval with
        | Mem.V.VUnk width' when width = width' ->
          let store = write state.store loc mval' in
          aux {state with store} next
        | _ ->
          Report.fail "cannot cast %a to %a" Mem.V.pp mval Mem.V.pp mval'
        end
      | ShInt width ->
        (* TODO this is wrong if we don't have a partition, and only a partition *)
        begin match mval with
        | Mem.V.VWord _ when Mem.V.size_of mval = width ->
          let rec fold_one_spec acc = function
            | Mem.V.VWord { base; range; spec = VImm {value; width} }
              when range.width = width ->
              let acc = Z.(acc + value lsl range.offset) in
              fold_one_spec acc base
            | VUnk _ -> acc
            | _ -> assert false
          in
          let mval' = Mem.V.VImm { width; value = fold_one_spec Z.zero mval } in
          let store = write state.store loc mval' in
          aux {state with store} next
        | _ ->
          Report.fail "cannot cast %a to w%i" Mem.V.pp mval width
        end
      end
    | Write (y, rhs, next) ->
      let loc = lookup_out_loc state y in
      begin match rhs with
        | CopyIn x -> (* FIXME shallow copy, this is asking for trouble *)
          let mval = read state.store (lookup_in_loc state x) in
          let store = write state.store loc mval in
          aux { state with store } next
        | Imm value ->
          let width = Mem.V.size_of (read state.store loc) in
          let store = write state.store loc (Mem.V.VImm { width; value }) in
          aux { state with store } next
        | Alloc l ->
          let width = Mem.V.size_of (read state.store loc) in
          let (AId addr) = fresh_addr () in
          let store = Name.Map.add addr (Mem.V.VUnk l)
            @@ write state.store loc (Mem.V.VPtr { width; addr })
          in
          aux { state with store } next
      end
    | Switch (x, branches, default) ->
      let _, discr_val = get_imm (read state.store @@ lookup_in_loc state x) in
      let matching_branch = List.find_opt
        (fun (z, _) -> Z.equal discr_val z)
        branches
      in
      match matching_branch, default with
      | Some (_, next), _ | None, Some next ->
        aux state next
      | _ -> Report.fail "no matching switch branch!"
  in
  aux state code

let eval_value funs vals res_iid code =
  let store, in_locs = List.fold_left (fun (store, in_locs) (x, (st, mval)) ->
      let (AId addr) = fresh_addr () in
      Name.Map.add addr mval @@ merge_store st store,
      InIDMap.add x (AId addr, Mem.Path.Empty) in_locs)
    (Name.Map.empty, InIDMap.empty)
    vals
  in
  let status, state =
    eval { in_locs; out_locs = OutIDMap.empty; store } (funs, code)
  in
  if status then begin
    let mval = read state.store (InIDMap.find res_iid state.in_locs) in
    let store = Mem.V.filter_store state.store mval in
    Some (store, mval)
  end
  else None
