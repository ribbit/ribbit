type tydecl =
  | Abstract
  | Decl of Base_types.Types.ty * Base_types.MemTy.t
type vardecl =
  | Fun of Base_types.Types.ty Name.Map.t * Base_types.Types.ty
  | Val of Base_types.Types.ty

type t = {
  tys : tydecl Name.Map.t ;
  vars : vardecl Name.Map.t ;
}

let empty = { tys = Name.Map.empty ; vars = Name.Map.empty }

let pp_bind_ty ppf (name, slot) = match slot with
  | Abstract ->
    Fmt.pf ppf
      "@[%s abstract@]"
      name
  | Decl (ty, mty) ->
    Fmt.pf ppf
      "@[<hv>@[%a@]@ represented as@ %a@]"
      Base_types.Types.pp_decl (name, ty)
      Base_types.MemTy.pp mty

let pp_bind_v ppf (name, slot) = match slot with
  | Val ty ->
    Fmt.pf ppf
      "@[%s : @[%a@]@]"
      name
      Base_types.Types.pp ty
  | Fun (tys, ty') ->
    Fmt.pf ppf
      "@[%s : @[%a -> %a@]@]"
      name
      (Name.Map.pp Base_types.Types.pp) tys
      Base_types.Types.pp ty'

let pp ppf { tys ; vars } =
  Fmt.pf ppf "@[<v>%a%a@]"
    (Fmt.iter_bindings ~sep:(Fmt.any "@,@,") Name.Map.iter pp_bind_ty) tys
    (Fmt.iter_bindings ~sep:(Fmt.any "@,@,") Name.Map.iter pp_bind_v) vars

exception Unbound_type of Name.t
exception Unbound_variable of Name.t

let union_fun key val1 val2 =
  assert (val1 = val2);
  Some val1
let union a b =
  { tys = Name.Map.union union_fun a.tys b.tys ;
    vars = Name.Map.union union_fun a.vars b.vars }
let union_l l = List.fold_left union empty l

let add_ty name ty env =
  if Name.Map.mem name env.tys then
    Report.fail "Type %s already exists." name
  else
    { env with tys = Name.Map.add name ty env.tys }
let add_var name ty env = { env with vars = Name.Map.add name ty env.vars }

let check_ty name env = 
  match Name.Map.find_opt name env.tys with
  | None -> raise (Unbound_type name)
  | Some (Abstract | Decl _) -> ()

let find_ty name env = match Name.Map.find_opt name env.tys with
  | None | Some Abstract -> raise @@ Unbound_type name
  | Some Decl (ty, _) -> ty

let find_mty name env = match Name.Map.find_opt name env.tys with
  | None | Some Abstract -> raise @@ Unbound_type name
  | Some Decl (_, mty) -> mty

let find_var name env = match Name.Map.find_opt name env.vars with
  | None -> raise @@ Unbound_variable name
  | Some def -> def

let map_tys f env =
  Name.Map.mapi (fun n slot -> match slot with
      | Abstract -> raise @@ Unbound_type n
      | Decl (ty, mty) -> f n ty mty
    ) env.tys

(** Error handling *)
let prepare_error = function
  | Unbound_type ty ->
    Some (Report.errorf
            "@[<v 2> Unbound type@;<1 2>%a@]"
            Name.pp ty)
  | Unbound_variable n ->
    Some (Report.errorf
            "@[<v 2> Unbound variable@;<1 2>%a@]"
            Name.pp n)
  | _ -> None
let () = Report.register_report_of_exn prepare_error
