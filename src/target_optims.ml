open Target


let subst_in s0 s1 =
  let f_call f sids d next =
    let sids = Name.Map.map (fun s -> if s = s0 then s1 else s) sids in
    Call (f, sids, d, next)
  and f_let bind next =
    let bind = match bind with
      | SubIn (y, x, mpath) ->
        if not (y <> s0 && y <> s1) then begin
          let IId n = y in
          Report.warnf "susbt" "Substituting shadowed variable %s" n;
          assert false
        end;
        let x = if x = s0 then s1 else x in
        SubIn (y, x, mpath)
      | _ -> bind
    in
    Let (bind, next)
  and f_write d rhs next =
    let rhs = match rhs with
      | CopyIn s -> CopyIn (if s = s0 then s1 else s)
      | _ -> rhs
    in
    Write (d, rhs, next)
  and f_freeze d s next =
    let s = if s = s0 then s1 else s in
    Freeze (d, s, next)
  and f_switch s branches default =
    let s = if s = s0 then s1 else s in
    switch s branches ?default
  and f_prim prim d s next =
    let s = List.map (fun s -> if s = s0 then s1 else s) s in
    Prim (prim, d, s, next)
  in
  default_fold ~f_call ~f_let ~f_write ~f_switch ~f_prim ~f_freeze ()

let subst_out d0 d1 =
  let f_call f s d next =
    let d = if d = d0 then d1 else d in
    Call (f, s, d, next)
  and f_let bind next =
    let bind = match bind with
      | SubOut (y, x, mpath) ->
        assert (y <> d0 && y <> d1);
        let x = if x = d0 then d1 else x in
        SubOut (y, x, mpath)
      | _ -> bind
    in
    Let (bind, next)
  and f_write d rhs next =
    let d = if d = d0 then d1 else d in
    Write (d, rhs, next)
  and f_freeze d s next =
    let d = if d = d0 then d1 else d in
    Freeze (d, s, next)
  and f_cast d sh next =
    let d = if d = d0 then d1 else d in
    Cast (d, sh, next)
  and f_prim prim d s next =
    let d = if d = d0 then d1 else d in
    Prim (prim, d, s, next)
  in
  default_fold ~f_call ~f_let ~f_write ~f_cast ~f_prim ~f_freeze ()

module Mk(M:sig type t end) = struct
  module M = struct
    type t = M.t * Mem.Path.t
    let compare = Stdlib.compare
  end
  include CCMap.Make(M)
  let inter s1 s2 =
    merge_safe ~f:(fun k -> function
        | `Both (v,_) -> Some v
        | _ -> None
      ) s1 s2
  let interl = function
    | [] -> empty
    | h :: t -> List.fold_left (inter) h t
  let remove_all ~from s =
    fold (fun k _ m -> remove k m) s from
end

module SrcMap = Mk(struct type t = in_id end)
module DestMap = Mk(struct type t = out_id end)

module Gvn = struct

  let global_value_numbering prog =
    let insert_local_lets (common_src, common_dest) ((srcm, destm), p) =
      SrcMap.fold (fun (s', p) s next ->
          match SrcMap.find_opt (s',p) common_src with
          | Some s0 -> subst_in s s0 next
          | None ->
            match p with
            | Empty -> subst_in s s' next
            | _ -> Let (SubIn (s, s', p), next))
        srcm @@
      DestMap.fold (fun (d', p) d next ->
          match DestMap.find_opt (d',p) common_dest with
          | Some d0 -> subst_out d d0 next
          | None ->
            match p with
            | Empty -> subst_out d d' next
            | _ -> Let (SubOut (d, d', p), next))
        destm @@
      p
    in
    let insert_src_lets p srcs next =
      let dest_of_p, destm = SrcMap.partition p srcs in
      let rec aux rest_dests inserted_dests =
        let destm, next = 
          let bound_srcs = SrcMap.values inserted_dests in
          let new_inserted_dests, rest_dests =
            SrcMap.partition
              (fun (s, _) _ -> Iter.mem s bound_srcs)
              rest_dests
          in
          if SrcMap.is_empty new_inserted_dests then
            rest_dests, next
          else
            aux rest_dests new_inserted_dests
        in
        let next =
          SrcMap.fold
            (fun (s, path) s0 next ->
               Let (SubIn (s0, s, path), next))
            inserted_dests next
        in
        destm, next
      in
      aux destm dest_of_p
    in
    let insert_dest_lets p dests next =
      let dest_of_p, destm = DestMap.partition p dests in
      let rec aux rest_dests inserted_dests =
        let destm, next = 
          let bound_dests = DestMap.values inserted_dests in
          let new_inserted_dests, rest_dests =
            DestMap.partition
              (fun (d, _) _ -> Iter.mem d bound_dests)
              rest_dests
          in
          if DestMap.is_empty new_inserted_dests then
            rest_dests, next
          else
            aux rest_dests new_inserted_dests
        in
        let next =
          DestMap.fold
            (fun (d, path) d0 next ->
               match path with
               | Empty -> next
               | _ -> Let (SubOut (d0, d, path), next))
            inserted_dests next
        in
        destm, next
      in
      aux destm dest_of_p
    in
    let rec subst_src srcm s0 =
      let m = SrcMap.filter (fun _ s -> s = s0) srcm in
      match SrcMap.choose_opt m with
      | None -> s0
      | Some ((s, Mem.Path.Empty), _) -> subst_src srcm s
      | Some _ -> s0
    in  
    let rec subst_dest destm d0 =
      let m = DestMap.filter (fun _ d -> d = d0) destm in
      match DestMap.choose_opt m with
      | None -> d0
      | Some ((d, Mem.Path.Empty), _) -> subst_dest destm d
      | Some _ -> d0
    in  
    let rec gvn (srcm, destm) p : _ * t =
      match p with
      | Let (SubIn (s0, s, p), next) -> 
        begin match SrcMap.find_opt (s,p) srcm with
          | Some s' ->
            let srcm = SrcMap.add (s',Empty) s0 srcm in
            gvn (srcm, destm) next
          | None ->
            let srcm = SrcMap.add (s,p) s0 srcm in
            gvn (srcm, destm) next
        end
      | Let (SubOut (d0, loc, p), next) -> 
        begin match DestMap.find_opt (loc,p) destm with
          | Some d' ->
            let destm = DestMap.add (d',Empty) d0 destm in
            gvn (srcm, destm) next
          | None ->
            let destm = DestMap.add (loc,p) d0 destm in
            gvn (srcm, destm) next
        end
      | Let (NewOut (d0, l), next) ->
        let (srcm, destm), next = gvn (srcm, destm) next in
        let destm, next =
          insert_dest_lets (fun (d,_) _ -> d = d0) destm next
        in
        (srcm, destm), Let (NewOut (d0, l), next)
      | Freeze (d0, s0, next) ->
        let d0 = subst_dest destm d0 in
        let s0 = subst_src srcm s0 in
        let (srcm, destm), next = gvn (srcm, destm) next in
        let srcm, next =
          insert_src_lets (fun (s,p) _ -> s = s0) srcm next
        in
        (srcm, destm), Freeze (d0, s0, next)
      | Fail | Success -> (srcm, destm), p
      | Call (f, args, d, next) ->
        let m, next = gvn (srcm, destm) next in
        let args = Name.Map.map (subst_src srcm) args in
        m, Call (f, args, subst_dest destm d, next)
      | Prim (prim, d, args, next) ->
        let m, next = gvn (srcm, destm) next in
        let args = List.map (subst_src srcm) args in
        m, Prim (prim, subst_dest destm d, args, next)
      | Comment (c, next) ->
        let m, next = gvn (srcm, destm) next in
        m, Comment (c, next)
      | Write (d, x, next) ->
        let m, next = gvn (srcm, destm) next in
        m, Write (subst_dest destm d, x, next)
      | Cast (d0, sh, next) ->
        let d0 = subst_dest destm d0 in
        let (srcm, destm), next = gvn (srcm, destm) next in
        let destm, next =
          insert_dest_lets (fun (d,p) _ -> d = d0) destm next
        in
        (srcm, destm), Cast (d0, sh, next)
      | Switch (discr, branches, default) ->
        let default_src, default_dest, default =
          match default with
          | None -> [], [], None
          | Some d ->
            let (srcm, destm), p = gvn (srcm, destm) d in
            [srcm], [destm], Some ((srcm, destm), p)
        in
        let branches =
          List.map (fun (v, next) -> v, gvn (srcm, destm) next) branches
        in
        let srcms, destms =
          CCList.split @@ List.map (fun (_,(m,_)) -> m) branches
        in
        let common_src = SrcMap.interl (default_src @ srcms)
        and common_dest = DestMap.interl (default_dest @ destms)
        in
        (* let pp_src ppf m = SrcMap.iter (fun (s',p) s -> pp_val_bind ppf (Src (s, s', p))) m in *)
        (* let pp_dest ppf m = DestMap.iter (fun (s',p) s -> pp_val_bind ppf (Dest (s, s', p))) m in *)
        (* Report.infof "GVN" "Switch : %a@.Common src %a@.Common dest %a@." pp p *)
        (*   pp_src common_src pp_dest common_dest *)
        (* ; *)
        let default =
          CCOption.map (insert_local_lets (common_src, common_dest)) default
        in 
        let branches =
          CCList.Assoc.map_values (insert_local_lets (common_src, common_dest)) branches
        in
        let discr = subst_src srcm discr in
        (common_src, common_dest), switch discr branches ?default
    in
    let common = (SrcMap.empty, DestMap.empty) in
    let p = insert_local_lets common @@ gvn common prog in
    default_fold () p
end

module Simple_copy_prop = struct

  type domain =
    | Top
    | Bot
    | InAlias of in_id | OutAlias of out_id
  let merge d1 d2 = match d1, d2 with
    | Top, _ | _, Top -> Top
    | Bot, t | t, Bot -> t
    | _ when d1 = d2 -> d1
    | _ -> Top
  type state = domain InIDMap.t * domain OutIDMap.t
  let empty = (InIDMap.empty, OutIDMap.empty)
  let union (ist1, ost1) (ist2, ost2) =
    let ist = InIDMap.union (fun _ v v' -> Some (merge v v')) ist1 ist2 in
    let ost = OutIDMap.union (fun _ v v' -> Some (merge v v')) ost1 ost2 in
    (ist, ost)
    
  
  type 'a t = state -> 'a * state

  let run (x: 'a t) = fst @@ x empty

  let return v : _ t = fun st -> v, st
  let bind (f : 'a -> 'b t) (x : 'a t) : 'b t =
    fun st ->
    let v, st = x st in
    f v st
  let map (f : 'a -> 'b) (x : 'a t) : 'b t =
    fun st ->
    let v, st = x st in
    f v, st
  let app (f : ('a -> 'b) t) (x : 'a t) : 'b t =
    fun st ->
    let f, st1 = f st in
    let x, st2 = x st in
    (f x), union st1 st2

  let (<$>) = map
  let (<*>) = app
      
  let (let*) e f = bind f e
  let (let+) e f = map f e
  
  let zip (x : 'a t) (y : 'b t) = CCPair.make <$> x <*> y
  let (and*) e1 e2 = zip e1 e2
      
  let zipL (l : _ t list) : _ t = fun st ->
    let vs, sts = 
      List.map (fun f -> f st) l
      |> List.split
    in
    vs, List.fold_left union empty sts
    
  let zipO l = match l with
    | None -> return None
    | Some h -> CCOption.some <$> h

  let add_in k v : unit t = fun (ist, ost) ->
    let ist = InIDMap.update k
        (function None -> Some v | Some v' -> Some (merge v' v)) ist
    in
    (), (ist, ost)
  let add_out k v : unit t = fun (ist, ost) ->
    let ost = OutIDMap.update k
        (function None -> Some v | Some v' -> Some (merge v' v)) ost
    in
    (), (ist, ost)
  let add_ins l : unit t = fun (ist, ost) ->
    let ist = 
      List.fold_left (fun ist (k, v) ->
          InIDMap.update k
            (function None -> Some v | Some v' -> Some (merge v' v)) ist
        ) ist l
    in
    (), (ist, ost)
  let add_outs l : unit t = fun (ist, ost) ->
    let ost =
      List.fold_left (fun ost (k, v) -> 
          OutIDMap.update k
            (function None -> Some v | Some v' -> Some (merge v' v)) ost
        ) ost l
    in
    (), (ist, ost)
  let get_in k : _ t = fun (ist, ost) ->
    let v = InIDMap.get_or k ~default:Bot ist in
    v, (ist, ost)
  let get_out k : _ t = fun (ist, ost) ->
    let v = OutIDMap.get_or k ~default:Bot ost in
    v, (ist, ost)
  
  let walk e =
    let tbl = Hashtbl.create 15 in
    let memo e =
      CCHashtbl.get_or_add tbl ~f:Fun.id ~k:e
    in
    let rec aux e =
      let* e' = match e with
        | Fail ->
          return Fail
        | Success ->
          return Success
        | Comment (str, next) ->
          let* next = aux next in 
          return @@ Comment (str, next)
        | Call (f, is, o, next) ->
          let* next = aux next in
          let* () = add_out o Top in
          let args =
            Name.Map.bindings is |> List.map (fun (_, v) -> (v, Top))
          in
          let* () = add_ins args in
          return @@ Call (f, is, o, next)
        | Prim (prim, o, is, next) ->
          let* next = aux next in
          let* () = add_out o Top in
          let args = List.map (fun v -> (v, Top)) is in
          let* () = add_ins args in
          return @@ Prim (prim, o, is, next)
        | Let (SubIn (i1, i2, path), next) ->
          let* next = aux next in
          let* d1 = get_in i1 in
          begin match d1 with
            | Bot -> return next
            | Top | InAlias _ | OutAlias _ ->
              let* () = add_in i2 Top in
              let* () = add_in i1 Bot in
              return @@ Let (SubIn (i1, i2, path), next)
          end
        | Let (SubOut (o1, o2, path), next) ->
          let* next = aux next in
          let* d1 = get_out o1 in
          begin match d1 with
            | Bot -> return next
            | Top | InAlias _ | OutAlias _ ->
              let* () = add_out o2 Top in
              let* () = add_out o1 Bot in
              return @@ Let (SubOut (o1, o2, path), next)
          end
        | Let (NewOut (o, l), next) ->
          let* next = aux next in
          let* d1 = get_out o in
          begin match d1 with
            | Bot -> return next
            | Top | InAlias _ | OutAlias _ ->
              let* () = add_out o Bot in
              return @@ Let (NewOut (o, l), next)
          end
        | Write (o, CopyIn i, next) ->
          let* next = aux next in
          let* () = add_in i Top in
          let* () = add_out o Top in
          return @@ Write (o, CopyIn i, next)
        | Write (o, (Imm _ | Alloc _ as rhs), next) ->
          let* next = aux next in
          let* () = add_out o Top in
          return @@ Write (o, rhs, next)
        | Cast (o, sh, next) ->
          let* next = aux next in 
          let* () = add_out o Top in
          return @@ Cast (o, sh, next)
        | Freeze (o, i, next) ->
          let* next = aux next in
          let* d1 = get_in i in
          begin match d1 with
            | Bot -> return next
            | Top | InAlias _ | OutAlias _ ->
              let* () = add_out o Top in
              return @@ Freeze (o, i, next)
          end
        | Switch (discr_id, branches, default) ->
          let* branches =
            zipL @@
            List.map (fun (lhs, cont) ->
                let* n = aux cont in
                return (lhs, n))
              branches
          and* default = zipO @@ Option.map aux default
          in
          (* We assume fail branches are unreachable *)
          let branches =
            List.filter
              (function (_, Fail) -> false | _ -> true)
              branches
          in
          match default, branches with
          | None, [] ->
            return Fail
          | Some br, _
            when List.for_all (fun (_, next) -> br == next) branches
            ->
            return br
          | None, (_, h)::t
            when List.for_all (fun (_, next) -> h == next) t
            ->
            return h
          | _ -> 
            let* () = add_in discr_id Top in
            return @@ switch discr_id branches ?default
      in
      let e' = memo e' in
      return e'
    in
    run @@ aux e

end

let optims e =
  e 
  |> Simple_copy_prop.walk
  (* |> Gvn.global_value_numbering *)

let go (funs, e) =
  List.map (fun (f,s,d,p) -> (f,s,d,optims p)) funs,
  optims e
