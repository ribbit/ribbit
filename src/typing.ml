let union_diff p =
  Name.Map.union (fun n _ _ ->
      Report.fail "Name %s is bound twice pattern %a" n Syntax.pp_pattern p)

let union_same env p =
  Name.Map.union (fun n ty1 ty2 ->
      if Types.equiv env ty1 ty2 then Some ty1
      else
        Report.fail "Name %s is bound with incompatible type %a and %a in pattern %a" n Types.pp ty1 Types.pp ty2 Syntax.pp_pattern p) 


let rec type_expr env : Syntax.type_expr -> Types.ty =
  function
  | Integer {signed; width} ->
    Word {width; values=()}
  | Record fields ->
    let fields = List.map
        (fun (n, te) -> let ty = type_expr env te in (n, ty))
        fields
    in
    Record fields
  | Tuple tes ->
    let tys = List.map (type_expr env) tes in
    Product tys
  | Type_constructor name ->
    Env.check_ty name env;
    Type name

let rec pattern env (p0 : Syntax.pattern) ty0 =
  match p0, Types.unroll env ty0 with
  | PVar n, _ -> Name.Map.singleton n ty0
  | PAny, _ -> Name.Map.empty
  | PConstr (k, p), (Sum _ as ty) ->
    let ty = Types.find_constr_in_type k ty in
    pattern env p ty
  | PRecord ps, Record tys ->
    List.fold_left (union_diff p0) Name.Map.empty @@
    List.map (fun (n, p) ->
        begin match List.assoc_opt n tys with
          | None -> raise (Types.Constructor_not_in_type (n, ty0))
          | Some ty -> pattern env p ty
        end
      ) ps
  | PTuple ps, Product tys when List.length ps = List.length tys ->
    List.fold_left (union_diff p0) Name.Map.empty @@
    List.map2 (pattern env) ps tys
  | POr (p1, p2), ty ->
    union_same env p0
      (pattern env p1 ty)
      (pattern env p2 ty)
  | PInt _, Word _ ->
    Name.Map.empty
  | _ -> Report.fail "Pattern %a doesn't have type %a"
           Syntax.pp_pattern p0 Types.pp ty0

module Matching = struct
  (* Explode OR pattern on the way *)

  let _union_bind a b =
    (Name.Map.union (fun _ _ v -> Some v)) a b

  let explode_inner_patterns f ps =
    let mps = List.map (fun (path, p_i) ->
        List.map (fun x -> path, x) p_i)
        ps
    in
    let combinations = CCList.cartesian_product mps in
    List.concat_map
      (fun ress ->
         let pats, binds =
           List.fold_left (fun (cases, binds) (rpath, (pat, bind)) ->
               (rpath, pat)::cases, _union_bind bind binds
             ) ([], Name.Map.empty) (List.rev ress)
         in
         f pats binds
      ) combinations
  
  let rec explode_pattern ~path (pat : Syntax.pattern) = match pat with
    | PAny ->
      [(Prov.Top, Name.Map.empty)]
    | PInt i ->
      [(Prov.Int i, Name.Map.empty)]
    | PVar v ->
      [(Prov.Top, Name.Map.singleton v path)]
    | POr (p1, p2) ->
      explode_pattern ~path p1 @ explode_pattern ~path p2
    | PConstr (k, p) ->
      let path = Hole.(path $. HConstr k) in
      List.map
        (fun (pat, cl) -> (Prov.Cons (k, pat), cl))
        (explode_pattern ~path p)
    | PTuple ps ->
      let inner_ps = List.mapi
          (fun i p -> i, explode_pattern ~path:Hole.(path $. HAccess i) p)
          ps
      in
      let f ps_i bind =
        [(Prov.Tup (List.map snd ps_i), bind)]
      in
      explode_inner_patterns f inner_ps
    | PRecord ps ->
      let inner_ps = List.map
          (fun (n, p) -> n, explode_pattern ~path:Hole.(path $. HField n) p)
          ps
      in
      let f ps_i bind =
        [(Prov.Rec ps_i, bind)]
      in
      explode_inner_patterns f inner_ps
  
  let explode_clause pats =
    List.rev @@ (* Try to preserve case ordering *)
    CCList.map_product_l (explode_pattern ~path:Empty) pats
  
  (* let explode_map ~expr ~paths (pats : Syntax.pattern Name.Map.t) = *)
  (*   Name.Map.fold *)
  (*     (explode_one ~expr ~paths) *)
  (*     pats *)
  (*     [Name.Map.empty, (expr, Name.Map.empty)] *)

  (* let explode_from_clauses ~root env clauses f : _ list = *)
  (*   let l =  *)
  (*     Report.enter ~__LINE__ ~__FILE__ "Explode from clauses" @@ fun _ -> *)
  (*     List.fold_left *)
  (*       (fun mp (pattern, expr) -> *)
  (*          let mp' = explode_map ~expr *)
  (*              ~paths:(Name.Map.singleton root Hole.Empty) *)
  (*              (Name.Map.singleton root pattern) *)
  (*          in *)
  (*          mp @ mp') *)
  (*       [] *)
  (*       clauses *)
  (*   in *)
  (*   List.map (fun pat (expr, bind) -> f pat expr) l *)
end


let rec value env (v0 : Value.t) ty0 =
  match v0, Types.unroll env ty0 with
  | VConstr (k, v), Sum l ->
    begin match List.assoc_opt k l with
      | None -> raise (Types.Constructor_not_in_type (k, ty0))
      | Some ty -> value env v ty
    end
  | VTuple vs, Product tys when List.length vs = List.length tys ->
    List.iter2 (value env) vs tys
  | VRecord vs, Record tys ->
    List.iter (fun (n, v) ->
        begin match List.assoc_opt n tys with
          | None -> raise (Types.Constructor_not_in_type (n, ty0))
          | Some ty -> value env v ty
        end
      ) vs;
    List.iter (fun (n, ty) ->
        if not @@ List.mem_assoc n vs then
          Report.fail "Missing field %s in %a" n Value.pp v0
      ) tys
  | VInt _, Word _ -> ()
  | _ -> Report.fail "Value %a doesn't have type %a"
           Value.pp v0 Types.pp ty0

let check_ty env e ty ~expected:ty' =
  match ty' with
  | ty' when not @@ Types.equiv env ty ty' ->
    Report.fail "%a has type %a but %a was expected"
      Syntax.pp_expr e Types.pp ty Types.pp ty'
  | _ -> ()
let may_check_ty env e ty ~expected:ty' =
  match ty' with
  | Some ty' when not @@ Types.equiv env ty ty' ->
    Report.fail "%a has type %a but %a was expected"
      Syntax.pp_expr e Types.pp ty Types.pp ty'
  | _ -> ()

let word_size env e ty =
  match Types.unroll env ty with
  | Types.Word l -> l
  | _ ->
    Report.fail "%a has type %a but a word was expected"
      Syntax.pp_expr e Types.pp ty

exception Cannot_infer

let make_typedexpr env e ty0 ~expected:ty' desc =
  may_check_ty env e ty0 ~expected:ty';
  let ty = match ty' with
    | Some t -> t
    | _ -> ty0
  in
  let mty = Reprs.find_mty env ty in
  TypedExpr.{ desc ; ty ; mty }

let rec expr env (e0 : Syntax.expr) expected_ty : TypedExpr.t =
  let mk = make_typedexpr env e0 ~expected:expected_ty in
  match e0 with
  | (EVar _ | EConstr _ | ETuple _ | ERecord _ | EInt _) as e ->
    let bound = Queue.create () in
    let pe, ty = path_expr bound env e expected_ty in
    let rec aux () = match Queue.take_opt bound with
      | None -> mk ty @@ PathExpr pe
      | Some (n, e) -> mk ty @@ Let (n, e, aux ())
    in
    aux ()
  | ECstr (e, ty) ->
    let ty = type_expr env ty in
    may_check_ty env e0 ty ~expected:expected_ty;
    expr env e (Some ty)
  | EPrim1 (Not, e) ->
    let* env, n, ty = env, expr env e expected_ty in
    may_check_ty env e0 Builtins.Bool.ty ~expected:expected_ty;
    check_ty env e ty ~expected:Builtins.Bool.ty;
    mk ty @@ Prim (Not, [n])
  | EPrim1 (Minus, e) ->
    let* env, n, ty = env, expr env e expected_ty in
    let l = word_size env e ty in
    may_check_ty env e0 (Word l) ~expected:expected_ty;
    mk ty @@ Prim (Neg, [n])
  | EPrim2 ((And | Or) as prim, e1, e2) ->
    let prim : Target.prim =
      match prim with And -> And | Or -> Or | _ -> assert false
    in
    let* env, n1, ty1 = env, expr env e1 expected_ty in
    let* env, n2, ty2 = env, expr env e2 expected_ty in
    may_check_ty env e0 Builtins.Bool.ty ~expected:expected_ty;
    check_ty env e1 ty1 ~expected:Builtins.Bool.ty;
    check_ty env e2 ty2 ~expected:Builtins.Bool.ty;
    (* TODO: Left bias on the representation *)
    mk ty1 @@ Prim (prim, [n1; n2])
  | EPrim2 (
      (Add | Sub | Mul | Div | ShiftLeft | ShiftRight | BitAnd | BitOr | BitXor)
      as prim
    , e1, e2) ->
    let prim : Target.prim =
      match prim with
      | Add -> Add | Sub -> Sub | Mul -> Mul | Div -> Div
      | ShiftLeft -> ShiftLeft | ShiftRight -> ShiftRight
      | BitAnd -> And | BitOr -> Or | BitXor -> Xor
      | _ -> assert false
    in
    let* env, n1, ty1 = env, expr env e1 expected_ty in
    let* env, n2, ty2 = env, expr env e2 expected_ty in
    let l = 
      let l1 = word_size env e1 ty1 in
      let l2 = word_size env e2 ty1 in
      if l1 = l2 then l1
      else Report.fail "In %a, both operand don't have the same wordsize."
          Syntax.pp_expr e0
    in
    may_check_ty env e0 (Word l) ~expected:expected_ty;
    (* TODO: Left bias on the representation *)
    mk ty1 @@ Prim (prim, [n1; n2])
  | EPrim2 ((Equal | Leq | Le | Geq | Ge) as prim, e1, e2) ->
    let prim : Target.prim =
      match prim with
      | Equal -> Equal
      | Leq -> Leq | Le ->  Le | Geq -> Geq | Ge -> Ge
      | _ -> assert false
    in
    (* TODO Better Inference *)
    let* env, n1, ty1 = env, expr env e1 None in
    let* env, n2, ty2 = env, expr env e2 None in
    check_ty env e0 ty2 ~expected:ty1;
    mk Builtins.Bool.ty @@ Prim (prim, [n1; n2])
  | EApp (f, args) ->
    let tys_in, ty_out = match Env.find_var f env with
      | Val ty' ->
        Report.fail
          "%s has type %a, it is not a function, it can not be applied."
          f Types.pp ty'
      | Fun (ty1, ty2) -> ty1, ty2
    in
    Name.Map.iter (fun n ty ->
        if not @@ Name.Map.mem n args then
          Report.fail "Missing field %s in %a" n Syntax.pp_expr e0
      ) tys_in;
    let rec aux env args = function
      | [] ->
        mk ty_out @@ Call (f, args)
      | (arg, arg_e) :: rest ->
        let arg_ty = Name.Map.find arg tys_in in
        let* env, arg_n, ty_n = env, expr env arg_e (Some arg_ty) in
        aux env (Name.Map.add arg arg_n args) rest
    in
    aux env Name.Map.empty @@ Name.Map.to_list args
  | EMatch (ETuple matchees, clauses)
    when List.for_all (fun (clause : Syntax.clause) ->
        match clause.pattern with
        | PTuple ps -> List.length ps = List.length matchees
        | PAny -> true
        | _ -> false
      ) clauses
    ->
    let arity = List.length matchees in
    let clauses =
      List.map (fun {Syntax. pattern = p; expr = e} ->
          let ps = match p with
            | PTuple ps -> ps
            | PAny -> CCList.replicate arity Syntax.PAny
            | _ -> assert false
          in
          ps, e
        )
        clauses
    in
    matching env e0 matchees clauses expected_ty
  | EMatch (matchee, clauses) ->
    let clauses =
      List.map (fun {Syntax. pattern = p; expr = e} -> [p], e) clauses
    in
    matching env e0 [matchee] clauses expected_ty
  | ELet (n, e, e') ->
    let** env, _ = env, n, expr env e None in
    expr env e' expected_ty

and matching env e0 matchees clauses expected_ty =  
  let rec aux env matchees ptyps l k = match l with
    | [] ->
      k env (List.rev matchees) (List.rev ptyps)
    | matchee :: rest ->
      let* env, matchee, ptyp = env, expr env matchee None in
      aux env (matchee::matchees) (ptyp::ptyps) rest k
  in
  aux env [] [] matchees @@ fun env matchees ptyps -> 
  let clauses = 
    List.concat_map (fun (ps, e) ->
        let env =
          List.fold_left2
            (fun env p ty ->
               let venv = pattern env p ty in
               Name.Map.fold (fun n ty env ->
                   Env.add_var n (Val ty) env) venv env
            )
            env
            ps ptyps
        in
        let e0 = expr env e expected_ty in
        let cases = Matching.explode_clause ps in
        let rec aux env matchees typs holesL =
          match matchees, typs, holesL with
          | matchee::matchees, ty::typs, holes::holesL ->
            let rest = aux env matchees typs holesL in
            let s = Name.Map.map (fun pos -> (matchee, pos)) holes in
            TypedExpr.subst s rest
          | _ -> e0
        in
        List.map (fun line ->
            let provs, holesL = List.split line in
            let e = aux env matchees ptyps holesL in
            (provs, e))
          cases
      ) clauses
  in
  let ty = match clauses with
    | [] -> raise Cannot_infer
    | (p, (te : TypedExpr.t)) :: rest ->
      (* A bit ad-hoc *)
      List.iter
        (fun (_, te') -> check_ty env e0 te'.TypedExpr.ty ~expected:te.ty)
        rest;
      te.ty
  in
  make_typedexpr env e0 ty ~expected:expected_ty @@
  Matching { matchee = matchees; clauses }

(** Let binding *)
and (let**) (env, n, (e : TypedExpr.t)) k : TypedExpr.t =
  let env = Env.add_var n (Val e.ty) env in
  let e' = k (env, e.ty) in
  TypedExpr.{ desc = Let (n, e, e'); ty = e'.ty ; mty = e'.mty }  

(** Lifting of subexpressions *)
and (let*) (env, (e : TypedExpr.t)) k : TypedExpr.t =
  (* match e.desc with *)
  (* | PathExpr (EPos (n, Empty)) -> *)
  (*   k (env, n, e.ty) *)
  (* | _ -> *)
    let n = Name.fresh () in
    let** env, ty = env, n, e in
    k (env, n, e.ty)

and path_expr bound env (e0 : Syntax.expr) ty0 : PathExpr.t * Types.ty =
  match e0, CCOption.map (Types.unroll env) ty0 with
  | EVar n, _ ->
    let ty' = match Env.find_var n env with
      | Fun (arg_tys, res_ty) ->
        Report.fail "%s has type %a -> %a but a value was expected"
          n (Name.Map.pp Types.pp) arg_tys Types.pp res_ty
      | Val ty' -> ty'
    in
    EPos (n, Hole.Empty), ty'
  | EConstr (k, e), Some (Sum l as ty0) ->
    begin match List.assoc_opt k l with
      | None -> raise (Types.Constructor_not_in_type (k, ty0))
      | Some ty ->
        let pe, _ = path_expr bound env e (Some ty) in
        EConstr(k, pe), Sum l
    end
  | ETuple es, None ->
    let ps, tys =
      List.split @@ List.map (fun e -> path_expr bound env e None) es
    in
    ETuple ps, Product tys
  | ETuple es, Some Product tys when List.length es = List.length tys ->
    ETuple (List.map2 (fun e ty -> fst @@ path_expr bound env e (Some ty)) es tys),
    Product tys
  | ERecord es, Some (Record tys as ty0) ->
    let fields =
      List.map (fun (n, e) ->
          begin match List.assoc_opt n tys with
            | None -> raise (Types.Constructor_not_in_type (n, ty0))
            | Some ty -> n, fst @@ path_expr bound env e (Some ty)
          end
        ) es
    in
    List.iter (fun (n, ty) ->
        if not @@ List.mem_assoc n es then
          Report.fail "Missing field %s in %a" n Syntax.pp_expr e0
      ) tys;
    ERecord fields, Record tys
  | EInt i, Some Word l ->
    EInt i, Word l
  | ECstr (e, ty), _ ->
    let ty = type_expr env ty in
    may_check_ty env e0 ty ~expected:ty0;
    path_expr bound env e (Some ty)
  | (EConstr _ | ERecord _ | EInt _), None ->
    Report.fail "Can't infer type for expression %a"
      Syntax.pp_expr e0
  | (EConstr _ | ETuple _ | ERecord _ | EInt _), Some ty ->
    Report.fail "Expr %a doesn't have type %a"
      Syntax.pp_expr e0 Types.pp ty
  | _ ->
    let te = expr env e0 ty0 in
    let n = Name.fresh () in
    Queue.add (n, te) bound;
    EPos (n, Empty), te.ty

let valdecl env e ty =
  let typ = type_expr env ty in
  expr env e (Some typ)
  |> TypedExpr.simplify

let fundecl env0 (d: Syntax.fundecl) =
  let args =
    Name.Map.map
      (fun ty -> type_expr env0 ty)
      d.arg_tys
  in
  let res = type_expr env0 d.res_ty in
  let env =
    Env.add_var d.name (Fun (args, res)) @@    
    Name.Map.fold Env.add_var
      (Name.Map.map (fun ty -> Env.Val ty) args)
      env0
  in
  let e = expr env d.body (Some res) in
  args, TypedExpr.simplify e

let tydecls (env0 : Env.t) decls : Env.t =
  Report.enter ~__LINE__ ~__FILE__ "Typing declarations" @@ fun _ ->
  let decl_env =
    List.fold_left (fun env (decl : Syntax.typedecl) ->
        Env.add_ty decl.name Env.Abstract env)
      env0 decls
  in
  Report.debugf "Starting env:@ %a" Env.pp decl_env;
  let get_src_type def =
    match def with
    | Syntax.Type_expr ty ->
      type_expr decl_env ty
    | Enum variants ->
      let sumspec =
        List.map
          (fun (constructor, arg_ty) -> constructor, type_expr decl_env arg_ty)
          variants
      in
      Types.Sum sumspec
  in
  let get_mem_type ty (repr : Syntax.repr) = match repr with
    | Named n -> Reprs.compute_mty_decl (Some n) decl_env ty
    | Default -> Reprs.compute_mty_decl None decl_env ty
    | Memty memty -> Speclang.validate memty
  in
  let ty_decls =
    List.map (fun (decl : Syntax.typedecl) ->
        let ty = get_src_type decl.definition in
        let mty = get_mem_type ty decl.repr in
        decl.name, ty, mty)
      decls
  in
  let new_env =
    List.fold_left (fun env (name, ty, mty) ->
        Env.add_ty name (Env.Decl (ty, mty)) env)
      Env.empty ty_decls
  in
  let full_env = Env.union env0 new_env in
  List.iter (fun (name, ty, mty) ->
      Speclang.check_agreement full_env name ty mty) ty_decls;
  new_env
