(* The lexer definition *)

{
open Parser
type Input.Lex.error +=
  | Unterminated_comment of Location.loc

(* The table of keywords *)

let keyword_table =
  CCHashtbl.of_list [
    "type", TYPE;
    "enum", ENUM;
    "struct", STRUCT;
    "let", LET;
    "match_fn", MATCH;
    "match", MATCH;
    "bench", BENCH;
    "with", WITH;
    "represented", REPRESENTED;
    "repr", REPRESENTED;
    "as", AS;
    "by", BY;
    "from", FROM;
    "split", SPLIT;
    "fn", FUN;
    "fun", FUN;
    "function", FUN;
]

(* To buffer string literals *)

let string_buffer = Buffer.create 256
let reset_string_buffer () = Buffer.reset string_buffer
let get_stored_string () = Buffer.contents string_buffer
let store_string s = Buffer.add_string string_buffer s
let store_lexeme lexbuf = store_string (Lexing.lexeme lexbuf)

(* To store the position of the beginning of a string and comment *)
let comment_start_loc = ref [];;

let wrap_comment_lexer comment lexbuf =
  let start_loc = Location.of_lex lexbuf  in
  comment_start_loc := [start_loc];
  reset_string_buffer ();
  let _end_loc = comment lexbuf in
  let s = get_stored_string () in
  reset_string_buffer ();
  s(* ,
    * { start_loc with Location.loc_end = end_loc.Location.loc_end } *)

let error lexbuf e = raise (Input.Lex.Error(e, Location.of_lex lexbuf))
let error_loc loc e = raise (Input.Lex.Error(e, loc))

(* Update the current location with file name and line number. *)

let update_loc lexbuf line chars =
  let pos = lexbuf.Lexing.lex_curr_p in
  let new_file = pos.pos_fname in
  lexbuf.lex_curr_p <- { pos with
    pos_fname = new_file;
    pos_lnum = pos.pos_lnum + line;
    pos_bol = pos.pos_cnum - chars;
  }

(* Error report *)

let prepare_error = function
  | Input.Lex.Error (Unterminated_comment _, loc) ->
    Some (Report.errorf ~loc "Comment not terminated")
  | _ -> None

let () =
  Report.register_report_of_exn prepare_error
}

let newline = ('\013'* '\010')
let blank = [' ' '\009' '\012']
let lowercase = ['a'-'z' '_']
let uppercase = ['A'-'Z']
let digit = ['0'-'9']
let other = [ '\'']
let identchar = lowercase|uppercase|digit|other

rule token = parse
  | newline
      { update_loc lexbuf 1 0;
        token lexbuf }
  | blank +
      { token lexbuf }
  | '_' { UNDERSCORE }
  | ('-'? digit+) as s
    { ZINT (Z.of_string s) }
  | 'i' (digit+ as s)
      { INTTY (int_of_string s) }
  | 'u' (digit+ as s)
      { UINTTY (int_of_string s) }
  | 'w' (digit+ as s)
      { WORDTY (int_of_string s) }
  | lowercase identchar* as name
      { try Hashtbl.find keyword_table name
        with Not_found -> LIDENT name }
  | uppercase identchar* as name
      { try Hashtbl.find keyword_table name
        with Not_found -> UIDENT name }
  | "("  { LPAREN }
  | ")"  { RPAREN }
  | "["  { LBRACK }
  | "]"  { RBRACK }
  | "{{"  { LACCOACCO }
  | "}}"  { RACCOACCO }
  | "{"  { LACCO }
  | "}"  { RACCO }
  | "!"  { BANG }
  | "<"  { LANGLE }
  | "<<"  { LANGLELANGLE }
  | "<="  { LANGLEEQ }
  | ">"  { RANGLE }
  | ">>"  { RANGLERANGLE }
  | ">="  { RANGLEEQ }
  | "="  { EQ }
  | "=="  { EQEQ }
  | "|"  { BAR }
  | "||"  { BARBAR }
  | "&"  { AMPER }
  | "&&"  { AMPERAMPER }
  | "^"  { CARRET }
  | ","  { COMMA }
  | ";"  { SEMI }
  | "."  { DOT }
  | ":"  { COLON }
  | "*"  { MULT }
  | "+"  { PLUS }
  | "/"  { SLASH }
  | "-"  { MINUS }
  | "->" { ARROW }
  | "=>" { DOUBLEARROW }
  | "(*EXPECT"
      { let s = wrap_comment_lexer expect lexbuf in
        EXPECT s }
  | "//"
      { let _ = line_comment lexbuf in
        token lexbuf }
  | "/*"
      { let _ = wrap_comment_lexer comment lexbuf in
        token lexbuf }
  | eof { EOF }
  | (_ as illegal_char)
      { error lexbuf (Peahell.Input.Lex.Illegal_character illegal_char) }

and line_comment = parse
  | eof
      { Location.of_lex lexbuf }
  | newline
      { update_loc lexbuf 1 0; Location.of_lex lexbuf }
  | _
      { store_lexeme lexbuf; line_comment lexbuf }

and comment = parse
    "/*"
      { comment_start_loc := (Location.of_lex lexbuf) :: !comment_start_loc;
        store_lexeme lexbuf;
        comment lexbuf
      }
  | "*/"
      { match !comment_start_loc with
        | [] -> assert false
        | [_] -> comment_start_loc := []; Location.of_lex lexbuf
        | _ :: l -> comment_start_loc := l;
                  store_lexeme lexbuf;
                  comment lexbuf
       }
  | eof
      { match !comment_start_loc with
        | [] -> assert false
        | loc :: _ ->
          let start = List.hd (List.rev !comment_start_loc) in
          comment_start_loc := [];
          error_loc loc (Unterminated_comment start)
      }
  | newline
      { update_loc lexbuf 1 0;
        store_lexeme lexbuf;
        comment lexbuf
      }
  | _
      { store_lexeme lexbuf; comment lexbuf }

and expect = parse
  | "EXPECT*)"
      { match !comment_start_loc with
        | [] -> assert false
        | [_] -> comment_start_loc := []; Location.of_lex lexbuf
        | _ :: l -> comment_start_loc := l;
                  store_lexeme lexbuf;
                  expect lexbuf
       }
  | eof
      { match !comment_start_loc with
        | [] -> assert false
        | loc :: _ ->
          let start = List.hd (List.rev !comment_start_loc) in
          comment_start_loc := [];
          error_loc loc (Unterminated_comment start)
      }
  | newline
      { update_loc lexbuf 1 0;
        store_lexeme lexbuf;
        expect lexbuf
      }
  | _
      { store_lexeme lexbuf; expect lexbuf }
