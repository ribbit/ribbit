type name = string

and type_expr =
  | Type_constructor of name
  | Tuple of type_expr list
  | Record of (name * type_expr) list
  | Integer of {signed: bool; width: int}

and definition =
  | Enum of (name * type_expr) list
  | Type_expr of type_expr

and typedecl = {
  name : name;
  definition : definition;
  repr : repr;
}

and repr =
  | Named of name
  | Memty of memty
  | Default

and path =
  | Root
  | Deref of path
  | Access of path * int
  | Extract of path * int * int

and memty =
  | TVar: name -> memty
  | TConst: Z.t -> memty
  | TWord: int -> memty
  | TPtr: { len: int; align: int; pointee: memty } -> memty
  | TStruct: memty list -> memty
  | TFrag : memty * Hole.t -> memty
  | TSplit: {
      discrs: path list;
      branches: membranch list;
    } -> memty
  | TWith : memty * path * memty -> memty
and membranch = {
  lhs : (Z.t option) list ;
  provenance: provenance;
  ty : memty ;
}
and provenance =
  | Any
  | Tuple of provenance list
  | Record of (Name.t * provenance) list
  | Constr of Name.t * provenance
  | Int of Z.t
  | Or of provenance * provenance

and matching = {
  pattern_type : type_expr ;
  expr_type : type_expr ;
  clauses : clause list ;
}

and clause = {
  pattern : pattern ;
  expr : expr ;
}

and pattern =
  | PVar of name
  | PConstr of Name.t * pattern
  | PRecord of (Name.t * pattern) list
  | PTuple of pattern list
  | PAny
  | POr of pattern * pattern
  | PInt of Z.t

and binop =
  | Add | Sub | Mul | Div
  | Leq | Le | Geq | Ge | Equal
  | And | Or
  | ShiftLeft | ShiftRight
  | BitAnd | BitOr | BitXor

and unop =
  | Minus
  | Not

and expr =
  | EVar of name
  | EApp of name * expr Name.Map.t
  | EPrim1 of unop * expr
  | EPrim2 of binop * expr * expr
  | ELet of name * expr * expr
  | ECstr of expr * type_expr
  | EMatch of expr * clause list
  | EInt of Z.t
  | ETuple of expr list
  | ERecord of (Name.t * expr) list
  | EConstr of Name.t * expr

and value =
  | VInt of Z.t
  | VRecord of (Name.t * value) list
  | VTuple of value list
  | VConstr of Name.t * value

and valdecl = name * type_expr * expr

and fundecl = {
  name: name;
  arg_tys: type_expr Name.Map.t;
  res_ty: type_expr;
  body: expr;
}

and command =
  | TypeDecl of typedecl
  | FunDecl of fundecl
  | ValDecl of valdecl
  | Bench of name * (name list)

and program = command list


let rec pp_provenance fmt (p : provenance) = match p with
  | Constr (constr, Tuple []) ->
    Fmt.pf fmt "%s" constr
  | Constr (constr, Tuple args) ->
    Fmt.pf fmt "%s(%a)" constr (Fmt.list ~sep:Fmt.comma pp_provenance) args
  | Constr (constr, arg) ->
    Fmt.pf fmt "%s(%a)" constr pp_provenance arg
  | Record l ->
    Fmt.pf fmt "{%a}" Fmt.(list ~sep:comma @@ pair ~sep:(any ": ") Name.pp pp_provenance) l
  | Tuple l ->
    Fmt.pf fmt "(%a)" (Fmt.list ~sep:Fmt.comma pp_provenance) l
  | Any -> Fmt.pf fmt "_"
  | Or(p1, p2) -> Fmt.pf fmt "%a | %a" pp_provenance p1 pp_provenance p2
  | Int z -> Fmt.pf fmt "%a" Z.pp_print z

let rec pp_pattern fmt p = match p with
  | PVar x -> Fmt.pf fmt "%s" x
  | PConstr (constr, PTuple []) ->
    Fmt.pf fmt "%s" constr
  | PConstr (constr, PTuple args) ->
    Fmt.pf fmt "%s(%a)" constr (Fmt.list ~sep:Fmt.comma pp_pattern) args
  | PConstr (constr, arg) ->
    Fmt.pf fmt "%s(%a)" constr pp_pattern arg
  | PRecord l ->
    Fmt.pf fmt "{%a}" Fmt.(list ~sep:comma @@ pair ~sep:(any ": ") Name.pp pp_pattern) l
  | PTuple l ->
    Fmt.pf fmt "(%a)" (Fmt.list ~sep:Fmt.comma pp_pattern) l
  | PAny -> Fmt.pf fmt "_"
  | POr(p1, p2) -> Fmt.pf fmt "%a | %a" pp_pattern p1 pp_pattern p2
  | PInt z -> Fmt.pf fmt "%a" Z.pp_print z

let pp_unop fmt = function
  | Minus -> Fmt.pf fmt "-"
  | Not -> Fmt.pf fmt "!"

let pp_binop fmt : binop -> _ = function
  | Add -> Fmt.pf fmt "+"
  | Sub -> Fmt.pf fmt "-"
  | Mul -> Fmt.pf fmt "*"
  | Div -> Fmt.pf fmt "/"
  | Leq -> Fmt.pf fmt "<="
  | Le -> Fmt.pf fmt "<"
  | Geq -> Fmt.pf fmt ">="
  | Ge -> Fmt.pf fmt ">"
  | Equal -> Fmt.pf fmt "=="
  | And -> Fmt.pf fmt "&&"
  | Or -> Fmt.pf fmt "||"
  | BitAnd -> Fmt.pf fmt "&"
  | BitOr -> Fmt.pf fmt "|"
  | BitXor -> Fmt.pf fmt "^"
  | ShiftLeft -> Fmt.pf fmt "<<"
  | ShiftRight -> Fmt.pf fmt ">>"

let rec pp_expr fmt e = match e with
  | EVar x -> Fmt.pf fmt "%s" x
  | EApp (f, args) -> Fmt.pf fmt "%s(%a)" f (Name.Map.pp pp_expr) args
  | EPrim1 (p, e) -> Fmt.pf fmt "%a(%a)" pp_unop p pp_expr e
  | EPrim2 (p, e, e') -> Fmt.pf fmt "%a %a %a" pp_expr e pp_binop p pp_expr e'
  | ELet (x, e, e') ->
    Fmt.pf fmt "let %s =@ @[<2>%a@]@ in@ %a" x pp_expr e pp_expr e'
  | EMatch (e, cls) ->
    Fmt.pf fmt "match %a {@ @[<v 2>%a@]@ }" pp_expr e
      Fmt.(list ~sep:Fmt.cut pp_clause) cls
  | EConstr (constr, ETuple []) ->
    Fmt.pf fmt "%s" constr
  | EConstr (constr, ETuple args) ->
    Fmt.pf fmt "%s(%a)" constr (Fmt.list ~sep:Fmt.comma pp_expr) args
  | EConstr (constr, arg) ->
    Fmt.pf fmt "%s(%a)" constr pp_expr arg
  | ETuple l ->
    Fmt.pf fmt "(%a)" (Fmt.list ~sep:Fmt.comma pp_expr) l
  | ERecord l ->
    Fmt.pf fmt "{%a}" Fmt.(list ~sep:comma @@ pair ~sep:(any ": ") Name.pp pp_expr) l
  | EInt z -> Fmt.pf fmt "%a" Z.pp_print z
  | ECstr (e, _ty) -> Fmt.pf fmt "%a" pp_expr e

and pp_clause fmt { pattern; expr } =
  Fmt.pf fmt "@[%a =>@ %a,@]" pp_pattern pattern pp_expr expr

let rec focus_expr (h0 : Hole.t) e0 = match Hole.pop h0, e0 with
  | None, _ -> e0
  | Some(HField n, hole), ERecord l ->
    focus_expr hole @@ List.assoc n l
  | Some(HAccess pos, hole), ETuple l ->
    focus_expr hole @@ List.nth l pos
  | Some(HConstr constr, hole), EConstr (constructor, argument)
    when constr = constructor ->
    focus_expr hole argument
  | _ ->
    Report.fail "Mismatched holexpr types: %a ≠ %a@." Hole.pp h0 pp_expr e0

let rec expr_of_val : value -> expr = function
  | VInt z -> EInt z
  | VRecord fields ->
    ERecord (List.map (fun (x, v) -> x, expr_of_val v) fields)
  | VTuple fields ->
    ETuple (List.map expr_of_val fields)
  | VConstr (k, v) ->
    EConstr (k, expr_of_val v)
