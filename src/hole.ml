(** A hole denotes a position in a term *)

module M = struct
  type op =
    | HAccess of int
    | HField of Name.t
    | HConstr of Name.t
    | HBits of { offset: int; width: int }
  type t =
    | Empty
    | Dot of t * op

  let equal (x : t) y = x = y
  let compare = Stdlib.compare
end
include M
module Map = CCMap.Make(M)
module Set = CCSet.Make(M)

type l = op list

let pp_op fmt = function
  | HAccess i -> Fmt.int fmt i
  | HField field -> Fmt.string fmt field
  | HConstr constr -> Fmt.string fmt constr
  | HBits {offset; width} ->
    Fmt.pf fmt "[%i:%i]" offset width

let rec pp_suffix fmt = function
  | Empty -> ()
  | Dot (t, op) ->
    Fmt.pf fmt "%a.%a" pp_suffix t pp_op op

let pp fmt path = Fmt.pf fmt "_%a" pp_suffix path

let rec (++) p1 p2 = match p2 with
  | Empty -> p1
  | Dot (p2', op) -> Dot (p1 ++ p2', op)

let rec subst h s = h ++ s

let ($.) pa op = Dot (pa, op)

let rec of_rev_list = function
  | [] -> Empty
  | h :: l -> Dot(of_rev_list l, h)
let of_list l = of_rev_list @@ List.rev l

let rec to_rev_list = function
  | Empty -> []
  | Dot (pa, op) -> op :: to_rev_list pa
let to_list pa = List.rev @@ to_rev_list pa

(** [is_prefix pa pa'] checks if [pa] is a prefix of [pa'] *)
let is_prefix pa pa' =
  let rec aux l l' = match l, l' with
    | [], _ -> true
    | _, [] -> false
    | op :: l, op' :: l' -> op = op' && aux l l'
  in
  let b = aux (to_list pa) (to_list pa') in
  Report.debugf "is_prefix(%a,%a) = %b" pp pa pp pa' b;
  b

let focus pa pa' =
  let rec aux l l' = match l, l' with
    | [], _ -> of_list l'
    | _, [] -> invalid_arg "Not a prefix"
    | op :: l, op' :: l' ->
      if op = op' then aux l l' else invalid_arg "Not a prefix"
  in aux (to_list pa) (to_list pa')
  
let rec pop_non_empty = function
  | Empty -> assert false
  | Dot (Empty, op) -> op, Empty
  | Dot (pa, op) ->
    let head, pa' = pop_non_empty pa in
    head, Dot(pa', op)

let rec pop = function
  | Empty -> None
  | p -> Some (pop_non_empty p)

let rec push op0 = function
  | Empty -> Dot (Empty, op0)
  | Dot (pa, op) -> Dot (push op0 pa, op)
